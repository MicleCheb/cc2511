#ifndef UTIL_H_INCLUDED
#define UTIL_H_INCLUDED
#include <stdint.h>
#include <stdio.h>
#include "cc1111.h"

#define sBit(byte,bit)     (byte|= (1<<bit))
#define cBit(byte,bit)     (byte&=~(1<<bit))
#define tBit(byte,bit)     (byte&  (1<<bit))
#define fBit(byte,bit)     (byte^= (1<<bit))

uint8_t asciiToHex( uint8_t ch);
uint8_t getHexData(uint8_t *ch );
void prtBuf(__xdata uint8_t *buf,uint8_t cnt);
uint32_t getDigit(void);
void printHelp(void);
uint8_t IntelHex(void)  __reentrant; //__xdata uint8_t *Buf
#define recData     0   //:10246200464C5549442050524F46494C4500464C33
#define recEOF      1   //:00000001FF
#define recSegment  2   //:020000021200EA
#define recStartSeg 3   //:040000030000C00039 Start Segment Address Record.
#define recLinear   4   //:02000004FFFFFC
#define recMDKARM   5   //:04000005000000CD2A

//
//uint16_t seed = 0;
//uint8_t rnd8(void) __reentrant
//{
//    seed = (seed << 7) - seed + 251;
//    return (uint8_t)(seed + (seed>>8));
//}
//
uint8_t asciiToBin(uint8_t ch)
{
    if((ch>='0') && (ch<='9'))      ch-='0';
    else if((ch>='A') && (ch<='F')) ch-=('A'-10);
    else if((ch>='a') && (ch<='f')) ch-=('a'-10);
    else ch=0xFF;
    return ch;
}
//
uint8_t getHexData(uint8_t *ch )
{
    uint8_t rD;
    if((rD=asciiToBin(getchar()))==0xFF) return 0xFF;
    *ch = (rD<<4);
    if((rD=asciiToBin(getchar()))==0xFF) return 0xFF;
    *ch +=rD;
    return 0;
}
//
void hPut(uint8_t c)
{
    if(c<10) c+='0';
    else c+='A'-10;
    putchar(c);
}
//
void hexPrint(uint8_t c)
{
    hPut(c>>4);
    hPut(c&0x0F);
}
//
void prtBuf(__xdata uint8_t* buf,uint8_t cnt)
{
    putchar('\n');
    while(cnt--) hexPrint(*buf++);
    putchar('\n');
}
//
uint8_t checkEnd(uint8_t ch)
{
    if(ch==0x0A || ch==0x0D || \
            ch==0x09 || ch==' '  || \
            ch==','  || ch==';'  || \
            ch==':') return 1;
    else return 0;
}
uint32_t getDigit(void)
{
    uint8_t ch,d,state=0;
    uint32_t Digit=0;
    while(1)
    {
        ch=getchar();
//        printf("%c",ch)
        switch (state)
        {
        case 0:
            if(!checkEnd(ch))
            {
                if(ch=='x' || ch=='X')
                {
                    state=2;
                    break;
                }
                else state=1;
            }
            else break;
        case 1: // decimal
            if(checkEnd(ch)) return Digit;
            else
            {
                if(ch=='x' || ch=='X')
                {
                    Digit = 0;
                    state=2;
                    break;
                }
                d=asciiToBin(ch);
                if(d <10)
                {
                    Digit *=10;
                    Digit+=d;
                }
                else return Digit;
            }
            break;
        case 2: // hex
            if(checkEnd(ch)) return Digit;
            d=asciiToBin(ch);
            if(d!= 0xFF)
            {
                Digit*=16;
                Digit+=d;
            }
            else return Digit;
            break;
        default:
            break;
        }
    }
}
//
int putchar(int c)
{
    while (!putUSB(c));
    return 0;
}
//
char getchar(void)
{
    uint8_t ch;
    while(!getUSB(&ch));
    return ch;
}
//
uint8_t IntelHex(void)
{
//    uint8_t hexVal,ihLength,ihCheckSum;
#define ihBuf       OsParam.flashBuf
#define ihLength    OsParam.wrk.d8[0]
#define ihCheckSum  OsParam.wrk.d8[1]
#define hexVal      OsParam.wrk.d8[2]

    while(getchar() != ':');
    if(getHexData(&hexVal)) return 1;
    *ihBuf++ = ihCheckSum = hexVal;
    ihLength=hexVal + 2 + 1 + 1;
    while( ihLength--)
    {
        if(getHexData(&hexVal)) return 1;
        *ihBuf++ = hexVal;
        ihCheckSum += hexVal;
    }
    return ihCheckSum;
}
//
void printHelp(void)
{
    printf("\nCC2511 USB Flash Loader\n");
    printf(" H - Help\n");
    printf(" U - USB-RS232 (Ctrl-Q end)\n");
    printf(" P - MCU FLASH LOADER\n");
//    printf(" R - USB-RADIO (Ctrl-Q end)\n");
//    printf(" S - USB-SPI (Ctrl-Q end)\n");
    printf(" B - Self BootLoader\n");
    printf(" Q - REBOOT\n");
}
//
#endif // UTIL_H_INCLUDED
