CC = sdcc
RM = del 
#rm -f

CFLAGS = --model-small --opt-code-speed

# NOTE: code-loc should be the same as the value specified for
# USER_CODE_BASE in the bootloader!
LDFLAGS_FLASH = \
	--out-fmt-ihx \
	--code-loc 0x000 --code-size 0x8000 \
	--xram-loc 0xf000 --xram-size 0x1000 \
	--iram-size 0x100

ifdef DEBUG
CFLAGS += --debug
endif
TARGET=UsbFlash
SRC = $(TARGET)

ADB=$(SRC:.c=.adb)
ASM=$(SRC:.c=.asm)
LNK=$(SRC:.c=.lnk)
LST=$(SRC:.c=.lst)
REL=$(SRC:.c=.rel)
RST=$(SRC:.c=.rst)
SYM=$(SRC:.c=.sym)

PROGS=$(TARGET).hex
PCDB=$(PROGS:.hex=.cdb)
PLNK=$(PROGS:.hex=.lnk)
PMAP=$(PROGS:.hex=.map)
PMEM=$(PROGS:.hex=.mem)
PAOM=$(PROGS:.hex=)

%.rel : %.c
	$(CC) -c $(CFLAGS) -o$*.rel $<

all: $(PROGS)

$(TARGET).hex: $(REL) Makefile
	$(CC) $(LDFLAGS_FLASH) $(CFLAGS) -o $(TARGET).ihx $(REL).rel
	srec_cat -O $(TARGET).hex -I $(TARGET).ihx -I

clean:
	$(RM)  $(ADB) $(ASM) $(LNK) $(LST) $(REL) $(RST) $(SYM)
	$(RM)  $(PROGS) $(PCDB) $(PLNK) $(PMAP) $(PMEM) $(PAOM) *.ihx

