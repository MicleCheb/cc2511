//
#include <stdint.h>
#include <stdio.h>
#include "cc1111.h"

#include "Board.h"
#include "Util.h"
#include "Uart.h"
#include "UsbCDC.h"
//
#include "BootLoader.h"
//#include "UsbMCS51.h"
//
//#include "UsbBsl.h"
//#include "UsbSwdArm.h"
//#include "Frequency.h"
//
//__code struct FlashProg_t FlashProg[4]=
//{
//    {initFlash,freeFlash,writeFlash,readFlash,exitFlash,},
//    {mcs51DbgInit,mcs51ChipErase,mcs51WriteFlash,mcs51ReadMemory,mcs51Exit,},
//    {swdArmInit,swdArmErase,swdArmWriteFlash,swdArmReadMemory,swdArmExit,},
//    {bsl430Init,bsl430PageErase,bsl430WriteFlash,bsl430ReadMemory,bsl430Exit,},
//};
#include "UsbFlash.h"
void main (void)
{
    initDebugger();
    initUSB();
    EA = 1;
// Bring up the USB link
    USB_UP();
    Rled_off;
    Gled_off;
    OsParam.RedLedMask=MaskFind;
    OsParam.Flags=0;
    DelayAlarm(8*4);
    printHelp();
    while(1)
    {
        LedsWait();
        switch(getchar())
        {
        case 'B':
            OsParam.GreenLedMask=MaskNorma;
            OsParam.RedLedMask=MaskOff;
            OsParam.FlashMemFunc = (__code  struct FlashProg_t *)&BootL;
//            printf("\nError %u\n",UsbFlashProg());
            printf("\nError %u\n",Flasher());
//            return Flasher();
            FlushUsb();
            printHelp();
            break;
        case 'P':
            OsParam.GreenLedMask=MaskNorma;
            OsParam.RedLedMask=MaskOff;
            printf("\nError %u\n",UsbFlashProg());
            FlushUsb();
            printHelp();
            break;
//        case 'R':
//            printf("RF: Not implemented\n");
//            printHelp();
//            break;
        case 'H':
            printHelp();
            break;
        case 'U':
            OsParam.GreenLedMask=MaskUART;
            OsParam.RedLedMask=MaskOff;
            UsbUart();
//            FlushUsb();
            printHelp();
            break;
//        case 'S':
//            OsParam.GreenLedMask=MaskUART;
//            OsParam.RedLedMask=MaskOff;
//            UsbSpi();
////            FlushUsb();
//            printHelp();
//            break;
        case 'Q': /* Soft RESET  quit */
//            EA = 0;
//            WDCTL = (1 << 3); // enable 1 sec
//            while (1);
            break;
        default:
            break;
        }
        goToSleep();
    }
}
//
struct RTOSfunction_t __code __at (RTOSROM)  RTOSfunction =
{
    getUSB,
    putUSB,
    uartSet,
    Flasher,
    IntelHex,
    printf,
    getDigit,
};

