#ifndef UART_H_INCLUDED
#define UART_H_INCLUDED
#include <stdint.h>
#include <stdio.h>
#include "cc1111.h"

//struct CDC_LINE_CODING_t
//{
//    uint32_t dwDTERate;      ///* Data terminal rate */ {57600,0,0,8,2,}
//    uint8_t  bStopBits;       ///* Number of stop bits */
//    uint8_t  bParityType;     ///* Parity bit type */
//    uint8_t  bDataBits;       ///* Number of data bits */
//    uint8_t  bState;
//};
//Where
//dwDTERate	Baudrate in bits per second.
//bCharFormat	Number of stop bits (0 = 1 stop bit, 1 = 1.5 stop bits, 2 = 2 stop bits).
//bParityType	Parity type (0 = None, 1 = Odd, 2 = Even, 3 = Mark, 4 = Space).
//bDataBits	Number of data bits (5, 6, 7, 8, or 16).
//bState Serial State     bit 0: DTR state
//                        bit 1: RTS state
//
//union cdcData_t
//{
//    struct CDC_LINE_CODING_t;
//    uint8_t b8[8];
//};
// CDC definitions
#define CDC_CHAR_FORMAT_1_STOP_BIT     0
#define CDC_CHAR_FORMAT_1_5_STOP_BIT   1
#define CDC_CHAR_FORMAT_2_STOP_BIT     2

#define CDC_PARITY_TYPE_NONE           0
#define CDC_PARITY_TYPE_ODD            1
#define CDC_PARITY_TYPE_EVEN           2
#define CDC_PARITY_TYPE_MARK           3
#define CDC_PARITY_TYPE_SPACE          4

void UsbUart(void);
uint8_t uartSet(void);
void initUart(void);
void disableUart(void);
void putUART(uint8_t c);
uint16_t getUART(void);
void printUart(void);
void UsbSpi(void);
void initSPI(void);
void disableSPI(void);
uint8_t spiTxRx(uint8_t c);
#define DTR_STATE  (1<<0)
#define RTS_STATE  (1<<1)

#define ENABLE_TX_UART()    {IEN2|=IEN2_UTX0IE;}
#define DISABLE_TX_UART()   {IEN2&=~IEN2_UTX0IE;}
#define ENABLE_RX_UART()    {URX0IE=1;}
#define DISABLE_RX_UART()   {URX0IE=0;}

#define SPI_DISABLE     P0_0=1
#define SPI_ENABLE      P0_0=0
#define UART_DISABLE    P1_3=1
#define UART_ENABLE     P1_3=0

#define DTR             P1_1
#define RTS             P1_7

#define SPI_CSN         P0_2
//
void URX0_isr (void) __interrupt (URX0_VECTOR) //__using (1)
{
//    USART0 RX Complete
//     It's a little tricky and might not be well explained in the datasheet,
//      but when using 9bit mode and parity is disabled one will actually
//      have to use a combination of the D9 bit and ERR bit in UxCSR.
//
//    If the received 9th bit is different from the content in D9, err will be set.
//    If the received 9th bit is equal the content in D9, err will be cleared.
//    uint8_t c=U0DBUF;
    uartBuf[OsParam.uartIn++]=U0DBUF;
}
//
void putUART(uint8_t c)
{
    U0DBUF = c;
    while(!(U0CSR & UxCSR_TX_BYTE));
    U0CSR &= ~UxCSR_TX_BYTE;
//    printf("%02X",c);
}
//
uint16_t getUART(void)
{
    while((U0CSR & UxCSR_RX_BYTE)==0);
    U0CSR &= ~UxCSR_RX_BYTE;
    return U0DBUF;
}
//
uint8_t uartSet(void)
{
//    uint32_t b;
#define  b  OsParam.wrk.d32
    uint8_t k;
//    U0UCR  = UxUCR_FLUSH;
    U0CSR  = UxCSR_MODE_UART|UxCSR_RE; // UART mode + Receiver enabled
    U0UCR  = UxUCR_FLUSH |UxUCR_FLOW_DISABLE|UxUCR_STOP_HIGH; //
    if(CDC_LINE_CODING.bDataBits==9)   U0UCR  |= UxUCR_BIT9_9_BITS;
    if(CDC_LINE_CODING.bStopBits==CDC_CHAR_FORMAT_2_STOP_BIT) U0UCR  |= UxUCR_SPB_2_STOP_BITS;
    if(CDC_LINE_CODING.bParityType==CDC_PARITY_TYPE_ODD)
    {
        U0UCR  |=UxUCR_PARITY_ENABLE|UxUCR_D9_ODD_PARITY|UxUCR_BIT9_9_BITS;
    }
    else if(CDC_LINE_CODING.bParityType==CDC_PARITY_TYPE_EVEN)
    {
        U0UCR  |=UxUCR_PARITY_ENABLE|UxUCR_D9_EVEN_PARITY|UxUCR_BIT9_9_BITS;
    }
    else
    {
        U0UCR  |=UxUCR_PARITY_DISABLE|UxUCR_BIT9_8_BITS;
    }
//    |UxUCR_FLOW_DISABLE|UxUCR_PARITY_DISABLE|UxUCR_SPB_1_STOP_BIT|UxUCR_BIT9_8_BITS;
    //|(1 << 7) | (0 << 6) | (1 << 1); // flush no RTS-CTS flow UxUCR|  # define UxUCR_D9_EVEN_PARITY           (0 << 5)
//# define UxUCR_D9_ODD_PARITY            (1 << 5)
#define U_E (LOG2(BAUD*2^28/FSYS/256))
#define U_M (2^28*BAUD/FSYS/2^U_E - 256)
    b=CDC_LINE_CODING.dwDTERate;
    k=0;
    while(b < FSYS)
    {
        k++;
        b<<=1;
    };
    U0GCR  = (20-k);   //14; // 24MHz : 16
    U0BAUD =(b/(FSYS>>8))&0x000000FF;   //163;
    U0DBUF = 13;
//    printUart();
    return 0;
}
//
void initUart(void)
{
    UART_DISABLE;
    P1DIR|=(1<<3);
//    Configure UART0 Alt2 SPI1 Alt1
//    USART0 Alt2 Rx   Tx   PIN8 - RX Pin10 - TX
//                P1_4 P1_5
//                P0_5 P0_4 P0_3 P0_2
//    SPI1   Alt1 MI   MO   CL   SS
    PERCFG = PERCFG_U0CFG_ALT_2|PERCFG_U1CFG_ALT_1;
    P1SEL |= (BIT5+BIT4);
//
    URX0IE=0; // USART0 RX interrupt enable
    OsParam.uartIn=OsParam.uartOut=0;
    IEN2&=~IEN2_UTX0IE;// USART0 TX interrupt disable

    CDC_LINE_CODING.dwDTERate = 115200;
    CDC_LINE_CODING.bStopBits = 0;
    CDC_LINE_CODING.bParityType = 0;
    CDC_LINE_CODING.bDataBits = 8;
//    CDC_LINE_CODING.bState = 0; //RTS_STATE;
    uartSet();
//    UART_ENABLE;
}
//
//void initSPI(void)
//{
//    SPI_DISABLE;//=YES;
//    P0DIR|=(1<<0);
//    // Configure UART0 Alt2 SPI1 Alt1
//    PERCFG = PERCFG_U0CFG_ALT_2|PERCFG_U1CFG_ALT_1;
//    /*
//    USART1 Alt1 Rx   Tx   Rt   Ct
//                P0_5 P0_4 P0_3 P0_2
//    SPI1   Alt1 MI   MO   CL   SS
//    */
////#define BAUD 115200
//// UART1 BAUD,8,1,N
//// PORT0 4 Tx  out
////       5 Rx  in
////       2 Ct  in
////       3 Rt  out
//    P0SEL |= (BIT5+BIT4+BIT3);
//    SPI_CSN=1;
//    P0DIR |= BIT2;
//    URX1IE=0; // SPI RX interrupt disable
//    IEN2 &= ~IEN2_UTX1IE;// USART1 TX interrupt disable
//// Set USART to SPI mode and Master mode
//    U1CSR = UxCSR_MODE_SPI | UxCSR_MASTER;
//    U1BAUD = 0;
//// FSPI = FSYS/16
//    U1GCR = UxGCR_CPOL_NEGATIVE+UxGCR_CPHA_FIRST_EDGE+UxGCR_ORDER_MSB+16;
////    SPI_ENABLE;
//}
//
void UsbUart(void)
{
    uint8_t ch;
    printf("\nUART>start :");
    printUart();
    uartSet();
    UART_ENABLE;
    DelayAlarm(2);
    OsParam.Flags|=CdC;
    OsParam.uartIn=OsParam.uartOut=0;
//    U0CSR |= UxCSR_TX_BYTE;
    UTX0IF=0;
    U0DBUF =13;
    URX0IF=0;
    URX0IE=1;
    while(OsParam.Flags&CdC)
    {
// RX
        if(OsParam.uartOut != OsParam.uartIn) OsParam.uartOut += putUSB(uartBuf[OsParam.uartOut]);
// TX
        if(U0CSR&UxCSR_TX_BYTE)
        {
            if(getUSB(&ch))
            {
                if(ch==0x11) // Ctrl-Q
                {
                    OsParam.Flags &= ~CdC;
                }
                else
                {
                    U0CSR &= ~UxCSR_TX_BYTE;
                    U0DBUF = ch;
                }
            }
        };
    };
    URX0IE=0;
    UART_DISABLE;
    U0CSR &= ~(UxCSR_MODE_UART|UxCSR_RE);
    printf("\nUART>end\n");
}
//
//void UsbSpi(void)
//{
//    uint8_t ch;
//    UART_DISABLE;
//    OsParam.Flags|=SpI;
//    printf("SPI>start Master MSB 1500 kBit\n");
//    SPI_ENABLE;
//    while(OsParam.Flags&SpI)
//    {
//        if(getUSB(&ch))
//        {
//            if(ch==0x11) // Ctrl-Q
//            {
//                OsParam.Flags &= ~SpI;
//            }
//            else
//            {
//                ch=spiTxRx(ch);
//                while(putUSB(ch)==0);
//            }
//        }
//    }
//    SPI_DISABLE;
//    printf("\nSPI>end\n");
////    uartDTR=1;//CDC_LINE_CODING.bState&0x01;
////    uartRTS=0;//CDC_LINE_CODING.bState&0x02;
//}
//
//uint8_t spiTxRx(uint8_t c) //__using (1)
//{
////    Gled_on;
//    U1DBUF = c;
//    while(!(U1CSR & UxCSR_TX_BYTE));
//    U1CSR &= ~UxCSR_TX_BYTE;
////    Gled_off;
//    return U1DBUF;
//}
//
void printUart(void)
{
    printf(" %lu",CDC_LINE_CODING.dwDTERate);
    printf(" %u",CDC_LINE_CODING.bStopBits);
    printf(" %u",CDC_LINE_CODING.bParityType);
    printf(" %u",CDC_LINE_CODING.bDataBits);
//    printf(" %02X\n",CDC_LINE_CODING.bState);
}
//
#endif // UART_H_INCLUDED
