#ifndef BOARD_H_INCLUDED
#define BOARD_H_INCLUDED

#include <stdint.h>
#include <string.h>
#include "cc1111.h"

uint8_t Flasher(void) __reentrant;

#define FLASH_PAGE_SIZE (uint16_t)1024
#define FLASH_SIZE FLASH_PAGE_SIZE*32

#define USER_CODE_BASE 0x2400


#define XRDATA 0xF400
#define memAddr 0xF000

// Common Flags
#define SpI (1<<2)  // uart SPI
#define CdC (1<<4)  // CDC mode
#define PrG (1<<5)  // Flash Programming
#define FlT (1<<7)  // Ticks Timer
// JTAG Flags
#define TmS (1<<0)  // SBW TMS state
#define TdI (1<<1)  // SBW TDI bit
#define TdO (1<<2)  // SBW TDO bit
#define TcK (1<<6)  // SBW TCLK state
// SWD Flags
#define B32 (1<<4)  // SWD 32 BIT mode
#define IgN (1<<5)  // SWD ignore ACK
//

struct RTOSfunction_t
{
    uint8_t (*osGetUSB)(uint8_t *ch);
    uint8_t (*osPutUSB)(uint8_t ch);
    uint8_t (*osUartSet)(void);
    uint8_t (*osFlasher)(void)  __reentrant; //struct FlashProg_t *chipID
    uint8_t (*osIntelHex)(void);
    int     (*osPrintf)(const char *,...);
    uint32_t (*osGetDigit)(void);
};
#define RTOSROM (USER_CODE_BASE - sizeof(struct RTOSfunction_t))
//#define FUNCROM (USER_CODE_BASE - 16*sizeof(struct FlashProg_t))

struct FlashProg_t
{
    uint8_t ind;
    uint8_t desc;
    uint8_t __code *descriptor;
    uint8_t (*flInit)(void);
    uint8_t (*flUnProtect)(void);
    uint8_t (*flRead)(void);
    uint8_t (*flErase)(void);
    uint8_t (*flWrite)(void);
    uint8_t (*flVerify)(void);
    uint8_t (*flProtect)(void);
    uint8_t (*flExit)(void);
};
#define MAX_USER_PROGRAM 16
#define FUNCROM (FLASH_SIZE - MAX_USER_PROGRAM*3)
//struct FlashProg_t __code __at (FUNCROMTEST)  FlashProgTest[16];
union FlashAddr_t
{
    uint32_t A32;
    uint32_t LinAdr;
    uint16_t A16[2];
    uint8_t A8[4];
    uint16_t adr;
};
union wrk_t
{
    uint32_t d32;
    uint16_t d16[2];
    uint8_t d8[4];
};
struct OsParam_t
{
    uint8_t Alarm;
    uint8_t TicksMain;
    uint8_t Flags;
//
    uint8_t LedMask;
    uint8_t RedLedMask;
    uint8_t GreenLedMask;
//
    uint8_t usbState;
    uint8_t UsbInCnt;
    uint8_t UsbOutCnt;
//
    uint8_t uartIn;
    uint8_t uartOut;
    uint8_t tmpI;
//
    uint8_t __xdata *flashBuf;
    uint16_t flashCnt;
    union FlashAddr_t FlashAddr;

    union wrk_t wrk;
    struct FlashProg_t __code *FlashMemFunc;
} OsParam;

#define xBuf    OsParam.flashBuf
#define xCnt    OsParam.flashCnt
#define xAdr    OsParam.FlashAddr.adr

struct CDC_LINE_CODING_t
{
    uint32_t dwDTERate;       /// Data terminal rate {57600,0,0,8,2,}
    uint8_t  bStopBits;       /// Number of stop bits
    uint8_t  bParityType;     /// Parity bit type
    uint8_t  bDataBits;       /// Number of data bits
};

//union cdcData_t
//{
//    struct CDC_LINE_CODING_t;
//    uint8_t b8[8];
//};
struct usbSetupPacket_t
{
    uint8_t   bmRequestType;
    uint8_t   bRequest;
    uint16_t  wValue;
    uint16_t  wIndex;
    uint16_t  wLength;
};

enum usbState_t {stSetup,stTx_In,stRx_Out,stCDC=0x80};

__xdata uint8_t  uartBuf[256]; // __at 0xF000
__xdata struct cc_dma_channel dmaCfg; // __at 0xF100
__xdata struct CDC_LINE_CODING_t CDC_LINE_CODING; // __at 0xF108
__xdata struct usbSetupPacket_t usbSetupPacket; // __at 0xF110
__xdata uint8_t store[8]; // __at 0xF118

//__xdata union flPrg_t chipID;

#define  FOSC 24000000
#define  FSYS (FOSC/1)
#define  FLF  (FOSC/750)
#define   BoardFreq 64


#define NONE    0
#define YES     1
#define INPUT   0
#define OUTPUT  1

#define DBG_RESET       P0_1

#define DBG_DC_DIR      P1_2
#define DC              P1_1
#define DBG_DC          DC
#define DC_BIT          (1<<1)
#define DC_INPUT        {P1DIR&=~DC_BIT;DBG_DC_DIR=INPUT;}
#define DC_OUTPUT       {DBG_DC_DIR=OUTPUT;P1DIR|=DC_BIT;}

#define DBG_DD_DIR      P1_6
#define DD              P1_7
#define DBG_DD          DD
#define DD_BIT          (1<<7)
#define DD_INPUT        {P1DIR&=~DD_BIT;DBG_DD_DIR=INPUT;}
#define DD_OUTPUT       {DBG_DD_DIR=OUTPUT;P1DIR|=DD_BIT;}


/*
Table 1 - Supported SPI connections (marked OK)
        USART0, alt 1   USART1, alt 2
SCLK    P0.5            P1.5
CS      P0.4            P1.4
MOSI    P0.3            P1.6
MISO    P0.2            P1.7

DBG_DC_DIR      P1_2
DBG_DC          P1_1
USB_IFC_CTRL    P1_0
SPI_EN          P0_0
DBG_RESET       P0_1
SPI_CSN         P0_2
SPI_SCLK        P0_3
SPI_MOSI        P0_4
SPI_MISO        P0_5
LED_RED         P2_3
LED_GREEN       P2_4
UART_EN         P1_3
UART_TX         P1_4    INPUT for CC Debugger!! error
UART_RX         P1_5    OUTPUT for CC Debugger!! error
DBG_DD_DIR      P1_6
DBG_DD          P1_7

    CC Debugger Debug header
1  GND          2  CC
3  DC           4  DD
5  NC           6  NC
7  RESET        8  NC
9  NC           10 NC


    CC Debugger Target connector
1  GND          2  VDD_TARGET
3  DC           4  DD
5  CS           6  SCLK
7  RESET        8  MOSI/TX
9  VCC          10 MISO/RX


    CC1110 Debug Connector
1               2
3               4
5               6
7               8
9               10

    CC1110 COnnector
*/


#define MaskOff   0x00
#define MaskFind  0x27
#define MaskProg  0x23
#define MaskNorma 0x01
#define MaskUART  0x03
// Setup LED
#define Initled() {P2DIR |= (1<<3)|(1<<4);}
#define Rled_on   {P2_3 = 1;}
#define Rled_off  {P2_3 = 0;}
#define Rled_flp  {P2_3^= 1;}
#define Gled_on   {P2_4 = 1;}
#define Gled_off  {P2_4 = 0;}
#define Gled_flp  {P2_4^= 1;}
//
void ST_isr (void)    __interrupt (ST_VECTOR);
void P2INT_isr (void) __interrupt (P2INT_VECTOR);
void URX0_isr (void)  __interrupt (URX0_VECTOR);
void P0INT_isr (void) __interrupt (P0INT_VECTOR); //__using (1);
void PORT1_isr (void) __interrupt (P1INT_VECTOR);
//
void ProcessP2_isr(uint8_t flags);
void ProcessP0_isr(uint8_t flags);
void usbIrqResumeHandler(uint8_t flags);
void initUSB(void);
void usbISR(void);
void usbInProcess(void);
uint8_t  putUSB(uint8_t ch);
uint8_t getUSB(uint8_t *ch);
void FlushUsb(void);
void UsbUart(void);
void UsbSpi(void);
void initUart(void);
void initSPI(void);
//
uint8_t UsbFlashProg(void);
void FlasherHelp(void);
//uint8_t Flasher(void) __reentrant;
void printFlashHelp(void);
void LedsWait(void);
void SetAlarm(uint8_t Q);
void delay (uint8_t ticks);
void DelayAlarm(uint8_t ticks);
void initDebugger(void);
void goToSleep(void);
//
void PORT1_isr (void) __interrupt (P1INT_VECTOR)
{
    __asm
        ljmp #USER_CODE_BASE + #(3+8*P1INT_VECTOR)
    __endasm;
}
void ST_isr (void) __interrupt (ST_VECTOR) //__using (1)
{
//    uint8_t storeIndex,cnt;
    /*  Sleep Timer Compare                         */
//       Note that the order in which the following flags are cleared is important.
//       For pulse or egde triggered interrupts one has to clear the CPU interrupt
//       flag prior to clearing the module interrupt flag.
// Clear [IRCON.STIF] (Sleep Timer CPU interrupt flag)
    STIF = 0;
// Clear [WORIRQ.EVENT0_FLAG] (Sleep Timer peripheral interrupt flag)
    WORIRQ &= ~(1<<0);  // WORIRQ_EVENT0_FLAG;
    ++OsParam.TicksMain;
    Rled_off;
    Gled_off;
    if(OsParam.TicksMain%(BoardFreq/8)==1)
    {
        OsParam.LedMask<<=1;
        if(OsParam.LedMask==0) OsParam.LedMask=1;
        if(OsParam.LedMask&OsParam.RedLedMask) Rled_on;
        if(OsParam.LedMask&OsParam.GreenLedMask) Gled_on;
        if(OsParam.Alarm)
        {
            --OsParam.Alarm;
        }
    }
    if(OsParam.UsbInCnt>0 && OsParam.UsbInCnt<0xFF) usbInProcess();
//    if(UFI!=0) UserFunctionInterrupt();
}
//
void SetAlarm(uint8_t Q)
{
    OsParam.Alarm=Q;
}
//
void P2INT_isr (void) __interrupt (P2INT_VECTOR) //__using (1)
{
    uint8_t flags;
    flags = P2IFG;
//    USB_INT_CLEAR();
    P2IFG=0;
    P2IF=0;
    usbISR();
    /*  Port 2 Inputs                               */
    ProcessP2_isr(flags);
}
//
void P0INT_isr (void) __interrupt (P0INT_VECTOR) //__using (1)
{
/// Port 0 Inputs
//   usbIrqResumeHandler();
    uint8_t flags;
// First make sure that the crystal oscillator is stable
// Wait until the HS XOSC is stable.
    while( !(SLEEP & SLEEP_XOSC_STB) );
// Clear and disable the interrupt flag
    flags = P0IFG;
    P0IFG=0;
    P0IF=0;
//
    usbIrqResumeHandler(flags);
//
    ProcessP0_isr(flags);
}
//
void ProcessP0_isr(uint8_t flags)
{
    flags=0;
}
//
void ProcessP2_isr(uint8_t flags)
{
    flags=0;
}
//
void initSleep(void)
{
    uint8_t t;
//  Setup interrupt
// Clear interrupt flags
// Clear [IRCON.STIF] (Sleep Timer CPU interrupt flag)
//    STIF = 0;
//    // Clear [WORIRQ.EVENT0_FLAG] (Sleep Timer peripheral interrupt flag)
//    cbit(WORIRQ,0);  // WORIRQ_EVENT0_FLAG;
//    // Set individual interrupt enable bit in the peripherals SFR
//    sbit(WORIRQ,4); // WORIRQ_EVENT0_MASK;    // Enable interrupt mask for sleep timer
//    // Set the individual, interrupt enable bit [IEN0.STIE=1]
//    STIE = 1;
////Now the time between two consecutive Event 0s is decided by:
//       t = EVENT0 * 2^(5*WOR_RES) / 32768
//       By using EVENT0 = 32 and WOR_RES = 2, t = 1 s. So by using these values,
//       a Sleep Timer interrupt will be generated every second.
// Set [WORCTL.WOR_RES = 1]
//    WORCTRL |= 0;
//Must wait for 2 clock periods after resetting the Sleep Timer before
//setting EVENT0 value
// Reset timer and set EVENT0 value.
#define WOR_RES 0
    WORCTRL = 4 + WOR_RES;    //WORCTL_WOR_RESET; 32 kHz Clock
    t = WORTIME0;
    while(t == WORTIME0);   // Wait until a positive 32 kHz edge
    t = WORTIME0;
    while(t == WORTIME0);   // Wait until a positive 32 kHz edge
    WOREVT0 = (FLF/(1<<(5*WOR_RES))/BoardFreq)&0x00FF;    // 64 Herz freq
    WOREVT1 = (FLF/(1<<(5*WOR_RES))/BoardFreq)>>8;
    STIF = 0;
    WORIRQ &=~(1<<0);
    WORIRQ |= (1<<4);
    STIE = 1;
// Select Power Mode 1 (SLEEP.MODE = 0).
//    SLEEP |= 0;
    OsParam.TicksMain=0;
//    UsbFlags.Alarm=16;
//    OsParam.Flags=0;
//The system will now wake up when Sleep Timer interrupt occurs. When awake,
//       the system will start enter/exit Power Mode 0 using the loop below.
}
//
void goToSleep(void)
{
    uint8_t j;
// Alignment of entering PM{0 - 2} to a positive edge on the 32 kHz clock source
    j = WORTIME0;
    while(j == WORTIME0);    // Wait until a positive 32 kHz edge
//    SLEEP=0x04+0x00;
    PCON |= 1;          // Go into Power Mode
    nop;
}
//
void DelayAlarm(uint8_t ticks)
{
    OsParam.Alarm=ticks;
    while(OsParam.Alarm);
}
//
void delay (uint8_t ticks)
{
    uint8_t j;
    j=WORTIME0;
    while(ticks--)
    {
        while(j==WORTIME0);
        j=WORTIME0;
    }
}
//
void DBG_INIT(void)
{
// RESET
    P0DIR|=(1<<1);
    DBG_RESET=NONE;
// DC
    P1DIR|=(0<<1)|(1<<2);
    DBG_DC_DIR=INPUT;
    DC=0;
// DD
    P1DIR|=(0<<7)|(1<<6);
    DBG_DD_DIR=INPUT;
    DD=0;
}
//
void initDebugger(void)
{
    SLEEP = 0;              // HS XTAL 48MHz + HSRC
    CLKCON = (1<<7)|(0<<6)|(0<<3)|(0<<0);        // LFRC 32kHz
    while( !(SLEEP&(1<<6)));// HS XT 48MHz READY
    while( !(SLEEP&(1<<5)));// HS RC 12MHz READY
    SLEEP |= (1<<2);        // HSRC OFF
//
    Initled();
    Rled_on;
    Gled_on;
    OsParam.LedMask=1;
//
    DBG_INIT();
    initUart();
//    initSPI();
    initSleep();
}
//

#endif // BOARD_H_INCLUDED
