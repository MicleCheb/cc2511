#ifndef USBCDC_H_INCLUDED
#define USBCDC_H_INCLUDED

#include <stdint.h>
#include "cc1111.h"

#define USB_UP()        {P1DIR |= 1; P1_0 = 1;}
#define USB_DOWN()      {P1DIR |= 1; P1_0 = 0;}
#define USB_ENABLE()     SLEEP|=SLEEP_USB_EN

#define USB_PULLUP_ENABLE()    P2INP&=~(1<<7)
#define USB_PULLUP_DISABLE()   P2INP|=(1<<7)
#define USB_INT_ENABLE()       IEN2|=IEN2_USBIE
#define USB_INT_DISABLE()      IEN2&=~IEN2_USBIE
#define USB_INT_CLEAR()        {P2IFG=0; P2IF=0;}
#define USB_RESUME_INT_ENABLE()    {PICTL|= 0x10;PICTL&=~0x01;IEN1|=IEN1_P0IE;}
#define USB_RESUME_INT_DISABLE()   PICTL&=~0x10
#define USB_RESUME_INT_CLEAR()    {P0IFG=0; P0IF=0;}

void initUSB(void);
void usbISR(void);
void usbIrqResumeHandler(uint8_t flags);

void ep0Process(void);
void ep0Setup(void);
void ep0In_Tx(void);
void ep0Out_Rx(void);
void getControlLineState(void);

void usbGetDescriptor(void);
void usbStdDeviceReq(void);
void usbStdInterfaceReq(void);
void usbStdEndPointReq(void);

uint8_t  putUSB(uint8_t ch);
void usbInProcess(void);

uint8_t getUSB(uint8_t *ch);
void FlushUsb(void);

//USB related Constant
#define USB_CONTROL_EP      0
#define USB_INT_EP          1
#define CDC_EP              1
#define USB_OUT_EP          3
#define USB_IN_EP           3
#define USB_CONTROL_SIZE    32

#define EP_DOUBLE_BUFFER    0
#define USB_CDC_SIZE        16
#define USB_IN_SIZE         (64 >> EP_DOUBLE_BUFFER)
#define USB_OUT_SIZE        (64 >> EP_DOUBLE_BUFFER)
#define USB_DESC_DEVICE           1
#define USB_DESC_CONFIGURATION    2
#define USB_DESC_STRING           3
#define USB_DESC_INTERFACE        4
#define USB_DESC_ENDPOINT         5
#define USB_DESC_DEVICE_QUALIFIER 6
#define USB_DESC_OTHER_SPEED      7
#define USB_DESC_INTERFACE_POWER  8
//Bit definitions for DEVICE_REQUEST.bmRequestType
//Bit 7:   Data direction
#define USB_REQ_TYPE_OUTPUT     0x00    //0 = Host sending data to device
#define USB_REQ_TYPE_INPUT      0x80    //1 = Device sending data to host

//Bit 6-5: Type
#define USB_REQ_TYPE_MASK       0x60    //Mask value for bits 6-5
#define USB_REQ_TYPE_STANDARD   0x00    //00 = Standard USB request
#define USB_REQ_TYPE_CLASS      0x20    //01 = Class specific
#define USB_REQ_TYPE_VENDOR     0x40    //10 = Vendor specific

//Bit 4-0: Recipient
#define USB_REQ_TYPE_RECIP_MASK 0x1F    //Mask value for bits 4-0
#define USB_REQ_TYPE_DEVICE     0x00    //00000 = Device
#define USB_REQ_TYPE_INTERFACE  0x01    //00001 = Interface
#define USB_REQ_TYPE_ENDPOINT   0x02    //00010 = Endpoint
#define USB_REQ_TYPE_OTHER      0x03    //00011 = Other

//Values for DEVICE_REQUEST.bRequest
//Standard Device Requests
#define USB_REQ_GET_STATUS              0
#define USB_REQ_CLEAR_FEATURE           1
#define USB_REQ_SET_FEATURE             3
#define USB_REQ_SET_ADDRESS             5
#define USB_REQ_GET_DESCRIPTOR          6
#define USB_REQ_SET_DESCRIPTOR          7
#define USB_REQ_GET_CONFIGURATION       8
#define USB_REQ_SET_CONFIGURATION       9
#define USB_REQ_GET_INTERFACE           10
#define USB_REQ_SET_INTERFACE           11
#define USB_REQ_SYNCH_FRAME             12

//CDC CLASS Requests
#define USB_CDC_SET_LINE_CODING                 0x20
#define USB_CDC_GET_LINE_CODING                 0x21
#define USB_CDC_SET_CONTROL_LINE_STATE          0x22
#define USB_CDC_SEND_BREAK                      0x23
#define USB_CDC_SEND_ENCAPSULATED_COMMAND       0x00
#define USB_CDC_GET_ENCAPSULATED_COMMAND        0x01

//[in]	ctrl_bmp	control line settings bitmap:
//bit 0: DTR state
//bit 1: RTS state

/*
error status and line states:
bit 6: bOverRun
bit 5: bParity
bit 4: bFraming
bit 3: bRingSignal
bit 2: bBreak
bit 1: bTxCarrier (DSR line state)
bit 0: bRxCarrier (DCD line status)
*/
// constants corresponding to the various serial parameters
#define CDC_DTR			    0x01
#define CDC_RTS			    0x02
#define CDC_1_STOP		    0
#define CDC_1_5_STOP		1
#define CDC_2_STOP		    2
#define CDC_PARITY_NONE		0
#define CDC_PARITY_ODD		1
#define CDC_PARITY_EVEN		2
#define CDC_PARITY_MARK		3
#define CDC_PARITY_SPACE	4
#define CDC_DCD			    0x01
#define CDC_DSR			    0x02
#define CDC_BREAK		    0x04
#define CDC_RI			    0x08
#define CDC_FRAME_ERR		0x10
#define CDC_PARITY_ERR		0x20
#define CDC_OVERRUN_ERR		0x40
// CDC definitions
#define CS_INTERFACE        0x24
#define CS_ENDPOINT         0x25

#define CDC_CHAR_FORMAT_1_STOP_BIT     0
#define CDC_CHAR_FORMAT_1_5_STOP_BIT   1
#define CDC_CHAR_FORMAT_2_STOP_BIT     2

#define CDC_PARITY_TYPE_NONE           0
#define CDC_PARITY_TYPE_ODD            1
#define CDC_PARITY_TYPE_EVEN           2
#define CDC_PARITY_TYPE_MARK           3
#define CDC_PARITY_TYPE_SPACE          4
//
/*
bmRequestType specifies the direction of data flow, the type of request, and
the recipient.
Bit 7 (Direction) names the direction of data flow for data in the Data stage.
Host to device (OUT) or no Data stage is zero; device to host (IN) is 1.
Bits 6..5 (Request Type) specify whether the request is one of USB’s standard
requests (00), a request defined for a specific USB class (01), or a request
defined by a vendor-specific driver for use with a particular product or products
(10).
Bits 4..0 (Recipient) define whether the request is directed to the device
(00000) or to a specific interface (00001), endpoint (00010), or other element
(00011) in the device.
*/
//struct usbSetupPacket_t
//{
//    uint8_t   bmRequestType;
//    uint8_t   bRequest;
//    uint16_t  wValue;
//    uint16_t  wIndex;
//    uint16_t  wLength;
//};
//union usbData_t
//{
//    struct usbSetupPacket_t;
//    uint8_t b8[8];
//};
//
//enum usbState_t {stSetup,stTx_In,stRx_Out,stCDC=0x80};

//// Data structure for GET_LINE_CODING / SET_LINE_CODING class requests
//struct _CDC_LINE_CODING
//{
//    uint32_t dwDTERate;       /* Data terminal rate */
//    uint8_t  bStopBits;       /* Number of stop bits */
//    uint8_t  bParityType;     /* Parity bit type */
//    uint8_t  bDataBits;       /* Number of data bits */
//    uint8_t  bState;
//};
//
//Where
//dwDTERate	Baudrate in bits per second.
//bCharFormat	Number of stop bits (0 = 1 stop bit, 1 = 1.5 stop bits, 2 = 2 stop bits).
//bParityType	Parity type (0 = None, 1 = Odd, 2 = Even, 3 = Mark, 4 = Space).
//bDataBits	Number of data bits (5, 6, 7, 8, or 16).
//bState Serial State     bit 0: DTR state
//                        bit 1: RTS state
//
#include "cdcDesc.h"
//
//  USB resume interrupt handler
// This routine clears the USB resume interrupt flag,
//  and makes sure that MCU does not return to power
//  mode 1 again until the the suspend loop has been exited.
//
void usbIrqResumeHandler(uint8_t flags)  //__interrupt (P0INT_VECTOR)
{
    if (flags & P0IFG_USB_RESUME)
    {
// We have a USB_RESUME interrupt (which could also be a USB reset)
        USB_RESUME_INT_DISABLE();
//      usbsuspStopPm1();
    }
}
//
// This interrupt is shared with port 2,
// so when we hook that up, fix this
void usbISR(void) //__interrupt (P2INT_VECTOR) __using (1)
{
    uint8_t usbFlags;
    uint8_t storeIndex;
    storeIndex=USBINDEX;
// Wait until the HS XOSC is stable.
    while( !(SLEEP & SLEEP_XOSC_STB) );
// Special handling for reset interrupts
    usbFlags = USBCIF;
    if (usbFlags & USBCIF_RSTIF)
    {
        initUSB();
    }
    else
    {
// SetUp Packet
        usbFlags=USBIIF; //
        if(usbFlags&0x01)
        {
            usbFlags &= ~0x01;
            ep0Process();
        }
// send data to Host (IBM PC )
        if(usbFlags & (1<<USB_IN_EP))
        {
            USBINDEX = USB_IN_EP;
            if(( USBCSIL&USBCSIL_INPKT_RDY)==0)
            {
                if(OsParam.UsbInCnt==0xFF) OsParam.UsbInCnt=0; // Double Buffering
            }
        };
// receive data from Host (IBM PC)
        usbFlags = USBOIF; //
        if(usbFlags & (1<<USB_OUT_EP))
        {
            USBINDEX=USB_OUT_EP;
            if(USBCSOL& USBCSOL_OUTPKT_RDY)
                OsParam.UsbOutCnt=USBCNTL;
        }
    }
    USBINDEX=storeIndex;
}
//
void usbInProcess(void)
{
    uint8_t storeIndex,status;
    if(OsParam.UsbInCnt==0 )  return;
    storeIndex=USBINDEX;
    USBINDEX = USB_IN_EP;
    if(( USBCSIL&USBCSIL_INPKT_RDY)==0)
    {
        if(OsParam.UsbInCnt!=0xFF)
        {
            USBCSIL|=USBCSIL_INPKT_RDY;
            status=USBCSIL&USBCSIL_INPKT_RDY;
            if(!status) OsParam.UsbInCnt=0;
            else  OsParam.UsbInCnt=0xFF;
        }
//        else UsbInCnt=0;
    }
    USBINDEX=storeIndex;
}
//
uint8_t putUSB(uint8_t ch)
{
    uint8_t storeIndex,status;
    storeIndex=USBINDEX;
    USBINDEX = USB_IN_EP;
    if( USBCSIL&USBCSIL_INPKT_RDY)
    {
        USBINDEX=storeIndex;
        return 0;
    }
    EA=0;
    USBFIFO[USB_IN_EP << 1]=ch;
    if(++OsParam.UsbInCnt > (USB_IN_SIZE -2) )
    {
        USBCSIL|=USBCSIL_INPKT_RDY;
        status=USBCSIL&USBCSIL_INPKT_RDY;
        if(!status) OsParam.UsbInCnt=0;
        else  OsParam.UsbInCnt=0xFF;
    }
    EA=1;
    USBINDEX=storeIndex;
    return 1;
}
//
uint8_t getUSB(uint8_t *ch)
{
    uint8_t storeIndex;
    if(OsParam.UsbOutCnt==0) return 0;
    *ch=USBFIFO[USB_OUT_EP << 1];
    if(--OsParam.UsbOutCnt == 0)
    {
        storeIndex = USBINDEX;
        USBINDEX = USB_OUT_EP;
        if(USBCSOL& USBCSOL_OUTPKT_RDY)
            USBCSOL &= ~USBCSOL_OUTPKT_RDY;
        USBINDEX=storeIndex;
    }
    return 1;
}
//
//
void FlushUsb(void)
{
    uint8_t ch;
#define flushTime 2
    OsParam.Alarm=flushTime;
    do
    {
        if(OsParam.UsbOutCnt)
        {
            while(getUSB(&ch));
            OsParam.Alarm=flushTime;
        }
    }
    while(OsParam.Alarm);
}
//
void initUSB(void)
{
    OsParam.UsbInCnt=0;
    OsParam.UsbOutCnt=0;
// Turn on the USB controller
    USB_ENABLE();
    USBINDEX=0;
    USBADDR=0;
    USBCS0=0;
// Select IRQ flags to handle
// IN interrupts on the control an IN endpoints
    USBIIE = (1 << USB_CONTROL_EP);
    OsParam.usbState=stSetup;
//    CDC_OsParam.Flags&=~UsB;
// OUT interrupts on the OUT endpoint
    USBOIE = 0;
// Only care about reset
//    Start-Of-Frame interrupt disabled
//    Reset interrupt enable
//    Resume interrupt disabled
//    Suspend interrupt disabled
    USBCIE = USBCIE_RSTIE;
// Configure P0 for rising edge detection on P0[7:4],
// but keep the interrupt disabled until it is needed.
//    PICTL |= 0x10;
//    PICTL &= ~(1<<0);
    USBPOW=0;
    USB_RESUME_INT_DISABLE();
    USB_RESUME_INT_CLEAR();
// CDC interrupt EP
    USBINDEX = CDC_EP;
    USBMAXI = USB_CDC_SIZE >> 3;
    USBMAXO =  0;
//    USBCSIH = 0x20; //|USBCSIL_SEND_STALL
// Set the IN max packet size, no double buffered
    USBINDEX  = USB_IN_EP;
    USBMAXI   = USB_IN_SIZE>>3;
    USBCSIH  |= USBCSIH_IN_DBL_BUF*EP_DOUBLE_BUFFER;//+USBCSIH_AUTOSET;
// Set the OUT max packet size
    USBINDEX = USB_OUT_EP;
    USBMAXO  = USB_OUT_SIZE>>3;
    USBCSOH  = USBCSOH_OUT_DBL_BUF*EP_DOUBLE_BUFFER; //USBCSOH_AUTOCLEAR;//+
// Clear any pending interrupts
    USBCIF = 0; // 0xDE06: USBCIF - Common USB Interrupt UsbFlash.Flags
    USBOIF = 0; // 0xDE04: USBOIF - Out Endpoints Interrupt UsbFlash.Flags
    USBIIF = 0; // 0xDE02: USBIIF - IN Endpoints and EP0 Interrupt UsbFlash.Flags
//
    USB_INT_CLEAR();
//    USB_INT_DISABLE();
//    USBIF  = 0; // * USB interrupt flag (shared with Port2) *
    USB_INT_ENABLE();
}
//
void ep0Process(void)
{
    uint8_t EP0flag;
    uint8_t storeIndex;
    storeIndex=USBINDEX;
    /*
    BIT7 CLR_SETUP_END 0 R/W Set this bit to 1 to de-assert the SETUP_END bit
        of this register. This bit will be H0 cleared automatically.
    BIT6 CLR_OUTPKT_RDY 0 R/W Set this bit to 1 to de-assert the OUTPKT_RDY bit
        of this register. This bit will H0 be cleared automatically.
    BIT5 SEND_STALL 0 R/W  H0 Set this bit to 1 to terminate the current transaction.
        The USB controller will send the STALL handshake and this bit will be de-asserted.
    BIT4 SETUP_END 0 R This bit is set if the control transfer ends due to a premature end of control
        transfer. The FIFO will be flushed and an interrupt request (EP0) will be
        generated if the interrupt is enabled. Setting CLR_SETUP_END=1 will deassert this bit
    BIT3 DATA_END 0 R/W This bit is used to signal the end of a data transfer and must be asserted in
                 H0 the following three situations:
        1 When the last data packet has been loaded and USBCS0.INPKT_RDY is set to 1
        2 When the last data packet has been unloaded and USBCS0.CLR_OUTPKT_RDY is set to 1
        3 When USBCS0.INPKT_RDY has been asserted without having loaded the FIFO (for sending a zero length data packet).
        The USB controller will clear this bit automatically
    BIT2 SENT_STALL 0 R/W H1  This bit is set when a STALL handshake has been sent. An interrupt request
        (EP0) will be generated if the interrupt is enabled This bit must be cleared from firmware.
    BIT1 INPKT_RDY 0 R/W H0 Set this bit when a data packet has been loaded into the EP0 FIFO to notify
        the USB controller that a new data packet is ready to be transferred. When data packet has been sent,
        this bit is cleared and an interrupt request (EP0) will be generated if the interrupt is enabled.
    BIT0 OUTPKT_RDY 0 R Data packet received. This bit is set when an incoming data packet has been
        placed in the OUT FIFO. An interrupt request (EP0) will be generated if the interrupt is enabled.
        Set CLR_OUTPKT_RDY=1 to de-assert this bit.
    */
    USBINDEX = 0;
// Error
    EP0flag=USBCS0;
    if (EP0flag & USBCS0_SETUP_END)
    {
        USBCS0 = USBCS0_CLR_SETUP_END;
        OsParam.usbState=stSetup;
    }
// Busy
    if (EP0flag & USBCS0_SENT_STALL)
    {
        USBCS0 = 0;
        OsParam.usbState=stSetup;
    }
// main loop
    switch(OsParam.usbState)
    {
    case stSetup:
        if(EP0flag&USBCS0_OUTPKT_RDY) ep0Setup();
        break;
    case stRx_Out:  // receive data from HOST
        if(EP0flag&USBCS0_OUTPKT_RDY) ep0Out_Rx();
        break;
    case stTx_In: // Send Descriptor to Host
        if((EP0flag&USBCS0_INPKT_RDY)==0) ep0In_Tx();
        break;
    default:
        USBCS0 = USBCS0_SEND_STALL;
        OsParam.usbState=stSetup;
        break;
    }
    USBINDEX=storeIndex;
}
//
void  usbStdDeviceReq(void)
{
    switch (usbSetupPacket.bRequest)
    {
    case USB_REQ_GET_STATUS:
        USBFIFO[0]=0;
        USBFIFO[0]=0;
        USBCS0 = USBCS0_INPKT_RDY|USBCS0_DATA_END;
        break;
    case USB_REQ_SET_FEATURE:
        USBCS0 = USBCS0_INPKT_RDY|USBCS0_DATA_END;
        break;
    case USB_REQ_CLEAR_FEATURE:
        USBCS0 = USBCS0_INPKT_RDY|USBCS0_DATA_END;
        break;
    case USB_REQ_SET_ADDRESS:
        USBADDR=(uint8_t)usbSetupPacket.wValue;
        USBCS0 = USBCS0_INPKT_RDY|USBCS0_DATA_END;
        break;
    case USB_REQ_GET_DESCRIPTOR:
        usbGetDescriptor();
        if(usbSetupPacket.wLength > 0)
        {
            OsParam.usbState=stTx_In;
            ep0In_Tx();
        }
        else USBCS0 = USBCS0_SEND_STALL;
        break;
    case USB_REQ_SET_DESCRIPTOR:
        USBCS0 = USBCS0_SEND_STALL;
        break;
    case USB_REQ_GET_CONFIGURATION:
        if(USBOIE) USBFIFO[0]=1;
        else USBFIFO[0]=0;
        USBCS0  = USBCS0_INPKT_RDY|USBCS0_DATA_END;
        break;
    case USB_REQ_SET_CONFIGURATION:
        if(usbSetupPacket.wValue==0)
        {
            USBIIE = (1 << USB_CONTROL_EP);
            USBOIE=0;
            USBCS0 = USBCS0_INPKT_RDY|USBCS0_DATA_END;
        }
        else  if(usbSetupPacket.wValue==1)
        {
            //            usbSendOK();
//            setControlLineState();
            USBIIE = (1 << USB_CONTROL_EP)|(1<<USB_IN_EP)|(1<<USB_INT_EP);
            USBOIE = (1<<USB_OUT_EP);
            USBCS0 = USBCS0_INPKT_RDY|USBCS0_DATA_END;
        }
        else USBCS0 = USBCS0_SEND_STALL;
        break;
    default:
        USBCS0 = USBCS0_SEND_STALL;
        break;
    }
}
//
void  usbStdInterfaceReq(void)
{
    switch (usbSetupPacket.bRequest)
    {
    case USB_REQ_GET_STATUS:
        USBFIFO[0]=0;
        USBFIFO[0]=0;
        USBCS0 = USBCS0_INPKT_RDY|USBCS0_DATA_END;
        break;
    case USB_REQ_SET_FEATURE:
        USBCS0 = USBCS0_INPKT_RDY|USBCS0_DATA_END;
        break;
    case USB_REQ_CLEAR_FEATURE:
        USBCS0 = USBCS0_INPKT_RDY|USBCS0_DATA_END;
        break;
    case USB_REQ_GET_INTERFACE:
        USBFIFO[0]=0;
        USBCS0  = USBCS0_INPKT_RDY|USBCS0_DATA_END;
        break;
    case USB_REQ_SET_INTERFACE:
        if(usbSetupPacket.wValue==0)
            USBCS0 = USBCS0_INPKT_RDY|USBCS0_DATA_END;
        else USBCS0 = USBCS0_SEND_STALL;
        break;
    default:
        USBCS0 = USBCS0_SEND_STALL;
        break;
    }
}
//
void  usbStdEndPointReq(void)
{
    switch (usbSetupPacket.bRequest)
    {
    case USB_REQ_GET_STATUS:
        USBFIFO[0]=0;
        USBFIFO[0]=0;
        USBCS0 |= USBCS0_INPKT_RDY|USBCS0_DATA_END;
        break;
    case USB_REQ_SET_FEATURE:
        USBCS0 = USBCS0_INPKT_RDY|USBCS0_DATA_END;
        break;
    case USB_REQ_CLEAR_FEATURE:
        USBCS0 = USBCS0_INPKT_RDY|USBCS0_DATA_END;
        break;
    case USB_REQ_SYNCH_FRAME:
        USBCS0 = USBCS0_SEND_STALL;
        break;
    default:
        USBCS0 = USBCS0_SEND_STALL;
        break;
    }
}
//
void ep0Setup(void)
{
    uint8_t cnt;
    __xdata uint8_t *buf;
//    uint8_t j;//*ep0Data;
    if((cnt=USBCNT0)!=8) return;
    buf = (uint8_t *) &usbSetupPacket;
    for(cnt=0; cnt<8; cnt++) buf[cnt]=USBFIFO[0];
    USBCS0 = USBCS0_CLR_OUTPKT_RDY;//|USBCS0_DATA_END;
    switch (usbSetupPacket.bmRequestType & USB_REQ_TYPE_MASK)
    {
//     Standard Requests
    case USB_REQ_TYPE_STANDARD:
        switch (usbSetupPacket.bmRequestType & 0x1F)
        {
        case USB_REQ_TYPE_DEVICE:
            usbStdDeviceReq();
            break;
        case USB_REQ_TYPE_INTERFACE:
            usbStdInterfaceReq();
            break;
        case USB_REQ_TYPE_ENDPOINT:
            usbStdEndPointReq();
            break;
        default:
            USBCS0 = USBCS0_SEND_STALL;
            break;
        }
        break;
// CDC
    case USB_REQ_TYPE_CLASS:
// Check Control interface
        if(usbSetupPacket.wIndex!=0)
        {
            USBCS0 = USBCS0_SEND_STALL;
            break;
        }
//
        switch (usbSetupPacket.bRequest)
        {
        case USB_CDC_GET_LINE_CODING:
        {
//            ep0Data = (uint8_t *) &CDC_LINE_CODING;
            buf = (__xdata uint8_t *)&CDC_LINE_CODING;
            for(cnt=0; cnt<7; cnt++) USBFIFO[0]=buf[cnt];
            USBCS0  = USBCS0_INPKT_RDY|USBCS0_DATA_END;
        }
        break;
        case USB_CDC_SET_LINE_CODING:
        {
//                setControlLineState();
            OsParam.usbState = stRx_Out;
//            OsParam.Flags &= ~SpI;
        }
        break;
        case USB_CDC_SET_CONTROL_LINE_STATE:        // it's work!
        {
//bit1
//Carrier control for half duplex modems. This signal corresponds to V.24 signal 105 and
//RS232 signal RTS.
//0: Deactivate carrier.
//1: Activate carrier.
//The device ignores the value of this bit when operating in full duplex mode.
//bit0
//Indicates to DCE if DTE is present or not.This signal corresponds to V.24 signal 108/2
//and RS232 signal DTR.
//0: DTE is not present.
//1: DTE is present
//            CDC_LINE_CODING.bState=usbSetupPacket.wValue;
            if(usbSetupPacket.wValue&CDC_DTR) DTR=0;
            else DTR=1;
            if(usbSetupPacket.wValue&CDC_RTS) RTS=0;
            else RTS=1;
//            setControlLineState();
            USBCS0  = USBCS0_INPKT_RDY|USBCS0_DATA_END;
        }
        break;
        case USB_CDC_SEND_BREAK:
        {
            USBCS0  = USBCS0_INPKT_RDY|USBCS0_DATA_END;
        }
        break;
        default:
//            sendError();
            USBCS0 = USBCS0_SEND_STALL;
            break;
        }
        break;
//
    case USB_REQ_TYPE_VENDOR:
        USBCS0 = USBCS0_SEND_STALL;
        break;
    default:
        USBCS0 = USBCS0_SEND_STALL;
        break;
    }
}
//
void ep0Out_Rx(void)
{
    uint8_t cnt;
    __xdata uint8_t *buf;
    if(usbSetupPacket.bRequest==USB_CDC_SET_LINE_CODING)
    {
        buf = (__xdata uint8_t *) (&CDC_LINE_CODING);
        for(cnt=0; cnt<sizeof(struct CDC_LINE_CODING_t); cnt++) buf[cnt]=USBFIFO[0];
        uartSet();
    }
    USBCS0 = USBCS0_CLR_OUTPKT_RDY|USBCS0_DATA_END;
    OsParam.usbState=stSetup;
}
//
void ep0In_Tx(void) //usbDescLength,usbDescIndex;
{
//RCV 80 06 0001 0000 1200
//SND 12011001000000205104a416000101020301
    uint8_t cnt;
    cnt=usbSetupPacket.wLength;
    if(cnt==0)
    {
        OsParam.usbState=stSetup;
    }
    else    // send rest of packet
    {
        if(cnt > USB_CONTROL_SIZE) cnt=USB_CONTROL_SIZE;
        if(cnt < USB_CONTROL_SIZE)
        {
            OsParam.usbState=stSetup;
        }
        usbSetupPacket.wLength -= cnt;
        while(cnt--) USBFIFO[0]=CC1111descriptor[usbSetupPacket.wIndex++];
    }
    if(OsParam.usbState==stSetup) USBCS0 = USBCS0_INPKT_RDY|USBCS0_DATA_END; // Send ZLP
    else USBCS0 = USBCS0_INPKT_RDY;
}
//
void getControlLineState(void)
{
// CDC EP

    uint8_t storeIndex;
    storeIndex = USBINDEX;
    USBINDEX=CDC_EP;
    USBFIFO[CDC_EP << 1] = 0xA1;
    USBFIFO[CDC_EP << 1] = 0x20;
    USBFIFO[CDC_EP << 1] = 0;
    USBFIFO[CDC_EP << 1] = 0;
    USBFIFO[CDC_EP << 1] = 0; // 0 seems to work nicely.  what if this is 1??
    USBFIFO[CDC_EP << 1] = 0;
    USBFIFO[CDC_EP << 1] = 2;   // 1 byte
    USBFIFO[CDC_EP << 1] = 0;
    USBFIFO[CDC_EP << 1] = 0;// 0; //CDC_DCD|CDC_DSR; //
//	(Data Set Ready) Установка данных готова.
//Сигналы, передаваемые по этой цепи, указывают на готовность DCE к работе.
//Состояние DSR=True (если цепь 25 находится в состоянии TM=False или не используется) указывает на то, что устройство преобразования сигналов или аналогичное устройство подсоединено к линии связи и что DCE готов к взаимодействию по цепям управления на стыке с DTE для обмена данными.
//Состояние DSR=True при наличии состояния TM=True в цепи 25 указывает на то, что DCE готов к взаимодействию по цепям управления на стыке DTE для проведения проверки.
//Состояние DSR=False при наличии в цепи CTS(5)=True указывает на то, что DCE готов осуществить обмен сигналами данных, связанных с программированием или управлением последовательным автоматическим вызовом.
//Состояние DSR=False при наличии в цепи CTS(5)= False указывает, что:
//    а)DCE не готов для передачи данных;
//    б)обнаружено состояние неисправности, которое может быть в сети или в DCE;
//    в)обнаружено разъединение от удаленной станции или от сети.
//	(Data Carrier Detected) Обнаружен носитель информации.
//Сигналы, передаваемые по этой цепи, указывают, находится ли уровень принимаемого линейного сигнала канала данных в пределах, установленных соответствующими рекомендациями на DCE.
//Состояние DCD=True указывает, что уровень принимаемого сигнала соответствует установленным пределам.
//Состояние DCD=True может быть также во время обмена данными между DCE и DTE при программировании или управлении последовательным автоматическим вызовом.
//Состояние DCD=False указывает, что уровень принимаемого сигнала не соответствует установленным пределам.
    USBFIFO[CDC_EP << 1] = 0;
    USBCSIL = USBCSIL_INPKT_RDY;
    USBINDEX=storeIndex;
}
//
//void usbSendError(void)
//{
//// CDC EP
//    uint8_t storeIndex;
//    storeIndex = USBINDEX;
//    USBINDEX=CDC_EP;
//    USBFIFO[CDC_EP << 1] = 'E';
//    USBFIFO[CDC_EP << 1] = 'R';
//    USBFIFO[CDC_EP << 1] = 'R';
//    USBFIFO[CDC_EP << 1] = 'O';
//    USBFIFO[CDC_EP << 1] = 'R';
//    USBFIFO[CDC_EP << 1] = '\r';
//    USBFIFO[CDC_EP << 1] = '\n';
//    USBCSIL = USBCSIL_INPKT_RDY;
//    USBINDEX=storeIndex;
//}
////
//void usbSendOK(void)
//{
//// CDC EP
//    uint8_t storeIndex;
//    storeIndex = USBINDEX;
//    USBINDEX=USB_IN_EP;
//    USBFIFO[USB_IN_EP << 1] = 'O';
//    USBFIFO[USB_IN_EP << 1] = 'K';
//    USBFIFO[USB_IN_EP << 1] = '\r';
//    USBFIFO[USB_IN_EP << 1] = '\n';
//    USBCSIL = USBCSIL_INPKT_RDY;
//    USBINDEX=storeIndex;
//}
// Walk through the list of descriptors and find a match
// return length of descriptorn
void  usbGetDescriptor(void) //usbDescLength,usbDescIndex;
{
    uint8_t type = usbSetupPacket.wValue>>8;
    uint8_t index = usbSetupPacket.wValue&0x00FF;
    uint16_t pos=0;
    uint8_t len;
    while (CC1111descriptor[pos] != 0)
    {
        if ((CC1111descriptor[pos+1] == type) && (index-- == 0))
        {
            if (type == USB_DESC_CONFIGURATION)
                len = CC1111descriptor[pos+2];
            else
                len = CC1111descriptor[pos];
            if(usbSetupPacket.wLength > len) usbSetupPacket.wLength = len;
            usbSetupPacket.wIndex=pos;
            return; // Found
        }
        pos += CC1111descriptor[pos];
    }
    usbSetupPacket.wLength=0;
}
#endif // USBCDC_H_INCLUDED
