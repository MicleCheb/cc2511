#ifndef BOOTLOADER_H_INCLUDED
#define BOOTLOADER_H_INCLUDED
#include <stdio.h>
#include <stdint.h>
// FWT = 21000 * FCLK / (16 * 10^9)
// For FCLK = 24MHz, FWT = 0x1F
#define FLASH_FWT 0x1F
// Address of flash controller data register
#define FLASH_FWDATA_ADDR 0xDFAF

//
uint8_t initFlash(void);
uint8_t freeFlash(void);
uint8_t writeFlash(void);
uint8_t readFlash(void);
uint8_t exitFlash(void);

uint8_t checkFlash(uint8_t mode);
uint8_t eraseFlash(void);
void dmaFlashWrite(void);

#define CHECK_NO_ERASE 0
#define CHECK_AND_ERASE 1
//
#define mcs51FLASH_PAGE_SIZE     (uint16_t)1024
#define mcs51FLASH_WORD_SIZE         2

//#define xBuf    OsParam.flashBuf
//#define xCnt    OsParam.flashCnt
//#define xAdr    OsParam.FlashAddr.adr
//

uint8_t checkFlash(uint8_t mode)
{
    if(mode == CHECK_NO_ERASE)
    {
        printf("\nCC2511 BootLoader Chip ID %02X%02X\n",PARTNUM,VERSION);
        printf("Check Flash : ");
    };
    for(xAdr=0; xAdr < (32*FLASH_PAGE_SIZE); xAdr++)
    {
        if(*(__xdata uint8_t *)xAdr != 0xFF)
        {
            if(xAdr >= USER_CODE_BASE)
            {
                if(mode == CHECK_AND_ERASE)
                {
                    eraseFlash();
                    printf("F");
                }
                else printf("O");
            }
            else printf("O");
            xAdr &= ~ (FLASH_PAGE_SIZE-1);
            xAdr += FLASH_PAGE_SIZE;
        }
        else
        {
            if(xAdr%FLASH_PAGE_SIZE == 0) printf("F");
        }
    }
    printf("\n");
    return 0;
}
//
uint8_t initFlash(void)
{
    return  checkFlash(CHECK_NO_ERASE);
}
//
uint8_t freeFlash(void)
{
    return checkFlash(CHECK_AND_ERASE);
}
//
uint8_t eraseFlash(void) //uint16_t flashAddr
{
    // Waiting for the flash controller to be ready
    if(xAdr < USER_CODE_BASE) return 0;
    while (FCTL & FCTL_BUSY) {};
    // Configure flash controller for a flash page erase
    // FADDRH[5:1] contains the page to erase
    // FADDRH[1]:FADDRL[7:0] contains the address within the page
    // (16-bit word addressed)
    FWT = FLASH_FWT;
    FADDRH = (xAdr>>8)>>1;
    FADDRL = 0;
    // Erase the page that will be written to
    FCTL |=  FCTL_ERASE;
    nop; // Required, see datasheet
    // Wait for the erase operation to complete
    while (FCTL & FCTL_BUSY) {};
    //FCTL &=  ~FCTL_ERASE;
    return 0;
}
//
uint8_t writeFlash(void)
{
    if(xAdr < USER_CODE_BASE) return 0;
//    if(xCnt==0xFFFF)
//    {
//        eraseFlash();//flashAddr
//        xCnt=0;
//        return 0;
//    }
    if((xAdr+xCnt)>=0x8000) xCnt=0x8000-xAdr;
    if(xAdr&1)
    {
        *(--xBuf)=0xFF; //        --xBuf;
        --xAdr;
        ++xCnt;
    }
    if(xCnt&1) *(xBuf + xCnt++)=0xFF;
    dmaFlashWrite();//flashAddr,xBuf,xCnt
    xCnt=0;
    OsParam.Flags &= ~PrG;
    return 0;
}
//
uint8_t readFlash(void)
{
    uint16_t i;
    for(i=0; i<mcs51FLASH_PAGE_SIZE; i++)
        *(xBuf + i) = *(__xdata uint8_t *)(xAdr + i);
    xCnt=mcs51FLASH_PAGE_SIZE;
    return 0;
}
//

uint8_t verifyFlash(void)
{
    return 0;
}
uint8_t protectFlash(void)
{
    return 0;
}
uint8_t unprotectFlash(void)
{
    return 0;
}
//
uint8_t exitFlash(void)
{
    return 0;
}
//
void dmaFlashWrite(void) //uint16_t flashAddr,__xdata uint8_t *xBuf,uint8_t xCnt
{
//          Really FlashAdr = (FlashAdr>>1)<<1 !!!!!
//          Configure DMA channel 0. Settings:
//    SRCADDR: address of the data to be written to flash (increasing).
//    DESTADDR: the flash controller data register (fixed), so that the
//    flash controller will write this data to flash.
//    VLEN: use LEN for transfer count.
//    LEN: equal to the number of bytes to be transferred.
//    WORDSIZE: each transfer should transfer one byte.
//    TMODE: should be set to single mode (see datasheet, DMA Flash Write).
//    Each flash write complete will re-trigger the DMA channel.
//    TRIG: let the DMA channel be triggered by flash data write complete
//    (trigger number 18). That is, the flash controller will trigger the
//    DMA channel when the Flash Write Data register, FWDATA, is ready to
//    receive new data.
//    SRCINC: increment by one byte.
//    DESTINC: fixed (always write to FWDATA).
//    IRQMASK: disable interrupts from this channel.
//    M8: 0, irrelevant since we use LEN for transfer count.
//    PRIORITY: low.
    if(xCnt < 2) return;
    FWT = FLASH_FWT;
// Waiting for the flash controller to be ready
    while (tBit(FCTL,7)); // Busy
    dmaCfg.src_high = ((uint16_t)xBuf)>>8; //
//    __asm
//    mov	dptr,#_dmaCfg
//    mov	a,(_xBuf + 1)
//    movx	@dptr,a
//
//    mov	dptr,#(_dmaCfg + 0x0001)
//    mov	a,_xBuf
//    movx	@dptr,a
//    __endasm;
    if(dmaCfg.src_high==0) dmaCfg.src_high=0xFF;
    dmaCfg.src_low  = ((uint16_t)xBuf)&0x00FF;
    dmaCfg.dst_high = FLASH_FWDATA_ADDR >> 8;    // 0xDF; FWDATA // DESTADDRH
    dmaCfg.dst_low  = FLASH_FWDATA_ADDR&0x00FF;  // 0xAF;        // DESTADDRL
    dmaCfg.len_high=0;  //xCnt>>8;
    dmaCfg.len_low=xCnt&0x00FF;
    if(dmaCfg.len_low&1) dmaCfg.len_low&=~1;
    dmaCfg.cfg0=DMA_CFG0_WORDSIZE_8|DMA_CFG0_TMODE_SINGLE|DMA_CFG0_TRIGGER_FLASH;
    dmaCfg.cfg1=DMA_CFG1_SRCINC_1|DMA_CFG1_DESTINC_0;
    DMA0CFGH  = ((uint16_t)&dmaCfg)>>8;
    if(DMA0CFGH==0) DMA0CFGH=0xFF;
    DMA0CFGL  = ((uint16_t)&dmaCfg)&0x00FF;
    FADDRH = xAdr >> 9;
    FADDRL = (xAdr >>1)&0x00FF;
    DMAARM = 0;
    DMAIRQ = 0;
    sBit(DMAARM, 0);
//    StartFlashWrite();
    if(((uint16_t)&dmaFlashWrite)&1)
    {
        nop;
        nop;
    }
    else
    {
        nop;
    }
    sBit(FCTL, 1);
    nop;
    while (!tBit(DMAIRQ,0));
    while (tBit(FCTL,6) || tBit(FCTL,7));
    cBit(DMAIRQ, 0);
    cBit(DMAARM, 0);
}
uint8_t __code BootDesk[]="BOOTLOADER";
struct FlashProg_t __code BootL=\
{'0',0xFF,BootDesk,initFlash,unprotectFlash,readFlash,freeFlash,writeFlash,verifyFlash,protectFlash,exitFlash
};
struct FlashProg_t* __code  __at (FUNCROM) BootProg[1] =  {&BootL};
#endif // BOOTLOADER_H_INCLUDED
