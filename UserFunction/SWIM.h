#ifndef SWIM_H_INCLUDED
#define SWIM_H_INCLUDED
#include <stdint.h>

#define SWIM_RST                DBG_RESET             //P0_1
#define SWIM_RST_HIGH           SWIM_RST=0
#define SWIM_RST_LOW            SWIM_RST=1

#define SWIM_DIO_DIR         DBG_DD_DIR          // P1_6
#define SWIM_DIO             DD                 // P1_7
#define SWIM_DIO_BIT         (1<<7)
#define SWIM_DIO_INPUT       DD_INPUT    //{P1DIR&=~DD_BIT;DBG_DD_DIR=INPUT;}
#define SWIM_DIO_OUTPUT      DD_OUTPUT   //{DBG_DD_DIR=OUTPUT;P1DIR|=DD_BIT;}

// SWIM commands
#define SWIM_SRST   0   // reset
// SRST command generates a system reset only if SWIM_CSR/SWIM_DM bit is set.
#define SWIM_ROTF   1   // read on the fly  ROTF N @E @H @L D[@] D[@+N]
#define SWIM_WOTF   2   // write on the fly WOTF N @E @H @L D[@] D[@+N]

#define SWIM_CSR        0x7F80

// SAFE_MASK NO_ACCESS SWIM_DM HS OSCOFF RST HSIT PRI
#define CLK_SWIMCCR 0x50CD // SWIM clock control register
#define FLASH_DUKR_ADR	0x5064
#define FLASH_PUKR_ADR	0x5062
#define FLASH_IAPSR_ADR	0x505F
#define FLASH_CR2		0x505b
#define FLASH_NCR2		0x505c

#define pB F0   // parity bit
#define dB F1   // data bit


void SWIMSendByte(uint8_t ch);
uint8_t SWIMReceiveByte(void);
void SWIMSendCmd(uint8_t ch);
void SWIMreadBuf(void);
void SWIMsendBuf(void);


uint8_t SWIMInit(void);
uint8_t SWIMUnprotect(void);
uint8_t SWIMRead(void);
uint8_t SWIMErase(void);
uint8_t SWIMWrite(void);
uint8_t SWIMVerify(void);
uint8_t SWIMProtect(void);
uint8_t SWIMExit(void);

void SWIMReceiveBit(void)   // receive bit dB
{
//    uint8_t j=0;
    while (OsParam.Alarm)
    {
        if( (SWIM_DIO == 0)  || (P1IFG & SWIM_DIO_BIT) ) //
        {
            NOP();
            NOP();
            NOP();
            NOP();
            NOP();
            NOP();
            if(SWIM_DIO) dB = 1;
            else dB = 0;
            P1IFG = 0;
            return;
        }
    }
//    return 0xFF;
}
void SWIMSendBit(void)  // send bit dB
{
    uint8_t j;
    if(dB)
    {
        SWIM_DIO = 0;
        NOP();
        NOP();
        NOP();
//        NOP();
        SWIM_DIO = 1;
        for(j=10; j; j--) {}
    }
    else
    {
        SWIM_DIO = 0;
        for(j=10; j; j--) {}
        SWIM_DIO = 1;
        NOP();
        NOP();
        NOP();
//        NOP();
    }
//    return 0;
}
void SWIMSendByte(uint8_t ch)
{
// 0 b2 b1 b0 pb ack 0 b7 b6 b5 b4 b3 b2 b1 b0 pb ack
// Header: 1 bit at ‘0’
// b2-b0: 3-bit command
// pb: parity bit: XOR between all b(i)
// ack:acknowledge (1 bit at ‘1’). The receiver must send the not-acknowledge
// value if it has detected a parity error (NACK: not acknowledge = 1 bit at ‘0’), or
// it is not yet ready.

    uint8_t j;
    pB = 0;
//    SetAlarm(2);
    SWIM_DIO=1;
    SWIM_DIO_OUTPUT;
    dB = 0;
    SWIMSendBit();
    for(j=8; j; j--)
    {
        if(ch & (1<<7))
        {
            dB=1;
            pB ^=1;
//
        }
        else dB = 0;
        SWIMSendBit();
        ch<<=1;
    }
    dB=pB;
    SWIMSendBit();
    SWIM_DIO_INPUT;
    P1IFG = 0;
    SWIMReceiveBit();
//    dB ^= 1;
}
uint8_t SWIMReceiveByte(void)
{
    uint8_t j,ch=0;
//    ch=0;
// 1 b7 b6 b5 b4 b3 b2 b1 b0 pb ack
// Header: 1 bit at ‘1’
// b7-b0: 8-bit data
// pb: parity bit sent after data
// ack: acknowledge
//    SetAlarm(2);
    SWIM_DIO_INPUT;
    P1IFG = 0;
    pB = 0;
    SWIMReceiveBit();
//    if(OsParam.Alarm==0) RTOSfunction.osPrintf("258\n");
    if(dB != 1) return 253;
    for(j=8; j; j--)
    {
        SWIMReceiveBit();
        if(OsParam.Alarm == 0)
        {
            dB=0;
            return 252;
        }
        ch <<= 1;
        if(dB)
        {
            ch |= 1;
            pB ^= 1;
        }
    }
    SWIMReceiveBit();
    if(OsParam.Alarm == 0)
    {
        dB=0;
        return 251;
    }
    pB ^= dB;
    if(pB)
    {
        dB=0;
        return 250;
    }
    SWIM_DIO=1;
    SWIM_DIO_OUTPUT;
    dB=1;
    SWIMSendBit();
    SWIM_DIO_INPUT;
    return ch;
}
void SWIMSendCmd(uint8_t ch)
{
    uint8_t j;
    pB = 0;
//    SetAlarm(2);
    SWIM_DIO=1;
    SWIM_DIO_OUTPUT;
    dB = 0;
    SWIMSendBit();
    for(j=3; j; j--)
    {
        if(ch & (1<<2))
        {
            dB=1;
            pB ^=1;
        }
        else dB = 0;
        SWIMSendBit();
        ch<<=1;
    }
    dB=pB;
    SWIMSendBit();
    SWIM_DIO_INPUT;
    P1IFG = 0;
    SWIMReceiveBit();
}
//uint8_t SWIMReceiveCmd(void)
void SWIMsendBuf(void)
{
    SetAlarm(1);
    while(OsParam.Alarm) {};
    SetAlarm(2);

    SWIMSendCmd(SWIM_WOTF);

    SWIMSendByte(xCnt);

    SWIMSendByte(xAdr>>16);

    SWIMSendByte(xAdr>>8);

    SWIMSendByte(xAdr);

    while(xCnt--)
    {
        SWIMSendByte(*xBuf++);

    }
}
void SWIMreadBuf(void)
{
    SetAlarm(1);
    while(OsParam.Alarm) {};
    SetAlarm(2);

    SWIMSendCmd(SWIM_ROTF);
//    if(OsParam.Alarm==0) RTOSfunction.osPrintf("-0\n");
    SWIMSendByte(xCnt);
//    if(OsParam.Alarm==0) RTOSfunction.osPrintf("-1\n");
    SWIMSendByte(xAdr>>16);
//    if(OsParam.Alarm==0) RTOSfunction.osPrintf("-2\n");
    SWIMSendByte(xAdr>>8);
//    if(OsParam.Alarm==0) RTOSfunction.osPrintf("-3\n");
    SWIMSendByte(xAdr);
//    if(OsParam.Alarm==0) RTOSfunction.osPrintf("-4\n");
    while(xCnt--)
    {

        *xBuf++ = SWIMReceiveByte();
//                if(OsParam.Alarm==0) RTOSfunction.osPrintf("-0\n");
    }
}
//{
//
//    return 0;
//}
uint8_t SWIMInit(void)
{
    uint8_t j;
    PICTL |= (1<<1);   // falling //rising edge
    P1IFG=0;
    SWIM_DIO=1;
    SWIM_DIO_OUTPUT;

    SWIM_RST_LOW;
    xBuf=(__xdata uint8_t *)XRDATA;
    delay(1);
    SWIM_DIO=0;
    for(j=128; j; j--) NOP();
//    SWIM_DIO=1;
//    for(j=16; j; j--) NOP();
    SetAlarm(2);
    while(OsParam.Alarm) {};
    SetAlarm(2);

    for(j=8; j; j--)
    {
        SWIM_DIO ^= 1;
        delay(16);
    }
    for(j=8; j; j--)
    {
        SWIM_DIO ^= 1;
        delay(8);
    }
    SWIM_DIO=1;
    SWIM_DIO_INPUT;
//

    while(SWIM_DIO && OsParam.Alarm) {};
    if(OsParam.Alarm == 0) RTOSfunction.osPrintf("SWIM ERR1\n");
    j=0;
    while((SWIM_DIO == 0 ) && OsParam.Alarm)  ++j;

    delay(1);
    if(OsParam.Alarm)
    {
//    Write 0A0h in the SWIM_CSR:
        xAdr=SWIM_CSR;
        xCnt=1;
        xBuf=(__xdata uint8_t *)XRDATA;
        *xBuf = (1<<5);
        SWIMsendBuf();
        xAdr=SWIM_CSR;
        xCnt=1;
        xBuf=(__xdata uint8_t *)XRDATA;
        *xBuf = (1<<7);
        SWIMsendBuf();
        SWIM_RST_HIGH;
        delay(34);
//        SWIMSendCmd(SWIM_WOTF);
//        SWIMSendByte(4);
//        SWIMSendByte(0x00);
//        SWIMSendByte(SWIM_CSR>>8);
//        SWIMSendByte(SWIM_CSR);
//        SWIMSendByte(0xA0);
//



//        RTOSfunction.osPrintf("SWIM F %u ACK %u\n",j,dB);

//        SWIMSendCmd(SWIM_SRST);
//        delay(33);

        xAdr=0x4926;//
        xCnt=12;
        xBuf=(__xdata uint8_t *)XRDATA;
//        *xBuf = 0x00;

//        for(j=0; j<16; j++)
//        {
        SWIMreadBuf();
//        ++xAdr;
        xBuf=(__xdata uint8_t *)XRDATA;
        for(j=12; j; j--) RTOSfunction.osPrintf("%02X",*xBuf++);
//        }
        RTOSfunction.osPrintf(" ID\n");
    }
    else RTOSfunction.osPrintf("SWIM ERR2\n");
//#define SWIM_WOTF   2   // write on the fly WOTF N @E @H @L D[@] D[@+N]
//
//#define SWIM_CSR    0x7F80

    return 0;
}
uint8_t SWIMUnprotect(void)
{
//    RTOSfunction.osPrintf("UserExit\n");
    return 0;
}
uint8_t SWIMRead(void)
{
    xCnt=16;
    xBuf=(__xdata uint8_t *)XRDATA;
    SWIMreadBuf();
    xCnt=16;
    xBuf=(__xdata uint8_t *)XRDATA;
//    RTOSfunction.osPrintf("UserExit\n");
    return 0;
}
uint8_t SWIMErase(void)
{
//    RTOSfunction.osPrintf("UserExit\n");
    return 0;
}
uint8_t SWIMWrite(void)
{
//    RTOSfunction.osPrintf("UserExit\n");
    return 0;
}
uint8_t SWIMVerify(void)
{
//    RTOSfunction.osPrintf("UserExit\n");
    return 0;
}
uint8_t SWIMProtect(void)
{
//    RTOSfunction.osPrintf("UserExit\n");
    return 0;
}
uint8_t SWIMExit(void)
{
    SWIM_RST_LOW;
    delay(2);
    SWIM_RST_HIGH;
//    RTOSfunction.osPrintf("UserExit\n");
    return 0;
}
//
uint8_t __code SWIMDesc[]="STM8 SWIM";
__code struct FlashProg_t  SWIME=\
{'A',0xFF,SWIMDesc,SWIMInit,SWIMUnprotect,SWIMRead,SWIMErase,SWIMWrite,SWIMVerify,SWIMProtect,SWIMExit
};
//struct FlashProg_t* __code  __at (FUNCROM + 10*3) SWIMProg[1] =  {&SWIME};
#endif // SWIM_H_INCLUDED
