#ifndef USBSBW430_H_INCLUDED
#define USBSBW430_H_INCLUDED
uint8_t sbw430Init(void);
uint8_t sbw430Erase(void);
uint8_t sbw430WriteFlash(void);
uint8_t sbw430ReadMemory(void);
uint8_t sbw430Exit(void);
//  JTAG Control Signal Register for 5xx and 6xx Families
#define bREAD   (1<<0)
#define bWRITE  (0<<0)
//(0<<1)  Always write 0
//(0<<2)  Always write 0
#define bWAIT   (1<<3)  // 0 = CPU clock not stopped
#define bBYTE   (1<<4)
#define bWORD   (0<<4)
//(0<<5)  Always write 0
//(0<<6)  Always write 0
#define bLOAD       (1<<7)  // CPU instruction state
#define bSUSP       (1<<8)  // 1 = CPU suspended
#define bSYNC       (1<<9)  // 1 = Synchronized
#define bJTAG       (1<<10) // 1 = CPU under JTAG control
#define bPOR        (1<<11) // 1 = Perform POR
#define bRELEASE0   (1<<12)
#define bRELEASE1   (1<<13) // 00 = All bits are controlled by JTAG if TCE1 is 1
// 01 = RW (bit 0) and BYTE (bit 4) are released from JTAG control
// 10 = RW (bit 0), HALT (bit 1), INTREQ (bit 2), and BYTE (bit 4) are released from JTAG control
#define bNOP    (1<<14)|(1<<15) // Instruction sequence number. Read only
//
// JTAG identification value
#define JTAG_ID 0x91
// Instructions for the JTAG control signal register
#define IR_CNTRL_SIG_16BIT   0xC8   // original value: 0x13 0x13
#define IR_CNTRL_SIG_CAPTURE 0x28   // original value: 0x14 0x14   //0x28 //
#define IR_CNTRL_SIG_RELEASE 0xA8   // original value: 0x15 0x15
#define IR_COREIP_ID         0xE8   // original value: 0x17 0x17   // original value: 0x17 0xE8

// Instructions for the JTAG data register
#define IR_DATA_16BIT        0x82   // original value: 0x41 0x41 // MAB is still controlled by the CPU
#define IR_DATA_CAPTURE      0x42
#define IR_DATA_QUICK        0xC2   // original value: 0x43 0x43   // auto-increments the program counter by two
// Instructions for the JTAG address register
#define IR_ADDR_16BIT        0xC1   // original value: 0x83 0x83
#define IR_ADDR_CAPTURE      0x21   // original value: 0x84 0x84
#define IR_DATA_TO_ADDR      0xA1   // original value: 0x85 0x85

#define IR_DEVICE_ID         0xE1   // original value: 0x87 0x87   //0xE1
// Instructions for the JTAG PSA mode
#define IR_DATA_PSA          0x22   // original value: 0x44 0x44
#define IR_SHIFT_OUT_PSA     0x62   // original value: 0x46 0x46
// Instructions for the JTAG Fuse
#define IR_PREPARE_BLOW      0x44   // original value: 0x22 0x22
#define IR_EX_BLOW           0x24   // original value: 0x24 0x24
// JTAG Mailbox System
#define IR_JMB_EXCHANGE      0x86   // original value: 0x61 0x61 //0x86
#define IR_TEST_REG          0x54   // original value: 0x2A
//! \brief Instruction for 3 volt test register in 5xx
#define IR_TEST_3V_REG       0xF4   // original value: 0x2F
/* Bypass instruction */
#define IR_BYPASS            0xFF /* 0xFF */
#endif // USBSBW430_H_INCLUDED
/*
All instructions sent to the target MSP430 through the JTAG
register are transferred LSB first.
Table 1-5. JTAG Communication Macros
    Macro Name Function
Shifts an 8-bit JTAG instruction into the JTAG instruction register. At the same time, the 8-bit
    IR_SHIFT (8-bit Instruction)
value is shifted out through TDO.
Shifts a 16-bit data word into a JTAG data register. At the same time, the 16-bit value is shifted
    DR_SHIFT16 (16-bit Data)
out through TDO.
Shifts a 20-bit address word into the JTAG Memory Address Bus register. At the same time, the
    DR_SHIFT20 (20-bit Address)
20-bit value is shifted out through TDO. Only applicable to MSP430X architecture devices.
    MsDelay (time) Waits for the specified time in milliseconds
    SetTCLK Sets TCLK to 1
    ClrTCLK Sets TCLK to 0
    TDOvalue Variable containing the last value shifted out on TDO
Macros for Spy-Bi-Wire (SBW) Interface
The low phase of the clock signal supplied on SBWTCK must not be longer than 7 �s. If the
low phase is longer, the SBW logic is deactivated, and it must be activated again according
to Section 1.3.1.


SBWTDIO  <   MASTER-TMS  MASTER-TDI  SLAVE-TDO > < MASTER-TMS  MASTER-TDI  SLAVE-TDO >   MASTER-TMS

SBWTCK   <       CLK         CLK         CLK   > <      CLK         CLK         CLK  >       CLK

RST/NMI/SBWTDIO
TEST/SBWTCK


Table 1-6. Memory Access Instructions
Instruction Name 8-Bit Instruction Value
Controlling the Memory Address Bus (MAB)
IR_ADDR_16BIT 0x83
IR_ADDR_CAPTURE 0x84
Controlling the Memory Data Bus (MDB)
IR_DATA_TO_ADDR 0x85
IR_DATA_16BIT 0x41
IR_DATA_QUICK 0x43
IR_BYPASS 0xFF
Controlling the CPU
IR_CNTRL_SIG_16BIT 0x13
IR_CNTRL_SIG_CAPTURE 0x14

Instruction Name 8-Bit Instruction Value
R_CNTRL_SIG_RELEASE 0x15
Memory Verification by Pseudo Signature Analysis (PSA)
R_DATA_PSA 0x44
R_SHIFT_OUT_PSA 0x46
JTAG Access Security Fuse Programming
R_Prepare_Blow 0x22
R_Ex_Blow 0x24
JTAG Mailbox System
R_JMB_EXCHANGE 0x61


void eeErase(uint8_t *eAdr)
{
//   WDTCTL = WDTPW | WDTHOLD;   // Stop WDT
    dint();
    FCTL4 = FWKEY;
    if( FCTL3&LOCKA) FCTL3 = FWKEY|LOCKA;
    else FCTL3 = FWKEY;
    FCTL1 = FWKEY|ERASE;    // Set Bank Erase bit
    while(FCTL3 & BUSY);
    *eAdr = 0x00;           // Dummy erase byte
    FCTL1 = FWKEY;          // Clear WRT bit
    if( FCTL3&LOCKA) FCTL3 = FWKEY;
    else FCTL3 = FWKEY|LOCKA;  // Set LOCK bit
    FCTL4 = FWKEY|LOCKINFO;
//    RunWdt();
    eint();
}
; segment Erase from RAM.
; Assumes Program Memory. Information memory or BSL
; requires LOCKINFO to be cleared as well.
; Assumes ACCVIE = NMIIE = OFIE = 0.
MOV #WDTPW+WDTHOLD,&WDTCTL ; Disable WDT
L1 BIT #BUSY,&FCTL3 ; Test BUSY
JNZ L1 ; Loop while busy
MOV #FWPW,&FCTL3 ; Clear LOCK
MOV #FWPW+ERASE,&FCTL1 ; Enable page era
CLR &0FC10h ; Dummy write
L2 BIT #BUSY,&FCTL3 ; Test BUSY
JNZ L2 ; Loop while busy
MOV #FWPW+LOCK,&FCTL3 ; Done, set LOCK
... ; Re-enable WDT?

void eeWrite(uint8_t *eAdr,uint8_t *buf,uint8_t cnt)
{
    dint();
    FCTL4 = FWKEY;                      // unlock INFO
    if( FCTL3&LOCKA) FCTL3 = FWKEY|LOCKA;
    else FCTL3 = FWKEY;
    FCTL1 = FWKEY|WRT; // Set WRT bit for write operation
    while(cnt--) {*eAdr++ = *buf++;  // Write value to flash
        while(FCTL3&BUSY);};
    FCTL1 = FWKEY; // Clear WRT bit
    if( FCTL3&LOCKA) FCTL3 = FWKEY;
    else FCTL3 = FWKEY|LOCKA;  // Set LOCK bit
    FCTL4 = FWKEY|LOCKINFO;  // unlock INFO
    eint();
}
; Byte or word write from flash.
; Assumes 0x0FF1E is already erased
; Assumes ACCVIE = NMIIE = OFIE = 0.
MOV #WDTPW+WDTHOLD,&WDTCTL ; Disable WDT
MOV #FWPW,&FCTL3 ; Clear LOCK
MOV #FWPW+WRT,&FCTL1 ; Enable write
MOV #0123h,&0FF1Eh ; 0123h -> 0x0FF1E
MOV #FWPW,&FCTL1 ; Done. Clear WRT
MOV #FWPW+LOCK,&FCTL3 ; Set LOCK
... ; Re-enable WDT?
*/
//----------------------------------------------------------------------------
// Constants for the JTAG instruction register (IR) require LSB first.
// The MSB has been interchanged with LSB due to use of the same shifting
// function as used for the JTAG data register (DR) which requires MSB
// first.
//----------------------------------------------------------------------------

//// Instructions for the JTAG control signal register
////! \brief Set the JTAG control signal register
//#define IR_CNTRL_SIG_16BIT         0xC8   // original value: 0x13
////! \brief Read out the JTAG control signal register
//#define IR_CNTRL_SIG_CAPTURE       0x28   // original value: 0x14
////! \brief Release the CPU from JTAG control
//#define IR_CNTRL_SIG_RELEASE       0xA8   // original value: 0x15
//
//// Instructions for the JTAG fuse
////! \brief Prepare for JTAG fuse blow
//#define IR_PREPARE_BLOW            0x44   // original value: 0x22
////! \brief Perform JTAG fuse blow
//#define IR_EX_BLOW                 0x24   // original value: 0x24
//
//// Instructions for the JTAG data register
////! \brief Set the MSP430 MDB to a specific 16-bit value with the next
////! 16-bit data access
//#define IR_DATA_16BIT              0x82   // original value: 0x41
////! \brief Set the MSP430 MDB to a specific 16-bit value (RAM only)
//#define IR_DATA_QUICK              0xC2   // original value: 0x43
//
//// Instructions for the JTAG PSA mode
////! \brief Switch JTAG data register to PSA mode
//#define IR_DATA_PSA                0x22   // original value: 0x44
////! \brief Shift out the PSA pattern generated by IR_DATA_PSA
//#define IR_SHIFT_OUT_PSA           0x62   // original value: 0x46
//
//// Instructions for the JTAG address register
////! \brief Set the MSP430 MAB to a specific 16-bit value
////! \details Use the 20-bit macro for 430X and 430Xv2 architectures
//#define IR_ADDR_16BIT              0xC1   // original value: 0x83
////! \brief Read out the MAB data on the next 16/20-bit data access
//#define IR_ADDR_CAPTURE            0x21   // original value: 0x84
////! \brief Set the MSP430 MDB with a specific 16-bit value and write
////! it to the memory address which is currently on the MAB
//#define IR_DATA_TO_ADDR            0xA1   // original value: 0x85
////! \brief Bypass instruction - TDI input is shifted to TDO as an output
//#define IR_BYPASS                  0xFF   // original value: 0xFF
//#define IR_DATA_CAPTURE            0x42
//
//// JTAG identification values for all existing Flash-based MSP430 devices
////! \brief JTAG identification value for 430X architecture devices
//#define JTAG_ID                    0x89
////! \brief JTAG identification value for 430Xv2 architecture devices
//#define JTAG_ID91                  0x91
////! \brief JTAG identification value for 430Xv2 architecture FR59XX devices
//#define JTAG_ID99                  0x99
//// Additional instructions for JTAG_ID91 architectures
////! \brief Instruction to determine device's CoreIP
//#define IR_COREIP_ID               0xE8   // original value: 0x17
////! \brief Instruction to determine device's DeviceID
//#define IR_DEVICE_ID               0xE1   // original value: 0x87
//
//// Instructions for the JTAG mailbox
////! \brief Request a JTAG mailbox exchange
//#define IR_JMB_EXCHANGE            0x86   // original value: 0x61
////! \brief Instruction for test register in 5xx
//#define IR_TEST_REG                0x54   // original value: 0x2A
////! \brief Instruction for 3 volt test register in 5xx
//#define IR_TEST_3V_REG             0xF4   // original value: 0x2F
