#ifndef USBMCS51_H_INCLUDED
#define USBMCS51_H_INCLUDED
/*
 * CCHL - ChipCon Hardware Loader
 * A hardware programmer for the CC1110/CC1111 which runs on the CC1110/CC1111
 * Micle Chebotarev (c) 2013 <mcheb@yandex.ru>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2 of the License.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.
 */
#include <stdint.h>
#include "cc1111.h"
// Connect the target CC1110 up as follows
//#define DD                  P1_7
//#define DC                  P1_1
//#define DBG_RST             P0_1
//#define DBG_DD_DIR          P1_6
// DEBUG COMMAND
#define CHIP_ERASE          0x14    //0001 0100
#define WR_CONFIG           0x1D    //0001 1101
#define RD_CONFIG           0x24    //0010 0100
#define GET_PC              0x28    //0010 1000
#define READ_STATUS         0x34    //0011 0100
#define SET_HW_BRKPNT       0x3B    //0011 1011
#define HALT                0x44    //0100 0100
#define RESUME              0x4C    //0100 1100
#define DEBUG_INSTR         0x54    //0101 01 yy
#define STEP_INSTR          0x5C    //0101 1100
#define STEP_REPLACE        0x64    //0110 01 yy
#define GET_CHIP_ID         0x68    //0110 1000
// STATUS CODE
#define ST_CHIP_ERASE_DONE   0x80
#define ST_PCON_IDLE         0x40
#define ST_CPU_HALTED        0x20
#define ST_POWER_MODE_0      0x10
#define ST_HALT_STATUS       0x08
#define ST_DEBUG_LOCKED      0x04
#define ST_OSCILLATOR_STABLE 0x02
#define ST_STACK_OVERFLOW    0x01

#define mcs51FLASH_PAGE_SIZE       1024
#define mcs51FLASH_WORD_SIZE         2
#define mcs51WORDS_PER_FLASH_PAGE (mcs51FLASHPAGE_SIZE/mcs51FLASH_WORD_SIZE)
//
uint8_t mcs51Init(void);
uint8_t mcs51Unprotect(void);
uint8_t mcs51Read(void);
uint8_t mcs51Erase(void);
uint8_t mcs51Write(void);
uint8_t mcs51Verify(void);
uint8_t mcs51Protect(void);
uint8_t mcs51Exit(void);

void mcs51ErasePage(uint8_t page);
//
void mcs51SendByte(uint8_t ch)
{
    uint8_t i;
    DD_OUTPUT;
    for (i = 8; i; i--)
    {
        if (ch & 0x80) DD = 1;
        else DD = 0;
        DC = 1;
        DC = 0;
        ch<<=1;
    }
}
//
uint8_t mcs51RecvByte(void)
{
    uint8_t ch = 0;
    uint8_t i;
    DD_INPUT;
    for (i = 8; i; i--)
    {
        ch<<=1;
        DC = 1;
        if (DD) ch |= 0x01;
        DC = 0;
    }
    return ch;
}
//
uint8_t mcs51CMD(uint8_t CMD)
{
    mcs51SendByte(CMD);
    return mcs51RecvByte();
}
//
uint16_t mcs51GET_CHIP_ID(void)
{
    union
    {
        uint16_t i16;
        uint8_t i8[2];
    } iD;
    iD.i8[1]= mcs51CMD(GET_CHIP_ID);
    iD.i8[0]=mcs51RecvByte();
    return iD.i16;
}
//
uint8_t mcs51PageErase(void)
{
    mcs51ErasePage(xAdr/mcs51FLASH_PAGE_SIZE);
    return 0;
}
uint8_t mcs51ChipErase(void)
{
    uint8_t st;
    mcs51CMD(CHIP_ERASE);
//    DelayAlarm(2);
    SetAlarm(2);
    while(OsParam.Alarm) {};

//    delay(201);
    do
    {
        st=mcs51CMD(READ_STATUS);
    }
    while (!(st & ST_CHIP_ERASE_DONE));
    return 0;
}
//
uint16_t mcs51GET_PC(void)
{
    union
    {
        uint16_t i16;
        uint8_t i8[2];
    } pC;
    pC.i8[1]=mcs51CMD(GET_PC);
    pC.i8[0]=mcs51RecvByte();
    return pC.i16;
}
//
void  mcs51SET_PC(uint16_t value) //uint16_t xAdr
{
    mcs51SendByte(DEBUG_INSTR + 3); //DEBUG_INSTR
    mcs51SendByte(0x02);            // LJMP addr16 Long jump 0x02
    mcs51SendByte(value >> 8);
    mcs51SendByte(value);
    mcs51RecvByte();
}
//
//__sfr __at 0xAB FWT;    // Flash Write Timing
//__sfr __at 0xAC FADDRL; // Flash Address Low Byte
//__sfr __at 0xAD FADDRH; // Flash Address High Byte
//__sfr __at 0xAE FCTL;   // Flash Control
//__sfr __at 0xAF FWDATA; // Flash Write Data
// MOV Rn,#data Move immediate data to register 0x78 - 0x7F 2
// MOV A,Rn Move register to accumulator 0xE8 - 0xEF 1
void mcs51WriteReg(uint8_t reg,uint8_t value)
{
    mcs51SendByte(DEBUG_INSTR + 3); //DEBUG_INSTR
    mcs51SendByte(0x75);    // MOV direct,#data Move immediate data to direct byte 0x75
    mcs51SendByte(reg);
    mcs51SendByte(value);
    mcs51RecvByte();
}
//
uint8_t mcs51ReadReg(uint8_t reg)
{
// (uint8_t)&FWDATA
    mcs51SendByte(DEBUG_INSTR + 2);
    mcs51SendByte(0xE5);    // MOV A,direct Move direct byte to accumulator 0xE5
    mcs51SendByte(reg);
    return mcs51RecvByte();
}
//
void mcs51LoadDPTR(uint16_t value) //uint16_t xAdr
{
// Load DPTR with xAdr  90 (ADR>>8) (ADR&0x00FF) mov dptr,#_ADR
    mcs51SendByte(DEBUG_INSTR + 3);
    mcs51SendByte(0x90);
    mcs51SendByte(value>>8);
    mcs51SendByte(value&0x00FF);
    mcs51RecvByte();
}
//
void mcs51IncDPTR(void)
{
// inc	dptr A3
    mcs51SendByte(DEBUG_INSTR + 1); //STEP_REPLACE
    mcs51SendByte(0xA3);
    mcs51RecvByte();
}
//
uint8_t mcs51ReadDPTR(void)
{
// movx	a,@dptr E0
    mcs51SendByte(DEBUG_INSTR + 1); //STEP_REPLACE
    mcs51SendByte(0xE0);
    return mcs51RecvByte();
}
//
void mcs51WriteDPTR(uint8_t value)
{
//74 80  mov	a,#0x80
    mcs51SendByte(DEBUG_INSTR + 2); //STEP_REPLACE
    mcs51SendByte(0x74);
    mcs51SendByte(value);
    mcs51RecvByte();
//MOVX @DPTR,A Move A to external RAM (16-bit address) 0xF0
    mcs51SendByte(DEBUG_INSTR + 1); //STEP_REPLACE
    mcs51SendByte(0xF0);
    mcs51RecvByte();
}
// uint16_t xAdr,__xdata uint8_t *Buf,uint16_t cnt
void mcs51WriteMemory(void) //__reentrant
{
    mcs51LoadDPTR(xAdr);//
    while(xCnt--)
    {
        mcs51WriteDPTR(*xBuf++);
        mcs51IncDPTR();
    }
}
//
void mcs51ErasePage(uint8_t page)
{
    // Waiting for the flash controller to be ready
//    while (FCTL & FCTL_BUSY);
    while( mcs51ReadReg((uint8_t)&FCTL) & FCTL_BUSY);
    // Configure flash controller for a flash page erase
    // FADDRH[5:1] contains the page to erase
    // FADDRH[1]:FADDRL[7:0] contains the address within the page
    // (16-bit word addressed)
//    FWT = FLASH_FWT;
//    mcs51WriteReg((uint8_t)&FWT,0x11);
//    FADDRH = flashAddr>> 9;
    mcs51WriteReg((uint8_t)&FADDRH,page<<1) ;
//     FADDRL = 0;
//    mcs51WriteReg((uint8_t)&FADDRL,0);
    /*
                                       4401 ;	ccFlash.h:33: FCTL |=  FCTL_ERASE;
          000F4A 43 AE 01         [24] 4402 	orl	_FCTL,#0x01
                                       4403 ;	ccFlash.h:34: nop; // Required, see datasheet
          000F4D 00               [12] 4404 	nop
                                       4405 ;	ccFlash.h:36: while (FCTL & FCTL_BUSY);
          000F4E                       4406 00104$:
          000F4E E5 AE            [12] 4407 	mov	a,_FCTL
          000F50 20 E7 FB         [24] 4408 	jb	acc.7,00104$
          000F53 22               [24] 4409 	ret
    */
// Erase the page that will be written to
//    FCTL |=  FCTL_ERASE;
    mcs51WriteReg((uint8_t)&FCTL,FCTL_ERASE); // mcs51ReadReg((uint8_t)&FCTL)|
//    nop; // Required, see datasheet
//    mcs51SendByte(DEBUG_INSTR+1); //DEBUG_INSTR
//    mcs51SendByte(0x00);            // NOP No operation 0x00
    mcs51RecvByte();
// Wait for the erase operation to complete
//    while (FCTL & FCTL_BUSY);
    while( mcs51ReadReg((uint8_t)&FCTL) & FCTL_BUSY);
}
//
#define cntReg 4
void mcs51CPUWriteFlash(void)
{
//    uint16_t cnt; -> (R5:R4)
//    __xdata uint8_t *SrcData;
//    cnt=700;
//    SrcData=(__xdata uint8_t *)0xF800;
    __asm
//    while (FCTL & FCTL_BUSY);
    1$:
    mov	a,_FCTL  // E5 AE
    jb	acc.7,1$   //   20 E7 FB
//    FCTL =  FCTL_WRITE;
    mov	_FCTL,#0x02    // 75 AE 02
//    do
//    mov	r5,#0x3F       // 7D 3F
    mov dptr,#0xF000  // 90 F0 00
    7$:
//    {
//        FWDATA=*SrcData++;
    movx	a,@dptr // E0
    mov	_FWDATA,a //F5 AF
    inc	dptr //A3
//        FWDATA=*SrcData++;
    movx	a,@dptr // E0
    mov	_FWDATA,a //F5 AF
    inc	dptr //A3
//        while (FCTL & FCTL_SWBSY);
    4$:
    mov	a,_FCTL //E5 AE
    jb	acc.6,4$ //20 E6 FB
//    }
//    while(--cnt);
    dec	r4 //1C
    cjne	r4,#0xFF,30$ //BC FF 01
    dec	r5 //1D
    30$:
    mov	a,r4 //EC
    orl	a,r5 //4D
    jnz	7$ //E2
    .DB 0xA5   // trap 0xA5 ret  22
////    djnz	r5,7$ //DD E9
    ret   //22 //trap 0xA5
    __endasm;
}
uint8_t mcs51Init(void)
{
    DC=0;
    DC_OUTPUT;
    DD=0;
    DD_OUTPUT;
// send debug init sequence
    RST_ON;    //0;
    delay(1);
    DC = 0;
    delay(1);
    DC = 1;
    delay(1);
    DC = 0;
    delay(1);
    DC = 1;
    delay(1);
    DC = 0;
    delay(1);
    RST_OFF;    //1;
    delay(1);
//
    RTOSfunction.osPrintf("\n\rChipID %04X\n", mcs51GET_CHIP_ID());
//  mcs51WriteMemory(0xFF00+64,(uint16_t)&prgCode,sizeof(prgCode));
    xAdr = 0xFF00+64*2;
    xBuf=(__xdata void *)mcs51CPUWriteFlash;
    xCnt=36;
    mcs51WriteMemory();
    return 0;// mcs51CHIP_ERASE();
}
uint8_t mcs51Unprotect(void)
{
    return 0;
}
uint8_t mcs51Read(void)
{
    xCnt  = mcs51FLASH_PAGE_SIZE;
    mcs51LoadDPTR(xAdr); //
    while(xCnt--)
    {
        *xBuf++ =  mcs51ReadDPTR();
        mcs51IncDPTR();
    }
    xCnt  = mcs51FLASH_PAGE_SIZE;
    return 0;
}

uint8_t mcs51Erase(void)
{
    return mcs51ChipErase();
}
uint8_t mcs51Write(void)
{
    uint8_t st=(uint8_t)xAdr&0x01;
    uint16_t i;
// Load image to 0xF000
////    mcs51WriteMemory(0xF000+st,Buf,cnt);
//
    mcs51LoadDPTR(0xF000);
    if(st)
    {
        mcs51WriteDPTR(0xFF);
        mcs51IncDPTR();
//        ++cnt;
    }
//
    for(i=0; i<xCnt; i++)
    {
        mcs51WriteDPTR(*xBuf++);
        mcs51IncDPTR();
    }
//
    if(st) ++xCnt;
//
    if((uint8_t)xCnt&0x01)
    {
        mcs51WriteDPTR(0xFF);
//        mcs51IncDPTR();
        ++xCnt;
    }
    xAdr>>=1;
//    mcs51WriteReg((uint8_t)&FWT,0x11);
//    mcs51WriteReg((uint8_t)&MEMCTR,0x01); // Prefetch disabled ,0x51 ??
    mcs51WriteReg((uint8_t)&FADDRH,xAdr>>8);
    mcs51WriteReg((uint8_t)&FADDRL,xAdr);
// Load Counter
    xCnt>>=1;
    mcs51WriteReg(cntReg,xCnt);
    mcs51WriteReg(cntReg+1,xCnt>>8);
//
    mcs51LoadDPTR(0xF000);
    mcs51SET_PC(0xFF00+64*2);
    mcs51CMD(RESUME);
    while (!(mcs51CMD(READ_STATUS) & ST_CPU_HALTED));
    return 0;
}
//
uint8_t mcs51Verify(void)
{
    return 0;
}
uint8_t mcs51Protect(void)
{
    return 0;
}
uint8_t mcs51Exit(void)
{
    RST_ON;
    DC_INPUT;
    DD_INPUT;
    DC=0;
    DD=0;
    delay(100);
    RST_OFF;
    return 0;
}
uint8_t __code MCS51Desk[]="CC1110";
__code struct FlashProg_t MCS51=\
{ '1',0xFF,MCS51Desk,mcs51Init,mcs51Unprotect,mcs51Read,mcs51Erase,mcs51Write,mcs51Verify,mcs51Protect,mcs51Exit
};
//struct FlashProg_t* __code  __at (FUNCROM + 1*3) mcs51Prog[1] =  {&MCS51};
#endif // USBMCS51_H_INCLUDED
