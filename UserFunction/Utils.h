#ifndef UTILS_H_INCLUDED
#define UTILS_H_INCLUDED
#include <stdint.h>
#include <stdio.h>
#include "cc1111.h"

void delay (uint8_t ticks);
void SetAlarm(uint8_t Q);
uint8_t asciiToBin(uint8_t ch);
uint8_t getData(__data uint8_t* ch );
void prtBuf(__xdata uint8_t* buf,uint8_t cnt);
void hexPrint(uint8_t c);
void hPut(uint8_t c);

#endif // UTILS_H_INCLUDED
