#ifndef bsl430F2121_H_INCLUDED
#define bsl430F2121_H_INCLUDED
#include <stdint.h>
#include <string.h>
//
#define F149FLASHPAGE_SIZE 512

#define BSL_HDR	           0x80
#define BSL_ACK            0x90

#define RX_PASSWORD        0x10 //RX password 80 10 24 24 xx xx xx xx D1 D2  D20 CKL CKH ACK
#define RX_DATA_BLOCK      0x12 //RX data block 80 12 n n AL AH n4 0 D1 D2  Dn4 CKL CKH ACK
#define TX_DATA_BLOCK      0x14 //TX data block 80 14 04 04 AL AH n 0     CKL CKH 
//BSL responds 80 xx n n D1 D2 ... ... ... ... ... Dn CKL CKH
#define ERASE_SEGMENT      0x16 //Erase segment 80 16 04 04 AL AH 02 A5     CKL CKH ACK
//#define ERASE_MAIN_OR_INFO 0x16 //Erase main or info 80 16 04 04 AL AH 04 A5     CKL CKH ACK
#define MASS_ERASE         0x18 //Mass erase 80 18 04 04 xx xx 06 A5     CKL CKH ACK
#define LOAD_PC            0x1A //Load PC 80 1A 04 04 AL AH xx xx     CKL CKH ACK
#define ERASE_CHECK        0x1C //Erase check 80 1C 04 04 AL AH LL LH     CKL CKH ACK
#define BSL_VERSION        0x1E // TX BSL version 80 1E 04 04 xx xx xx xx     CKL CKH 
// BSL responds 80 xx 10 10 D1 D2 ... ...    D10 CKL CKH 
#define CMD_BAUD           0x20 //Change baud rate 80 20 04 04 D1 D2 D3 xx     CKL CKH ACK
#define SET_MEM_OFFSET     0x21 //Set mem offset 80 21 04 04 xx xx AL AH     CKL CKH ACK

#define BSL_TX_SIZE 256

//#define BSLRTS         DC      // // RTS - TEST - TCK
//#define BSLDTR         DD     // // DTR - RESET
//#define DTR_DIR_OFF   {P1DIR&=~DD_BIT;DBG_DD_DIR=INPUT;}
//#define DTR_DIR_ON    {DBG_DD_DIR=OUTPUT;P1DIR|=DD_BIT;}
//#define RTS_DIR_OFF   {P1DIR&=~DC_BIT;DBG_DC_DIR=INPUT;}
//#define RTS_DIR_ON    {DBG_DC_DIR=OUTPUT;P1DIR|=DC_BIT;}

//#define BSLTST         DC           // // RTS - TEST - TCK
//#define BSLTCK         DD           // // DTR - RESET
//#define BSLRST         DBG_RESET     // // DTR - RESET
//#define BSLRST_ON      DBG_RESET=0     // // DTR - RESET
//#define BSLRST_OFF     DBG_RESET=1     // // DTR - RESET
//#define TCK_DIR_OFF   {P1DIR&=~DD_BIT;DBG_DD_DIR=INPUT;}
//#define TCK_DIR_ON    {DBG_DD_DIR=OUTPUT;P1DIR|=DD_BIT;}
//#define TST_DIR_OFF   {P1DIR&=~DC_BIT;DBG_DC_DIR=INPUT;}
//#define TST_DIR_ON    {DBG_DC_DIR=OUTPUT;P1DIR|=DC_BIT;}


#define bslCnt      OsParam.wrk.d8[0]
#define bslCRC      OsParam.wrk.d16[1]
#define bslCRCL     OsParam.wrk.d8[2]
#define bslCRCH     OsParam.wrk.d8[3]
#define XAdr        OsParam.FlashAddr.A32
#define xBuf        OsParam.flashBuf
#define xCnt        OsParam.flashCnt

#define oddFlags    (1<<3)
//const uint8_t patch[]= {0x31,0x40,0x1A,0x02,0x09,0x43,0xB0,0x12,0x2A,0x0E,0xB0,0x12,0xBA,0x0D,0x55,0x42
//                        ,0x0B,0x02,0x75,0x90,0x12,0x00,0x1F,0x24,0xB0,0x12,0xBA,0x02,0x55,0x42,0x0B,0x02
//                        ,0x75,0x90,0x16,0x00,0x16,0x24,0x75,0x90,0x14,0x00,0x11,0x24,0xB0,0x12,0x84,0x0E
//                        ,0x06,0x3C,0xB0,0x12,0x94,0x0E,0x03,0x3C,0x21,0x53,0xB0,0x12,0x8C,0x0E,0xB2,0x40
//                        ,0x10,0xA5,0x2C,0x01,0xB2,0x40,0x00,0xA5,0x28,0x01,0x30,0x40,0x42,0x0C,0x30,0x40
//                        ,0x76,0x0D,0x30,0x40,0xAC,0x0C,0x16,0x42,0x0E,0x02,0x17,0x42,0x10,0x02,0xE2,0xB2
//                        ,0x08,0x02,0x14,0x24,0xB0,0x12,0x10,0x0F,0x36,0x90,0x00,0x10,0x06,0x28,0xB2,0x40
//                        ,0x00,0xA5,0x2C,0x01,0xB2,0x40,0x40,0xA5,0x28,0x01,0xD6,0x42,0x06,0x02,0x00,0x00
//                        ,0x16,0x53,0x17,0x83,0xEF,0x23,0xB0,0x12,0xBA,0x02,0xD3,0x3F,0xB0,0x12,0x10,0x0F
//                        ,0x17,0x83,0xFC,0x23,0xB0,0x12,0xBA,0x02,0xD0,0x3F,0x18,0x42,0x12,0x02,0xB0,0x12
//                        ,0x10,0x0F,0xD2,0x42,0x06,0x02,0x12,0x02,0xB0,0x12,0x10,0x0F,0xD2,0x42,0x06,0x02
//                        ,0x13,0x02,0x38,0xE3,0x18,0x92,0x12,0x02,0xBF,0x23,0xE2,0xB3,0x08,0x02,0xBC,0x23
//                        ,0x30,0x41,
//                       };
//
//uint8_t bsl430F2121Init(void);
//uint8_t bsl430F149PageErase(void);
//uint8_t bsl430F149WriteFlash(void);
//uint8_t bsl430F149ReadMemory(void);
//uint8_t bsl430F2121Exit(void);
uint8_t bsl430F2121Init(void);
uint8_t bsl430F2121Unprotect(void);
uint8_t bsl430F2121Read(void);
uint8_t bsl430F2121Erase(void);
uint8_t bsl430F2121Write(void);
uint8_t bsl430F2121Verify(void);
uint8_t bsl430F2121Protect(void);
uint8_t bsl430F2121Exit(void);
////
//void bsl430F149crc_xor(uint8_t value); //,uint16_t crcInit
//uint8_t bsl430F149Cmd(uint8_t bslCommand); //,__xdata uint8_t *buf,uint32_t Addr, uint16_t length

//void bsl430F149crc_xor(uint8_t value)    //__xdata uint8_t *buf, uint16_t length
//{
//    if(OsParam.Flags & oddFlags) bslCRCH ^= value;
//    else bslCRCL ^= value;
//    OsParam.Flags ^= oddFlags;
//}
////
//void bsl430F149_SEND(uint8_t Q)
//{
//    U0CSR &= ~UxCSR_TX_BYTE;
//    U0DBUF = Q;
//    bsl430F149crc_xor(Q);
//    while(!(U0CSR & UxCSR_TX_BYTE));
//}
//void  bsl430F149_SEND_CORE(uint8_t Q)
//{
//    U0CSR &= ~UxCSR_TX_BYTE;
//    U0DBUF = Q;
//    bsl430F149crc_xor(Q);
//    while(!(U0CSR & UxCSR_TX_BYTE));
////    --xCnt;
//}
//
//uint8_t bsl430F149_READ(uint8_t *ch)
//{
//    SetAlarm(4);
//    while(OsParam.Alarm!=0)
//    {
//        if(OsParam.uartIn != OsParam.uartOut)
//        {
//            *ch=uartBuf[OsParam.uartOut++];
//            bsl430F149crc_xor(*ch);
//            return 0;
//        }
//    }
//    return 0xFF;
//}
//
//uint8_t bsl430F149Cmd(uint8_t bslCommand)
//{
//    uint8_t rsp;
//// SYNC BSL
//    bsl430F149_SEND(BSL_HDR);
//    if(bsl430F149_READ(&rsp) || (rsp != BSL_ACK)) return -1;
//    bslCRC=0x0000;
//    OsParam.Flags &= ~oddFlags;
//// CMD
//    bsl430F149_SEND(BSL_HDR);
//    bsl430F149_SEND(bslCommand);
//// Total Length
//    bsl430F149_SEND(xCnt+4);
//    bsl430F149_SEND(xCnt+4);
////
//    if(bslCommand==CMD_BAUD) goto SEND_DATA;
//// ADDRESS
//    bsl430F149_SEND(xAdr&0x000000FF);
//    bsl430F149_SEND((xAdr>>8)&0x000000FF);
////    Data Length
//    switch(bslCommand)
//    {
//    case RX_PASSWORD:
//    case RX_DATA_BLOCK:
//        bsl430F149_SEND(xCnt);
//        bsl430F149_SEND(0);
//        break;
//    case TX_DATA_BLOCK:
////BSL responds 80 xx n n D1 D2 ... ... ... ... ... Dn CKL CKH
//        bsl430F149_SEND(128);
//        bsl430F149_SEND(0);
//        break;
//    case ERASE_SEGMENT:
//        bsl430F149_SEND(0x02);
//        bsl430F149_SEND(0xA5);
//        break;
////    case ERASE_MAIN_OR_INFO:
////        bsl430F149_SEND_CORE(0x04);
////        bsl430F149_SEND_CORE(0xA5);
//        break;
//    case MASS_ERASE:
//        bsl430F149_SEND(0x06);
//        bsl430F149_SEND(0xA5);
//        break;
//    case LOAD_PC:
//    case ERASE_CHECK:
//    case BSL_VERSION:
//// BSL responds 80 xx 10 10 D1 D2 ... ...    D10 CKL CKH 
//        bsl430F149_SEND(0);
//        bsl430F149_SEND(0);
//        break;
////    case CMD_BAUD:
////        break;
//    case SET_MEM_OFFSET:
//        bsl430F149_SEND(xAdr&0x000000FF);
//        bsl430F149_SEND((xAdr>>8)&0x000000FF);
//        break;
//    default :
//        return -1;
//        break;
//    }
//// DATA
//SEND_DATA:
//    while(xCnt--)
//    {
//        bsl430F149_SEND(*xBuf++);
//    }
//// CRC
//    bsl430F149_SEND(~bslCRCL);
//    bsl430F149_SEND(~bslCRCH);
//// Respond
//    switch(bslCommand)
//    {
//    case RX_PASSWORD:
//    case RX_DATA_BLOCK:
//    case ERASE_SEGMENT:
////    case ERASE_MAIN_OR_INFO:
//    case MASS_ERASE:
//    case LOAD_PC:
//    case ERASE_CHECK:
//    case CMD_BAUD:
//    case SET_MEM_OFFSET:
//        if(bsl430F149_READ(&rsp) || (rsp !=BSL_ACK)) return -1;
//        else return 0;
//        break;
//    case TX_DATA_BLOCK:
//    //BSL responds 80 xx n n D1 D2 ... ... ... ... ... Dn CKL CKH
//    case BSL_VERSION:
//// BSL responds 80 xx 10 10 D1 D2 ... ...    D10 CKL CKH 
//        break;
//    default :
//        return -1;
//        break;
//    }
//// Receive data from BSL
//    bslCRC=0x0000;
//    OsParam.Flags &= ~oddFlags;
//    xBuf = (__xdata uint8_t *)XRDATA;
////      HEADER
//    if(bsl430F149_READ(&rsp) || (rsp!=BSL_HDR)) return -1;
//// Dummy read
//    if(bsl430F149_READ(&rsp)) return -1;
////      Length
//    if(bsl430F149_READ(&rsp)) return -1;
//    xCnt=rsp;
//    bslCnt=rsp;
////    if((bslCommand==BSL_VERSION) && (rsp != 0x10) ) return -1;
//    if(bsl430F149_READ(&rsp) || (rsp!=bslCnt)) return -1;
//// Read Buff
//    while(bslCnt--)
//    {
//        if(bsl430F149_READ(&rsp)) return -1;
//        *xBuf++=rsp;
//    }
//// Check CRC
//    if(bsl430F149_READ(&rsp) || (bslCRCL!=0xFF))  return -1;
//    if(bsl430F149_READ(&rsp) || (bslCRCH!=0xFF))  return -1;
//    return 0;
//}
////

uint8_t bsl430F2121Init(void)
{
// use TEST
    uint8_t i;
//
    BSLRST_OFF;   // DTR - RESET
    BSLTST=0;   // RTS - TEST - TCK
    TST_DIR_ON;
    BSLTCK=1;   // RTS - TEST - TCK
    TCK_DIR_ON;
    delay(10);
//
    SPI_DISABLE;
    for(i=0; i<8; i++) store[i] = CDC_LINE_CODING.b8[i];
    CDC_LINE_CODING.dwDTERate=9600;
    CDC_LINE_CODING.bDataBits=8;
    CDC_LINE_CODING.bParityType=CDC_PARITY_TYPE_EVEN;
    CDC_LINE_CODING.bStopBits=CDC_CHAR_FORMAT_1_STOP_BIT;
    RTOSfunction.osUartSet();
    UART_ENABLE;
//
    OsParam.uartIn=OsParam.uartOut=0;
    UTX0IF = 0;
    URX0IF = 0;
    URX0IE=1; // USART0 RX interrupt enable
//    for(i=10; i; i--)
    {
//        bslStart();
//        BSLDTR=0;
//        delay(10);    // DTR - RESET

        BSLTST=1;
        BSLTCK=0;
        delay(10); // RTS - TEST - TCK
        BSLTST=0;
        BSLTCK=1;
        delay(10);

        BSLTST=1;
        BSLTCK=0;
        delay(10); // RTS - TEST - TCK
        BSLTST=0;
        BSLTCK=1;
        delay(10);

        BSLTST=1;
        BSLTCK=0;
        delay(10); // RTS - TEST - TCK
        BSLRST_ON;
        delay(10);
        BSLTST=0;
        BSLTCK=1;
        delay(10);

        SetAlarm(2);
        while(OsParam.Alarm)
        {
            NOP();
        };
        OsParam.uartIn=OsParam.uartOut=0;
    }
//   BSL should now be running!
    if(bsl430F149_READ(&i)==0)
    {
        RTOSfunction.osPrintf("ErrBSL...\n");
        return 1;
    }
    else RTOSfunction.osPrintf("Erase...\n");
//  ERASE
    xCnt=0;
    xAdr=0;
    if(bsl430F149Cmd( MASS_ERASE )) return 1;
// RX PASSWORD
//    RTOSfunction.osPrintf("Passw...\n");
    xBuf=(__xdata uint8_t *)XRDATA;
    for(i=32; i; i--) *xBuf++=0xFF;
//
    xBuf=(__xdata uint8_t *)XRDATA;
    xCnt=32;
    if(bsl430F149Cmd(RX_PASSWORD)) return 2;
//
    xBuf=(__xdata uint8_t *)XRDATA;
    xAdr=0x0FF0;
    xCnt = 0;
    if(bsl430F149Cmd( TX_DATA_BLOCK )) return 1;
    xBuf=(__xdata uint8_t *)(XRDATA);
    RTOSfunction.osPrintf("CHIP ID : %02X%02X\n",xBuf[0],xBuf[1]); // CHIP ID : 51351212
    RTOSfunction.osPrintf("BSL : %02X.%02X\n",xBuf[10],xBuf[11]);
//// Patch
//    xAdr=0x0C22;
//    xCnt=0;
//    RTOSfunction.osPrintf("Patch\n");
//    if(bsl430F149Cmd(LOAD_PC)) return 4;
//    xBuf=(__xdata uint8_t *)XRDATA;
////
//    xBuf=(__xdata uint8_t *)XRDATA;
//    for(i=32; i; i--) *xBuf++=0xFF;
//    xBuf=(__xdata uint8_t *)XRDATA;
//    xCnt=32;
//    if(bsl430F149Cmd(RX_PASSWORD)) return 2;
////
//    xAdr=0x0220;
//    xCnt=sizeof(patch);
//    for(i=0; i<sizeof(patch); i++) xBuf[i]=patch[i];
//    if(bsl430F149Cmd( RX_DATA_BLOCK )) return 5;
////    RTOSfunction.osPrintf("RAM\n");
    return 0;
}


uint8_t bsl430F2121Unprotect(void)
{
    return 0;
}
uint8_t bsl430F2121Read(void)
{
    xBuf=(__xdata uint8_t *)XRDATA;
    xCnt = 0;
    return bsl430F149Cmd( TX_DATA_BLOCK );
}

uint8_t bsl430F2121Erase(void)
{
    xAdr=RTOSfunction.osGetDigit();
    RTOSfunction.osPrintf("0x%08lX ",xAdr);
    xCnt=0;
    return bsl430F149Cmd( ERASE_SEGMENT ); //
}
uint8_t bsl430F2121Write(void)
{
    return bsl430F149Cmd( RX_DATA_BLOCK );
}


uint8_t bsl430F2121Verify(void)
{
    return 0;
}
uint8_t bsl430F2121Protect(void)
{
    return 0;
}
uint8_t bsl430F2121Exit(void)
{
    uint8_t i;
//#define PMMCTL0 0x0120
//#define PMMCTL0_UNLOCK 0xA5
//#define PMMSWPOR (1<<3)
//    XAdr=PMMCTL0+1;
//    xBuf=(__xdata uint8_t *)XRDATA;
//    xCnt = 1+3+1;
//    *xBuf=PMMCTL0_UNLOCK;
//    bsl430F149Cmd( RX_DATA_BLOCK );
////   if(bsl430Cmd( RX_DATA_BLOCK )) return 1;
//    XAdr=PMMCTL0+0;
//    xBuf=(__xdata uint8_t *)XRDATA;
//    xCnt = 1+3+1;
//    *xBuf=PMMSWPOR;
//    bsl430F149Cmd( RX_DATA_BLOCK );
//   if(bsl430Cmd( RX_DATA_BLOCK )) return 1;
//
    URX0IE=0; // USART0 RX interrupt enable
    UART_DISABLE;
    for(i=0; i<8; i++) CDC_LINE_CODING.b8[i] = store[i];
//C:\Prj\UsbFlash\UsbBsl.c
    BSLRST_OFF;   // DTR - RESET
//    BSLRTS=1;   // RTS - TEST - TCK
    delay(32);
    BSLRST_ON;   // DTR - RESET
    delay(32);
    TST_DIR_OFF;
    TCK_DIR_OFF;
    return 0;
}
uint8_t __code bsl430F2121_Desk[] = "MSP430F2121 BSL v2.2";
__code struct FlashProg_t bsl430F2121=\
{
    '8',0xFF,bsl430F2121_Desk,\
    bsl430F2121Init,bsl430F2121Unprotect,bsl430F2121Read,bsl430F2121Erase,\
    bsl430F2121Write,bsl430F2121Verify,bsl430F2121Protect,bsl430F2121Exit
};
//struct FlashProg_t* __code  __at (FUNCROM + 8*3) bsl430F2121Prog[1] =  {&bsl430F2121};
#endif // bsl430F149_H_INCLUDED
