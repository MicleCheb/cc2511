#ifndef TWICONN_H_INCLUDED
#define TWICONN_H_INCLUDED

#include <stdint.h>

#define TWICLK             DC                 // P1_1
#define TWICLK_0           {NOP();NOP();NOP();DC=0;NOP();NOP();NOP();}
#define TWICLK_1           {NOP();NOP();NOP();DC=1;NOP();NOP();NOP();}
#define TWICLK_DIR         DBG_DC_DIR     // P1_2
#define TWICLK_BIT         (1<<1)
#define TWICLK_INPUT       DC_INPUT      //{P1DIR&=~DC_BIT;DBG_DC_DIR=INPUT;}
#define TWICLK_OUTPUT      DC_OUTPUT    //{DBG_DC_DIR=OUTPUT;P1DIR|=DC_BIT;}

#define TWIDAT_DIR         DBG_DD_DIR          // P1_6
#define TWIDAT             DD                 // P1_7
#define TWIDAT_0           DD=0                 // P1_7
#define TWIDAT_1           DD=1                 // P1_7
#define TWIDAT_BIT         (1<<7)
#define TWIDAT_INPUT       DD_INPUT    //{P1DIR&=~DD_BIT;DBG_DD_DIR=INPUT;}
#define TWIDAT_OUTPUT      DD_OUTPUT   //{DBG_DD_DIR=OUTPUT;P1DIR|=DD_BIT;}
//
uint8_t twiConnInit(void);
uint8_t twiConnErase(void);
uint8_t twiConnWrite(void);
uint8_t twiConnRead(void);
uint8_t twiConnExit(void);

void TwiToUsb(void);
#define inTwi   OsParam.uartIn
#define outTwi  OsParam.uartOut
#define MAXTWI  16
void twiSend(uint8_t ch)
{
    uint8_t i;
//    TWICLK_0;
    TWICLK_OUTPUT;
    TWIDAT_OUTPUT;
//    RTOSfunction.osPrintf("Ch=%c\n",ch);
    for(i=8; i; i--)
    {
        if(ch&1) TWIDAT_1;
        else TWIDAT_0;
        ch>>=1;
        TWICLK_1;
        TWICLK_0;
    }
    TWICLK_INPUT;
    TWIDAT_INPUT;
}

void TwiToUsb(void)
{
//    if(outTwi != inTwi)
//    {
//        if(RTOSfunction.osPutUSB(uartBuf[outTwi])) ++outTwi;
//    }
}
void twiRead(void)
{
    uint8_t i,ch,state,cnt;
    TWICLK_INPUT;
    TWIDAT_INPUT;
    i=0;
    inTwi=outTwi=0;
    state=0;
    while(1)
    {
        SetAlarm(8);
        ch=0;
        cnt=2;
        switch(state)
        {
        case 0:
            while((ch != 'G') && (ch != 'T') && (ch != 'R'))
            {
                if(P1IFG&2)
                {
                    ch >>= 1;
                    if(TWIDAT) ch |= 0x80;
                    P1IFG &= ~2;
                }
//            TwiToUsb();
                if(outTwi != inTwi)
                {
                    if(RTOSfunction.osPutUSB(uartBuf[outTwi])) ++outTwi;
                }
                if(OsParam.Alarm==0) break;
            }
            uartBuf[inTwi++] = ch;
            ch=0;
            i=8;
            state=1;
            break;
        case 1:
            while(i)
            {
                if(P1IFG&2)
                {
                    ch >>= 1;
                    if(TWIDAT) ch |= 0x80;
                    P1IFG &= ~2;
                    --i;
                }
                if(OsParam.Alarm==0)
                {
                    --inTwi;
                    break;
                }
            }
            if(ch == ' ')
            {
//                while (!RTOSfunction.osPutUSB('G')) {};
//                while (!RTOSfunction.osPutUSB(' ')) {};
                uartBuf[inTwi++] = ch;
//                i=8;
                state=2;
            }
            else
            {
//                ch=0;
                state=0;
                --inTwi;
            }
            break;
        case 2:
            i=8;
            while(i)
            {
                if(P1IFG&2)
                {
                    ch >>= 1;
                    if(TWIDAT) ch |= 0x80;
                    P1IFG &= ~2;
                    --i;
                }
                if(OsParam.Alarm==0)
                {
                    inTwi -= cnt;
                    break;
                }
            }
//
            if((ch == 0x0D) || ((ch >= '0') && (ch <= '9') ) ||  ((ch >= 'A') && (ch <= 'F') ) )
            {
                uartBuf[inTwi++] = ch;
                ++cnt;
            }
            else
            {
                state=0;
                inTwi -= cnt;
            }
//            while (!RTOSfunction.osPutUSB(ch)) {};
            if(ch == 0x0D)
            {
                state=0;
            }
            else if(cnt==MAXTWI)
            {
                uartBuf[inTwi++] = 0x0D;
                state=0;
            }
            break;
        default:
            state=0;
            break;
        }
        RTOSfunction.osGetUSB(&ch);
        if(ch==0x11) return; // Ctrl-Q
    }
}
uint8_t twiConnInit(void)
{
//    uint8_t ch;
    RTOSfunction.osPrintf("\nTWI>start : (CTRL-Q END)");
    TWICLK_0;
    TWIDAT_0;
    TWICLK_INPUT;
    TWIDAT_INPUT;
    P1IFG=0;
    PICTL |= (1<<1);   // falling //rising edge
    delay(32);
//    OsParam.Flags|=CdC;
//    while(OsParam.Flags&CdC)
//    {
//// RX
    twiRead();
//// TX
//        while(RTOSfunction.osGetUSB(&ch))
//        {
//            if(ch==0x11) // Ctrl-Q
//            {
//                OsParam.Flags &= ~CdC;
//            }
//            else
//            {
//                twiSend(ch);
//            }
//        }
//        P1IFG = 0;
//    };
    TWICLK_INPUT;
    TWIDAT_INPUT;
    TWICLK_0;
    TWIDAT_0;
    delay(32);
    RTOSfunction.osPrintf("\nTWI>end\n");
    return -1;
}
//
uint8_t twiConnExit(void)
{
    return 0;
}
//
uint8_t __code twiConn_Desc[] = "Simple TWI";
__code  struct FlashProg_t twiConn=\
{
    '5',0xFF,twiConn_Desc,\
    twiConnInit,twiConnExit,twiConnExit,twiConnExit,twiConnExit,twiConnExit,twiConnExit,twiConnExit
};
//struct FlashProg_t* __code  __at (FUNCROM + 5*3) TWIProg[1] =  {&twiConn};
#endif // TWICONN_H_INCLUDED
