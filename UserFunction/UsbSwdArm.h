#ifndef USBSWDARM_H_INCLUDED
#define USBSWDARM_H_INCLUDED
#include <stdint.h>

uint8_t swdRWreg(uint8_t reg); // ,uint32_t *value

uint8_t swdArmInit(void);
uint8_t swdArmErase(void);
uint8_t swdArmWriteFlash(void);
uint8_t swdArmReadMemory(void);
uint8_t swdArmExit(void);

//uint8_t UserInit(void);
//uint8_t UserUnprotect(void);
//uint8_t UserRead(void);
//uint8_t UserErase(void);
//uint8_t UserWrite(void);
//uint8_t UserVerify(void);
//uint8_t UserProtect(void);
//uint8_t UserExit(void);
//extern const struct FlashProg_t swdArmFun;

#define STM32_FLASH_BASE_ADR 0x08000000

#define JTAG2SWD    0xE79E
#define START_BIT   (1<<0)
#define AP_BIT      (1<<1)   // Access Port
#define DP_BIT      (0<<1)   // Debug Port
#define RD_BIT      (1<<2)   // Read
#define WR_BIT      (0<<2)   // Write
#define A2_BIT      (1<<3)
#define A3_BIT      (1<<4)
//  Bit 5 is a parity bit
//  This bit should be 1 if bits 1-4 contains an odd number of 1's. If the number
//  of 1's are even, the parity bit should be zero.
#define EVEN_BIT    (1<<5) // #define PrG (1<<5)  // Flash Programming
#define STOP_BIT    (0<<6)
#define PARK_BIT    (1<<7)
#define AP_DP_REG   (START_BIT+STOP_BIT+PARK_BIT)
// ACK responses
#define ACK_OK      1
#define ACK_WAIT    2
#define ACK_FAULT   4
// Address of DP read registers
#define DP_IDCODE   0
#define DP_CTRL     1
#define DP_RESEND   2
#define DP_RDBUFF   3
// Adressses of DP write registers
#define DP_ABORT    0
#define DP_STAT     1
#define DP_SELECT   2
// AHB-AP registers
#define AP_CSW      0
#define AP_TAR      1
#define AP_DRW      3
#define AP_IDR      3    // In bank 0xF
//// AAP registers
//#define AAP_CMD     0
//#define AAP_CMDKEY  1
//#define AAP_STATUS  2
//#define AAP_IDR     3  // In bank 0xf
////
//#define AAP_CMD_ZERO      0xF0E00000
//#define AAP_CMDKEY_ZERO   0xF0E00004
//#define AAP_STATUS_ZERO   0xF0E00008
//#define AAP_IDR_ZERO      0xF0E000FC
//// AAP bit fields
//#define AAP_CMD_DEVICEERASE     1
//#define AAP_CMD_SYSRESETREQ     2
//#define AAP_STATUS_ERASEBUSY    1
////  Key which must be written to AAP_CMDKEY before
////  writing to AAP_CMD
//#define AAP_UNLOCK_KEY 0xcfacc118

// Bit fields for the CSW register
#define AP_CSW_16BIT_TRANSFER   0x01
#define AP_CSW_32BIT_TRANSFER   0x02
#define AP_CSW_AUTO_INCREMENT   0x10
#define AP_CSW_PKT_INCREMENT    0x20
#define AP_CSW_MASTERTYPE_DEBUG ((uint32_t)1 << 29)
#define AP_CSW_HPROT            ((uint32_t)1 << 25)
#define AP_CSW_32 (AP_CSW_32BIT_TRANSFER | AP_CSW_MASTERTYPE_DEBUG | AP_CSW_HPROT | AP_CSW_AUTO_INCREMENT)
#define AP_CSW_16 (AP_CSW_16BIT_TRANSFER | AP_CSW_MASTERTYPE_DEBUG | AP_CSW_HPROT | AP_CSW_PKT_INCREMENT)
// Powerup request and acknowledge bits in CTRL/STAT
#define DP_CTRL_CDBGRSTREQ    ((uint32_t)1 << 26)   // - Debug reset request.
#define DP_CTRL_CDBGRSTACK    ((uint32_t)1 << 27) // - Debug reset acknowledge.
#define DP_CTRL_CDBGPWRUPREQ  ((uint32_t)1 << 28)
#define DP_CTRL_CDBGPWRUPACK  ((uint32_t)1 << 29)
#define DP_CTRL_CSYSPWRUPREQ  ((uint32_t)1 << 30)
#define DP_CTRL_CSYSPWRUPACK  ((uint32_t)1 << 31)
// Defines for Cortex-M debug unit
#define COREDEBUG_ITM_STIM      ((uint32_t)0xE0000000) // STIM word acces
#define COREDEBUG_ITM_ENA       ((uint32_t)0xE0000E00) // ITM Enable
#define COREDEBUG_ITM_TCR       ((uint32_t)0xE0000E80) // ITM Trace Control Reg.
#define COREDEBUG_DHCSR         ((uint32_t)0xE000EDF0) // Debug register
#define COREDEBUG_DEMCR         ((uint32_t)0xE000EDFC) // Debug register address 0xE000EDFC
// Privileged Application Interrupt and Reset Control Register
#define COREDEBUG_AIRCR	        ((uint32_t)0xE000ED0C)
#define SYSRESETREQ             ((uint32_t)0x05FA0004) //+2

#define COREDEBUG_IDCODE        ((uint32_t)0xE0042000)  //_DBGMCU
// STM32F4
//0x1000 = Revision A
//0x1007 = Revision 1
//Bits 15:12 Reserved, must be kept at reset value.
//Bits 11:0 DEV_ID(11:0): Device identifier
//The device ID is 0x423.

// Commands to run/step and let CPU run.
// Write these to DHCSR
#define DEBUG_EN    0xA05F0001
#define RUN_CMD     0xA05F0001
#define STOP_CMD    0xA05F0003
#define STEP_CMD    0xA05F0005
///* Core Debug Register */
//typedef struct
//{
//  __IO uint32_t DHCSR;                        /*!< Debug Halting Control and Status Register       */
//  __O  uint32_t DCRSR;                        /*!< Debug Core Register Selector Register           */
//  __IO uint32_t DCRDR;                        /*!< Debug Core Register Data Register               */
//  __IO uint32_t DEMCR;                        /*!< Debug Exception and Monitor Control Register    */
//} CoreDebug_Type;
///* Memory mapping of Cortex-M3 Hardware */
//#define SCS_BASE            (0xE000E000)                              /*!< System Control Space Base Address    */
//#define ITM_BASE            (0xE0000000)                              /*!< ITM Base Address                     */
//#define CoreDebug_BASE      (0xE000EDF0)                              /*!< Core Debug Base Address              */
//#define SysTick_BASE        (SCS_BASE +  0x0010)                      /*!< SysTick Base Address                 */
//#define NVIC_BASE           (SCS_BASE +  0x0100)                      /*!< NVIC Base Address                    */
//#define SCB_BASE            (SCS_BASE +  0x0D00)                      /*!< System Control Block Base Address    */

//#define CoreDebug_DHCSR_S_HALT_Pos         17                                             /*!< CoreDebug DHCSR: S_HALT Position */
#define CoreDebug_DHCSR_S_HALT_Msk      ((uint32_t)1 << 17)
#define FLASH_KEYR      0x40022004
#define RDPRT_key       0x00A5
#define KEY1            0x45670123
#define KEY2            0xCDEF89AB
#define FLASH_CR        0x40022010
#define FLASH_AR        0x40022014
#define FLASH_SR        0x4002200C
#define OPTWRE          ((uint32_t)1 << 9) //: Option bytes write enable
#define LOCK            ((uint32_t)1 << 7) //Bit 7 LOCK: Lock
#define STRT            ((uint32_t)1 << 6) //Bit 6 STRT: Start
#define MER             ((uint32_t)1 << 2) //Bit 2 MER: Mass erase
#define PER             ((uint32_t)1 << 1) //Bit 1 PER: Page erase
#define PG              ((uint32_t)1 << 0) //Bit 0 PG: Programming
#define BSY             ((uint32_t)1 << 0) //Bit 0 BSY: Busy
#define VC_CORERESET    ((uint32_t)1 << 0)

#define SWD_ERROR_OK                    1
#define SWD_ERROR_WAIT                  2
#define SWD_ERROR_FAULT                 3
#define SWD_ERROR_PROTOCOL              4
#define SWD_ERROR_PARITY                5
#define SWD_ERROR_MCU_LOCKED            6
#define SWD_ERROR_INVALID_IDR           7
#define SWD_ERROR_INVALID_IDCODE        8
#define SWD_ERROR_FLASH_WRITE_FAILED    9
#define SWD_ERROR_UNLOCK_FAILED         10
#define SWD_ERROR_AAP_EXTENSION_FAILED  11
#define SWD_ERROR_LOCK_FAILED           12
#define SWD_ERROR_CLR_DLW_FAILED        13
#define SWD_ERROR_MASS_ERASE_TIMEOUT    14
#define SWD_ERROR_VERIFY_FW_FAILED      15
#define SWD_ERROR_TIMEOUT_WAITING_RESET 16
#define SWD_ERROR_TARGET_NOT_HALTED     17
#define SWD_ERROR_FLASHLOADER_ERROR     18
#define SWD_ERROR_DEVICE_ERASE_FAILED   19
#define SWD_ERROR_TIMEOUT_HALT          20
#define SWD_ERROR_DEBUG_POWER           21
// STM32F1 discovery
//Flash size register
#define FLASHSIZEREG 0x1FFFF7E0
// Flash addresses to retrieve the device unique id
#define UNIQUE_ID    0x1FFFF7E8
// STM32F4 discovery
//Flash size register
//#define FLASHSIZEREG 0x1FFF7A22
// Flash addresses to retrieve the device unique id
//#define UNIQUE_ID    0x1FFF7A10

#include <stdint.h>

#define SWD_PAGE_SIZE 1024
#define XAdr            OsParam.FlashAddr.A32
#define xBuf            OsParam.flashBuf
#define xCnt            OsParam.flashCnt
#define swdReg          OsParam.wrk.d32
//
#define SWDCLK_1            {NOP();DC=1;NOP();}
#define SWDCLK_0            {NOP();DC=0;NOP();}
#define SWDIO()             DD
#define SWDIO_1             DD=1
#define SWDIO_0             DD=0
#define SWDIO_DIR_IN        {P1DIR&=~DD_BIT;DBG_DD_DIR=INPUT;}
#define SWDIO_DIR_OUT       {DBG_DD_DIR=OUTPUT;P1DIR|=DD_BIT;}
#define SWDCLK_DIR_IN       {P1DIR&=~DC_BIT;DBG_DC_DIR=INPUT;}
#define SWDCLK_DIR_OUT      {DBG_DC_DIR=OUTPUT;P1DIR|=DC_BIT;}
//
#define SWDIO_SET_INPUT()   SWDIO_DIR_IN
#define SWDIO_SET_OUTPUT()  SWDIO_DIR_OUT
#define SWDIO_CHECK         (P1DIR&DD_BIT)
//
#define SWD_CLK_CYCLE() {SWDCLK_0;SWDCLK_1;}
//#define SWDIO_CYCLE()   {SWDIO=1;  SWDIO=0;}
//
void SWD_WRITE_BIT(uint8_t wbit)
{
    if(wbit) SWDIO_1;
    else SWDIO_0;
    SWD_CLK_CYCLE();
}
//
uint8_t SWD_READ_BIT(void)
{
    uint8_t rbit;
    SWDCLK_0;
    rbit = SWDIO();
    SWDCLK_1;
    return rbit;
}
// Read Write from an AP or DP register to swdReg
uint8_t swdRWreg(uint8_t reg) //,uint32_t *value
{
    uint8_t j,ACK,mask;
//    uint32_t swdReg;
    mask = 1;
    for(j=4; j; j--)
    {
        mask <<= 1;
        if(mask&reg) reg ^= EVEN_BIT;
    };
//    printf("Reg %02X\n",reg);
    SWDIO_SET_OUTPUT();
    mask = 1;
    for(j=8; j; j--)
    {
        SWD_WRITE_BIT(mask&reg);
        mask<<=1;
    };
//     Turnaround
    SWDIO_SET_INPUT();
    SWD_CLK_CYCLE();
//     Read ACK
    ACK=0;
    for ( j=3; j; j-- )
    {
        ACK >>= 1;
        if(SWD_READ_BIT()) ACK |= (1<<2);
    };
//    printf("Ack %02X\n",ACK);
//     Verify that ACK is OK
    reg &= ~EVEN_BIT;
    if(OsParam.Flags&IgN) ACK = ACK_OK;
    if (ACK == ACK_OK)
    {
// READ
        if(reg&RD_BIT)
        {
            swdReg=0;
            for ( j=32; j; j--)
            {
//             Read bit , Keep track of expected parity
                swdReg >>= 1;
                if(SWD_READ_BIT())
                {
                    swdReg |= 0x80000000;
                    reg ^= EVEN_BIT;
                };
            }
//            *value = swdReg;
//         Read parity bit
            if(SWD_READ_BIT()) reg ^= EVEN_BIT;
//          Turnaround
            SWD_CLK_CYCLE();
//         Verify parity
            if ( (reg&EVEN_BIT) == 0 ) ACK = SWD_ERROR_OK;
            else ACK = SWD_ERROR_PARITY;
        }
        else
// WRITE
        {
//          Turnaround
            SWD_CLK_CYCLE();
            SWDIO_SET_OUTPUT();
//             Write data
//            swdReg = *value;
            for ( j=32; j; j-- )
            {
                if(swdReg&1)
                {
                    SWD_WRITE_BIT(1);
                    reg ^= EVEN_BIT;
                }
                else SWD_WRITE_BIT(0);
                swdReg >>= 1;
            }
//             Write parity bit
            SWD_WRITE_BIT(reg&EVEN_BIT);
        }
    }
    else
    {
//        RTOSfunction.osPrintf("REG %02X ERR %02X\n",reg,ACK); // 0x1BA01477
    }
// 8-cycle idle period. Make sure transaction is clocked through DAP.
    SWDIO_0;
    SWDIO_SET_OUTPUT();
    for(j=8; j; j--) SWD_CLK_CYCLE();
    return ACK;
}
//
//
#define  swdDPRead(reg) {swdRWreg(AP_DP_REG|DP_BIT|RD_BIT|(reg<<3));}
//
#define  swdDPWrite(reg,value) {swdReg = value; swdRWreg(AP_DP_REG|DP_BIT|WR_BIT|(reg<<3));}
//
#define  swdAPRead(reg) {swdRWreg(AP_DP_REG|AP_BIT|RD_BIT|(reg<<3));}
//
#define  swdAPWrite(reg,value) {swdReg = value; swdRWreg(AP_DP_REG|AP_BIT|WR_BIT|(reg<<3));}
//
#define  swdMemWrite(reg,value) {swdAPWrite(AP_TAR,reg); swdAPWrite(AP_DRW,value);}
//
#define  swdMemRead(reg) {swdAPWrite(AP_TAR,reg); swdAPRead(AP_DRW); swdDPRead(DP_RDBUFF);}

uint8_t swdArmDbgReset(void)
{
    OsParam.Flags &= ~IgN;
//   Select first AP bank
    swdDPWrite(DP_SELECT,0);
//   Set transfer size to 32 bit
    swdAPWrite(AP_CSW,AP_CSW_32);
    OsParam.Flags |= B32;
//  Lock flash
    swdMemWrite(FLASH_CR,LOCK);//OPTWRE
//    stm32.sysReset()
    swdMemWrite(COREDEBUG_AIRCR,SYSRESETREQ);
    return 0;
}
//
uint8_t swdArmInit(void)
{
    uint8_t i;
    OsParam.Flags &= ~IgN;
    SWDCLK_1;
    SWDIO_1;
    SWDCLK_DIR_OUT;
    SWDIO_DIR_OUT;
    DelayAlarm(2);
//     First reset line with > 50 cycles with SWDIO high
    SWDIO_SET_OUTPUT();
    SWDIO_1;
    for ( i=64; i; i-- ) SWD_CLK_CYCLE();
//     Transmit 16-bit JTAG-to-SWD sequence
#define swdSeq  OsParam.wrk.d16[0]
    swdSeq=JTAG2SWD;
    for ( i=16; i; i--)
    {
        SWD_WRITE_BIT(swdSeq&1);
        swdSeq>>=1;
    }
//     Do another reset to make sure SW-DP is in reset state
    SWDIO_1;
    for ( i=64; i; i-- ) SWD_CLK_CYCLE();
//     Insert a 16 cycle idle period
    SWDIO_0;
    for ( i=64; i; i-- ) SWD_CLK_CYCLE();
// Read DP.IDCODE
    swdReg = 0;
    swdDPRead(DP_IDCODE);
    RTOSfunction.osPrintf("DP.ID: %08lX\n",(uint32_t)swdReg); // 0x1BA01477
//   Debug power up request
// clear the STICKYERR flags
    swdDPWrite(DP_ABORT,0x1F);
    swdDPWrite(DP_CTRL, DP_CTRL_CSYSPWRUPREQ | DP_CTRL_CDBGPWRUPREQ  | DP_CTRL_CDBGRSTREQ); //
//   Wait until we receive powerup ACK
    for(i=16; i; i--)
    {
        swdDPRead(DP_CTRL);

        if ((OsParam.wrk.d8[3] & 0xF4) == 0xF4)
        {
            RTOSfunction.osPrintf("PWR DP.CTRL: %08lX\n",(uint32_t)swdReg);
            swdDPWrite(DP_CTRL, DP_CTRL_CSYSPWRUPREQ | DP_CTRL_CDBGPWRUPREQ  | 0*DP_CTRL_CDBGRSTREQ); //
            break;
        }
        else
        {
            if(i==1)
            {
                RTOSfunction.osPrintf("DP.CTRL: %08lX\n",(uint32_t)swdReg);
                RTOSfunction.osPrintf("PWR ERR!\n");
                return SWD_ERROR_DEBUG_POWER;
            }
        }
    }
//            swdDPRead(DP_CTRL);
//        RTOSfunction.osPrintf("DP.CTRL: %08lX\n",(uint32_t)swdReg);
//   Read AP ID
//   Select last AP bank  writeDP(DP_SELECT, 0xf0);
    swdDPWrite(DP_SELECT,0xF0);
    swdMemRead(AP_IDR);
    RTOSfunction.osPrintf("AP.ID: %08lX\n",(uint32_t)swdReg);  // 0x14770011
//   Select first AP bank again
    swdDPWrite(DP_SELECT,0);
//   Set transfer size to 32 bit
    swdAPWrite(AP_CSW,AP_CSW_32);
    OsParam.Flags |= B32;
// Halts the target CPU
    swdMemWrite(COREDEBUG_DHCSR,STOP_CMD);
    swdMemRead(COREDEBUG_DHCSR);
// Check HALT
//        RTOSfunction.osPrintf("DHCSR: %08lX\n",(uint32_t)swdReg); // 0x03030003
    if((swdReg&CoreDebug_DHCSR_S_HALT_Msk) == 0)
    {
//            RTOSfunction.osPrintf("CPU NOTHALT DHCSR: %08lX\n",swdReg);
        return SWD_ERROR_TARGET_NOT_HALTED;
    }
    else
    {
        RTOSfunction.osPrintf( "CPU HALT DHCSR: %08lX\n",swdReg);
//            break;
    };
// COREDEBUG_IDCODE
    swdMemRead(COREDEBUG_IDCODE);
    RTOSfunction.osPrintf("CD.ID: %08lX\n",(uint32_t)swdReg);
//UNIQUE_ID
    RTOSfunction.osPrintf( "UNIQUE_ID : ");
    swdMemRead(UNIQUE_ID);
    RTOSfunction.osPrintf("%08lX ",(uint32_t)swdReg);
    swdMemRead(UNIQUE_ID+4);
    RTOSfunction.osPrintf("%08lX ",(uint32_t)swdReg);
    swdMemRead(UNIQUE_ID+8);
    RTOSfunction.osPrintf("%08lX\n",(uint32_t)swdReg);
//
    swdMemRead(FLASHSIZEREG);
    RTOSfunction.osPrintf( "FLASHSIZE : ");
    RTOSfunction.osPrintf("%04X\n",(uint16_t)swdReg);
// STM32F1 discovery
//DP.ID: 1BA01477
//AP.ID: 14770011
//COREDEBUG_IDCODE : 10016420
//UNIQUE_ID : 07A9FFFF FFFFFFFF FFFFFFFF
//FLASHSIZE : FFFF0080

// ST-LINK 2 - SWCLK DC orange
// ST-LINK 4 - SWDIO DD blue

// STM32F4 discovery
//DP.ID: 2BA01477
//AP.ID: 24770011
//COREDEBUG_IDCODE : 10016413
//UNIQUE_ID : 0034002C 32314719 31383435
//FLASHSIZE : 0400FFFF
//    //  unlock main flash
    swdMemWrite(FLASH_KEYR,KEY1);
    swdMemWrite(FLASH_KEYR,KEY2);
    return 0;
}
//
uint8_t swdArmUnprotect(void)
{
    //  unlock main flash
    swdMemWrite(FLASH_KEYR,KEY1);
    swdMemWrite(FLASH_KEYR,KEY2);
    return 0;
}
uint8_t swdArmRead(void)
{
#define ARMBUFSIZE 64
    swdAPWrite(AP_CSW,AP_CSW_32BIT_TRANSFER);
    OsParam.Flags |= B32;
    xCnt = ARMBUFSIZE;
    xAdr &= ~3;
    while(xCnt)
    {
        swdMemRead(XAdr);
        *(uint32_t *)xBuf = swdReg;
        xBuf+=4;
        xCnt-=4;
        XAdr += 4;
    }
    xCnt = ARMBUFSIZE;
    xAdr -= ARMBUFSIZE;
    return 0;
}
//
uint8_t swdArmErase(void)
{
//  enable the mass erase
    swdMemWrite(FLASH_CR,OPTWRE+MER);
//  start the mass erase
    swdMemWrite(FLASH_CR,OPTWRE+STRT+MER);
//  check the BSY flag
    do
    {
        DelayAlarm(2);
        swdMemRead(FLASH_SR);
    }
    while(swdReg&BSY);
    return 0;
}
//
uint8_t swdArmWrite(void)
{
    uint8_t i;
    union wrk_t w;
    if(OsParam.Flags&B32)   // 32 bit Accsess
    {
        swdMemWrite(FLASH_CR,OPTWRE+PG);
//// Select 16bit Accsess + packedInc
//        swdAPWrite(AP_CSW,AP_CSW_16);
        swdAPWrite(AP_CSW,(AP_CSW_16BIT_TRANSFER | AP_CSW_PKT_INCREMENT));
        OsParam.Flags &= ~B32;
    }
    OsParam.Flags |=  IgN;
// align 4
    i = XAdr&3;
    if(i)
    {
        w.d32 = 0xFFFFFFFF;
        XAdr &= ~3;
        swdAPWrite(AP_TAR,XAdr);
        while(i < 4)
        {
            if(xCnt)
            {
                w.d8[i] = *xBuf++;
                --xCnt;
            };
            ++i;
        }
        swdAPWrite(AP_DRW,w.d32);
        delay(1);
    }
    else swdAPWrite(AP_TAR,XAdr);
    while(xCnt)
    {
//        OsParam.wrk.d32 = *(uint32_t *)xBuf;
        w.d8[0] = *xBuf++;
        --xCnt;
        if(xCnt)
        {
            w.d8[1] = *xBuf++;
            --xCnt;
        }
        else w.d8[1] = 0xFF;
        if(xCnt)
        {
            w.d8[2] = *xBuf++;
            --xCnt;
        }
        else w.d8[2] = 0xFF;
        if(xCnt)
        {
            w.d8[3] = *xBuf++;
            --xCnt;
        }
        else w.d8[3] = 0xFF;
        swdAPWrite(AP_DRW,w.d32);
        delay(1);
    }
    OsParam.Flags &= ~IgN;
    return 0;
}
//
uint8_t swdArmVerify(void)
{
    return 0;
}
uint8_t swdArmProtect(void)
{
    return 0;
}
//
uint8_t swdArmExit(void)
{
//    uint8_t i;
    swdArmDbgReset();
// EXIT
    OsParam.Flags&=~B32;
//    SWDCLK_0;
//    SWDIO = 0;
    DelayAlarm(2);
    SWDCLK_DIR_IN;
    SWDIO_DIR_IN;
    return 0;
}
//
uint8_t __code SWDM0Desk[]="STM32F0 SWD";
__code struct FlashProg_t  swdArmM0=\
{ '3',0xFF,SWDM0Desk,swdArmInit,swdArmUnprotect,swdArmRead,swdArmErase,swdArmWrite,swdArmVerify,swdArmProtect,swdArmExit
};
//struct FlashProg_t* __code  __at (FUNCROM + 3*3) SWDM0[1] =  {&swdArmM0};
#endif // USBSWDARM_H_INCLUDED
/*
LPC1114 - NXP   0x400483F4
LPC43XX_CHIPID	0x40043200
DP.ID: 0BB11477
AP.ID: 04770021
CD.ID: 00000000
 r - Read
 e - Erase
 w - Write
 q - Quit
 h - Help
*/
