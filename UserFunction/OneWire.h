#ifndef ONEWIRE_H_INCLUDED
#define ONEWIRE_H_INCLUDED
#include <stdint.h>
//#include <stdio.h>
#include "cc1111.h"



uint8_t OneWireInit(void)
{
    uint8_t ch;
    RTOSfunction.osPrintf("\nOneWire start\n");
//    printUart();
    RTOSfunction.osUartSet();
    UART_ENABLE;
    DelayAlarm(2);
    OsParam.Flags|=CdC;
    OsParam.uartIn=OsParam.uartOut=0;
//    U0CSR |= UxCSR_TX_BYTE;
    UTX0IF=0;
//    U0DBUF =13;
    URX0IF=0;
    URX0IE=1;
    while(OsParam.Flags&CdC)
    {
// RX
        if(OsParam.uartOut != OsParam.uartIn)
        {
            OsParam.uartOut += RTOSfunction.osPutUSB(uartBuf[OsParam.uartOut]);
        }

// TX
//        if(U0CSR&UxCSR_TX_BYTE)
//        {
        if(RTOSfunction.osGetUSB(&ch))
        {
            if(ch==0x11) // Ctrl-Q
            {
                OsParam.Flags &= ~CdC;
            }
//                else
//                {
//                    U0CSR &= ~UxCSR_TX_BYTE;
//                    U0DBUF = ch;
//                }
        }
//        };
    };
    URX0IE=0;
    UART_DISABLE;
    U0CSR &= ~(UxCSR_MODE_UART|UxCSR_RE);
    RTOSfunction.osPrintf("\nOneWire>end\n");
    return -1;
}
uint8_t OneWireExit(void)
{
    return 0;
}
//
uint8_t __code OneWire_Desc[] = "OneWire UART";
__code  struct FlashProg_t OneWire=\
{
    '9',0xFF,OneWire_Desc,\
    OneWireInit,OneWireExit,OneWireExit,OneWireExit,OneWireExit,OneWireExit,OneWireExit,OneWireExit
};
//struct FlashProg_t* __code  __at (FUNCROM + 9*3) OneWireProg[1] =  {&OneWire};



#endif // ONEWIRE_H_INCLUDED
