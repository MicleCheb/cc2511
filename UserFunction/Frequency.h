#ifndef FREQUENCY_H_INCLUDED
#define FREQUENCY_H_INCLUDED
// Frequency input DD (BSLTCK) P1_7

uint8_t freqInit(void);
uint8_t freqExit(void);

#define sumTIME     OsParam.wrk.d32
#define Period      sumTIME
#define tmpTIME     OsParam.FlashAddr.A16[0]
#define oldTIME     OsParam.FlashAddr.A16[0]
#define newTIME     OsParam.FlashAddr.A16[1]
#define cntFREQ     OsParam.flashCnt
#define SYSFREQ     (24000000>>5)
#define flOVF       (1<<3)

uint8_t freqInit(void)
{
    uint8_t CMD;
    uint32_t Freq32;
    RTOSfunction.osPrintf("METER INIT\n");
    RTOSfunction.osPrintf("Q - quit\n");
    P1DIR&=~DD_BIT;
    DBG_DD_DIR=INPUT;
    T1CTL=T1CTL_MODE_FREE;
    T1CNTL = 0;
    CMD=0;
    OsParam.Flags &= ~flOVF;
    do
    {
        cntFREQ=0;
        sumTIME=0;
        tmpTIME=0;
        T1CTL=0;
        T1CNTL = 0;
        SetAlarm(9);
        OsParam.Flags &= ~flOVF;
        P1IFG &= ~(1<<7);
        while(OsParam.Alarm )
        {
            if(P1IFG & (1<<7))
            {
                if(!cntFREQ) T1CTL = T1CTL_MODE_FREE + T1CTL_DIV_32;
                else
                {
                    OsParam.FlashAddr.A8[0]=T1CNTL;
                    OsParam.FlashAddr.A8[1]=T1CNTH;
                };                  //  tmpTIME  = T1CNTL + (T1CNTH<<8);
                ++cntFREQ;
                P1IFG &= ~(1<<7);
                OsParam.Flags &= ~flOVF;
            }
            if(T1CTL & T1CTL_OVFIF)
            {
                sumTIME+=0x10000;
                T1CTL &= ~T1CTL_OVFIF;
                OsParam.Flags |= flOVF;
            }
        };
//        if(T1CTL & T1CTL_OVFIF)
//        {
//            if(tmpTIME < 0x8000) sumTIME += 0x10000;
//        }
        sumTIME += tmpTIME;
        if(OsParam.Flags & flOVF) sumTIME -= 0x10000;
//
//        RTOSfunction.osPrintf("F %lu C %u\n",sumTIME,cntFREQ);
        if(cntFREQ > 1)
        {
            --cntFREQ;
            Period = sumTIME/cntFREQ;
            Freq32 = SYSFREQ/Period;
            RTOSfunction.osPrintf("%lu.",Freq32);
            Freq32 = (SYSFREQ - Period*Freq32)*1000;
            Freq32 /= Period;
            RTOSfunction.osPrintf("%03lu Hz\n",Freq32);
        }
        else  RTOSfunction.osPrintf("No Signal\n");
        RTOSfunction.osGetUSB(&CMD);
//        if(RTOSfunction.osGetUSB(&CMD))  RTOSfunction.osPrintf("CMD %c\n",CMD);
    }
    while(CMD != 'Q');
    return 0+freqExit();
}
uint8_t freqExit(void)
{
    T1CTL=T1CTL_MODE_SUSPENDED;
    OsParam.Flags &= ~flOVF;
//    RTOSfunction.osPrintf("EXIT\n");
    return -1;
}
//
uint8_t __code FREQ_Desk[] = "FREQ METER";
__code struct FlashProg_t  freqMeter=\
{'4',0xFF,FREQ_Desk,freqInit,freqExit,freqExit,freqExit,freqExit,freqExit,freqExit,freqExit
};
//struct FlashProg_t* __code  __at (FUNCROM + 4*3) FreqProg[1] =  {&freqMeter};
#endif // FREQUENCY_H_INCLUDED
