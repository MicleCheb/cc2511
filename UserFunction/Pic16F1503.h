#ifndef PIC16F1503_H_INCLUDED
#define PIC16F1503_H_INCLUDED
#include <stdint.h>

//
uint8_t pic16F1503Init(void);
uint8_t pic16F1503Unprotect(void);
uint8_t pic16F1503Read(void);
uint8_t pic16F1503Erase(void);
uint8_t pic16F1503Write(void);
uint8_t pic16F1503Verify(void);
uint8_t pic16F1503Protect(void);
uint8_t pic16F1503Exit(void);

// Connect the target PIC16F1503 up as follows
#define MCLR                DBG_RESET             //P0_1
#define MCLR_HIGH           MCLR=0
#define MCLR_LOW            MCLR=1

#define ICSPCLK             DC                 // P1_1
#define ICSPCLK_0           {DC=0;NOP();NOP();NOP();NOP();NOP();}
#define ICSPCLK_1           {DC=1;NOP();NOP();NOP();NOP();NOP();}
#define ICSPCLK_DIR         DBG_DC_DIR     // P1_2
#define ICSPCLK_BIT         (1<<1)
#define ICSPCLK_INPUT       DC_INPUT      //{P1DIR&=~DC_BIT;DBG_DC_DIR=INPUT;}
#define ICSPCLK_OUTPUT      DC_OUTPUT    //{DBG_DC_DIR=OUTPUT;P1DIR|=DC_BIT;}

#define ICSPDAT_DIR         DBG_DD_DIR          // P1_6
#define ICSPDAT             DD                 // P1_7
#define ICSPDAT_BIT         (1<<7)
#define ICSPDAT_INPUT       DD_INPUT    //{P1DIR&=~DD_BIT;DBG_DD_DIR=INPUT;}
#define ICSPDAT_OUTPUT      DD_OUTPUT   //{DBG_DD_DIR=OUTPUT;P1DIR|=DD_BIT;}

#define MCHP_WORD       "MCHP"

// ICSP COMMAND
#define LOAD_CONFIG     0x00    // Load Configuration         00h 0, data (14), 0
#define LOAD_DATA       0x02    //Load Data For Program Memory x 0 0 0 1 0 02h 0, data (14), 0
#define READ_DATA       0x04    //Read Data From Program Memory x 0 0 1 0 0 04h 0, data (14), 0
#define INC_ADR         0x06    //Increment Address x 0 0 1 1 0 06h —
#define RESET_ADR       0x16    //Reset Address x 1 0 1 1 0 16h —
#define BEGIN_PRG_INT   0x08    //Begin Internally Timed Programming x 0 1 0 0 0 08h —
#define BEGIN_PRG_EXT   0x18    //Begin Externally Timed Programming x 1 1 0 0 0 18h —
#define END_PRG_EXT     0x0A    //End Externally Timed Programming x 0 1 0 1 0 0Ah —
#define BULK_ERASE      0x09    //Bulk Erase Program Memory x 0 1 0 0 1 09h Internally Timed
#define ROW_ERASE       0x11    //Row Erase Program Memory x 1 0 0 0 1 11h Internally Timed
// Bulk Erase cycle time 5ms
// Row Erase cycle time 2.5 ms
// Internally timed programming operation time Program memory 2.5 ms
// Internally timed programming operation time Configuration Words 5ms

//#define xAdr  OsParam.FlashAddr.adr
//#define xBuf  OsParam.flashBuf
//#define xCnt  OsParam.flashCnt
#define LastAdr OldAddr
#define cAdr    OsParam.FlashAddr.A16[1]
#define pAdr    OsParam.FlashAddr.A16[0]
#define picCFG  (1<<5)  // pic CONFIG

#define UserID                  0x8000
#define DeviceID                0x8006
#define ConfigurationWord1      0x8007
#define ConfigurationWord2      0x8008
#define CalibrationWord1        0x8009
#define CalibrationWord2        0x800A



//
void pic16FSendCmd(uint8_t Cmd)
{
    uint8_t i;
    ICSPDAT_OUTPUT;
    for (i = 6; i; i--)
    {
        if (Cmd & 0x01) ICSPDAT = 1;
        else ICSPDAT = 0;
        ICSPCLK_1;
        ICSPCLK_0;
        Cmd>>= 1;
    }
    for (i = 12; i; i--) NOP();
}
void pic16FSendByte(uint8_t ch)
{
    uint8_t i;
    ICSPDAT_OUTPUT;
    for (i = 8; i; i--)
    {
        if (ch & 0x01) ICSPDAT = 1;
        else ICSPDAT = 0;
        ICSPCLK_1;
        ICSPCLK_0;
        ch >>= 1;
    }
}
void pic16FSendWord(uint16_t value)
{
//    uint8_t i;
    value &= 0x3FFF;
    value <<= 1;
    pic16FSendByte(value);
    pic16FSendByte(value>>8);
//    for (i = 4; i; i--) NOP();
}
//
uint16_t pic16FRecvWord(void)
{
    uint16_t value=0;
    uint8_t i;
    pic16FSendCmd(READ_DATA);
    ICSPDAT_INPUT;
    for (i = 16; i; i--)
    {
        value >>= 1;
        ICSPCLK_1;
        if (ICSPDAT) value |= 0x8000;
        ICSPCLK_0;
    }
    value >>= 1;
    value &= 0x3FFF;
    return value;
}
//
void pic16FSetAdr(void)
{
    if(pAdr & 1)
    {
        RTOSfunction.osPrintf("ERROR : OddAddress\n");
        return;
    }
//    RTOSfunction.osPrintf("LOAD ADR : %08lX\n",xAdr);
    if(cAdr)
    {
        if(((OsParam.Flags&picCFG)==0) || (pAdr<LastAdr))
        {

            pic16FSendCmd(LOAD_CONFIG);
            pic16FSendWord(0x3FFF);
            LastAdr = 0x0000;
            OsParam.Flags |= picCFG;
        }
    }
    else
    {
        if((OsParam.Flags&picCFG) || (pAdr<LastAdr))
        {
            pic16FSendCmd(RESET_ADR);
            LastAdr = 0x0000;
            OsParam.Flags &= ~picCFG;
        }
    }
    while(LastAdr < pAdr)
    {
        pic16FSendCmd(INC_ADR);
        LastAdr += 2;
    }
}
//
uint8_t pic16F1503Init(void)
{
    ICSPCLK_0;
    ICSPCLK_OUTPUT;
    ICSPDAT=0;
    ICSPDAT_OUTPUT;
//    delay(10);
    MCLR_LOW;
    delay(12);
    pic16FSendByte('P');
    pic16FSendByte('H');
    pic16FSendByte('C');
    pic16FSendByte('M');
//    0100 1101 0100 0011 0100 1000 0101 0000
    ICSPCLK_1;
    ICSPCLK_0;
// Read UserID
    pic16FSendCmd(LOAD_CONFIG);
    pic16FSendWord(0x3FFF);
    LastAdr=0;
    OsParam.Flags |= picCFG;
    RTOSfunction.osPrintf("UD1: %04X\n",pic16FRecvWord());
    pic16FSendCmd(INC_ADR);
    RTOSfunction.osPrintf("UD2: %04X\n",pic16FRecvWord());
    pic16FSendCmd(INC_ADR);
    RTOSfunction.osPrintf("UD3: %04X\n",pic16FRecvWord());
    pic16FSendCmd(INC_ADR);
    RTOSfunction.osPrintf("UD4: %04X\n",pic16FRecvWord());
    pic16FSendCmd(INC_ADR);
    pic16FSendCmd(INC_ADR);
    pic16FSendCmd(INC_ADR);
// Read DeviceID
    RTOSfunction.osPrintf("DEVID: %04X\n",pic16FRecvWord());
    pic16FSendCmd(INC_ADR);
//  Read Config
    RTOSfunction.osPrintf("Config1: %04X\n",pic16FRecvWord());
    pic16FSendCmd(INC_ADR);
    RTOSfunction.osPrintf("Config2: %04X\n",pic16FRecvWord());
    pic16FSendCmd(INC_ADR);
// Read Calibration
    RTOSfunction.osPrintf("CW1: %04X\n",pic16FRecvWord());
    pic16FSendCmd(INC_ADR);
    RTOSfunction.osPrintf("CW2: %04X\n",pic16FRecvWord());
//
    pic16FSendCmd(RESET_ADR);
    LastAdr=0;
    OsParam.Flags &= ~picCFG;
//    pic16FSendWord(0x3FFF);
    return 0;
}

uint8_t pic16F1503Unprotect(void)
{
    return 0;
}
uint8_t pic16F1503Read(void)
{
    uint16_t d;
    xBuf=(__xdata uint8_t *)XRDATA;
    for(xCnt=0; xCnt<32; xCnt+=2)
    {
        pic16FSetAdr();
        d=pic16FRecvWord();
        *xBuf++ = d&0x00FF;
        *xBuf++ = d>>8;
        pAdr += 2;
    }
    pAdr -= 32;
    xBuf=(__xdata uint8_t *)XRDATA;
    return 0;
}
uint8_t pic16F1503Erase(void)
{
    pic16FSendCmd(BULK_ERASE);
    delay(200);
    return 0;
}
uint8_t pic16F1503Write(void)
{
    uint16_t d;
    while(xCnt >= 2)
    {
        d  = *xBuf++;
        d |= (*xBuf++) <<8;
        xCnt -= 2;
        if(cAdr)
        {
            pic16FSendCmd(LOAD_CONFIG);
            pic16FSendWord(d);
            LastAdr = 0x0000;
            OsParam.Flags |= picCFG;
            while(LastAdr < pAdr)
            {
                pic16FSendCmd(INC_ADR);
                LastAdr += 2;
            }
            pic16FSendCmd(BEGIN_PRG_INT);
            delay(200);
        }
        else
        {
            pic16FSetAdr();
            pic16FSendCmd(LOAD_DATA);
            pic16FSendWord(d);
        }
        pAdr += 2;
    }
    if(!cAdr)
    {
        pic16FSendCmd(BEGIN_PRG_INT);
        delay(100);
    }
    return xCnt;
}
//
uint8_t pic16F1503Verify(void)
{
    return 0;
}
uint8_t pic16F1503Protect(void)
{
    return 0;
}
uint8_t pic16F1503Exit(void)
{
//    MCLR_LOW;
//    delay(32);
    OsParam.Flags &= ~picCFG;
    ICSPCLK_INPUT;
    ICSPDAT_INPUT;
    MCLR_HIGH;
    delay(32);
    return 0;
}
//
uint8_t __code pic16F1503_Desc[] = "Microchip PIC16F1503 LVP";
__code struct FlashProg_t pic16F1503=\
{
    '6',0xFF,pic16F1503_Desc,\
    pic16F1503Init,pic16F1503Unprotect,pic16F1503Read,pic16F1503Erase,\
    pic16F1503Write,pic16F1503Verify,pic16F1503Protect,pic16F1503Exit
};
//struct FlashProg_t* __code  __at (FUNCROM + 6*3) pic16F1503Prog[1] =  {&pic16F1503};
#endif // PIC16F1503_H_INCLUDED
