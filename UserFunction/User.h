#ifndef USER_H_INCLUDED
#define USER_H_INCLUDED

#include <stdint.h>
//#include <stdio.h>
#include <string.h>
#include "cc1111.h"

#define USER_FUN 0
#define NEWUSERFUN 0
//    __xdata uint8_t RamBuf[1024];
#define FLASH_PAGE_SIZE (uint16_t)1024

#define FLASH_SIZE FLASH_PAGE_SIZE*32
#define USER_CODE_BASE 0x2400
#define memAddr 0xF000
#define XRDATA 0xF400
//
#define MaskOff   0x00
#define MaskFind  0x27
#define MaskProg  0x23
#define MaskNorma 0x01
#define MaskUART  0x03

#define Rled_on   {P2_3 = 1;}
#define Rled_off  {P2_3 = 0;}
#define Rled_flp  {P2_3^= 1;}
#define Gled_on   {P2_4 = 1;}
#define Gled_off  {P2_4 = 0;}
#define Gled_flp  {P2_4^= 1;}

#define NONE    0
#define YES     1
#define INPUT   0
#define OUTPUT  1

#define DBG_RESET       P0_1
#define RST_ON          DBG_RESET=1
#define RST_OFF         DBG_RESET=0

#define DBG_DC_DIR      P1_2
#define DC              P1_1
#define DBG_DC          DC
#define DC_BIT          (1<<1)
#define DC_INPUT        {P1DIR&=~DC_BIT;DBG_DC_DIR=INPUT;}
#define DC_OUTPUT       {DBG_DC_DIR=OUTPUT;P1DIR|=DC_BIT;}

#define DBG_DD_DIR      P1_6
#define DD              P1_7
#define DBG_DD          DD
#define DD_BIT          (1<<7)
#define DD_INPUT        {P1DIR&=~DD_BIT;DBG_DD_DIR=INPUT;}
#define DD_OUTPUT       {DBG_DD_DIR=OUTPUT;P1DIR|=DD_BIT;}
//
#define CDC_CHAR_FORMAT_1_STOP_BIT     0
#define CDC_CHAR_FORMAT_1_5_STOP_BIT   1
#define CDC_CHAR_FORMAT_2_STOP_BIT     2

#define CDC_PARITY_TYPE_NONE           0
#define CDC_PARITY_TYPE_ODD            1
#define CDC_PARITY_TYPE_EVEN           2
#define CDC_PARITY_TYPE_MARK           3
#define CDC_PARITY_TYPE_SPACE          4

#define SPI_DISABLE     {P0_0 = 1; P0DIR |= (1<<0);}
#define SPI_ENABLE      {P0_0 = 0; P0DIR |= (1<<0);}
#define UART_DISABLE    {P1_3 = 1; P1DIR |= (1<<3);}
#define UART_ENABLE     {P1_3 = 0; P1DIR |= (1<<3);}
struct FlashProg_t
{
    uint8_t ind;
    uint8_t desc;
    uint8_t __code *descriptor;
    uint8_t (*flInit)(void);
    uint8_t (*flUnProtect)(void);
    uint8_t (*flRead)(void);
    uint8_t (*flErase)(void);
    uint8_t (*flWrite)(void);
    uint8_t (*flVerify)(void);
    uint8_t (*flProtect)(void);
    uint8_t (*flExit)(void);
};
#define MAX_USER_PROGRAM 16
#define FUNCROM (FLASH_SIZE - MAX_USER_PROGRAM*3)
__code __at (FUNCROM) struct FlashProg_t*  FlashProg[MAX_USER_PROGRAM];

struct RTOSfunction_t
{
    uint8_t  (*osGetUSB)(uint8_t *ch);
    uint8_t  (*osPutUSB)(uint8_t ch);
    uint8_t  (*osUartSet)(void);
    uint8_t  (*osFlasher)(void)  __reentrant; //struct FlashProg_t *chipID
    uint8_t  (*osIntelHex)(void);
    int      (*osPrintf)(const char *,...);
    uint32_t (*osGetDigit)(void);
};
#define RTOSROM (USER_CODE_BASE - sizeof(struct RTOSfunction_t))
//#define RTOSROM (USER_CODE_BASE - sizeof(struct RTOSfunction_t) - 16*sizeof(struct FlashProg_t))
__code __at (RTOSROM) struct RTOSfunction_t RTOSfunction;

union FlashAddr_t
{
    uint32_t A32;
    uint32_t LinAdr;
    uint16_t A16[2];
    uint8_t A8[4];
    uint16_t adr;
};
union wrk_t
{
    uint32_t d32;
    uint16_t d16[2];
    uint8_t d8[4];
};
struct OsParam_t
{
//    volatile uint8_t reserved[0x23-0x08];
    uint8_t Alarm;
    uint8_t TicksMain;
    uint8_t Flags;
//
    uint8_t LedMask;
    uint8_t RedLedMask;
    uint8_t GreenLedMask;
//
    uint8_t usbState;
    uint8_t UsbInCnt;
    uint8_t UsbOutCnt;
//
    uint8_t uartIn;
    uint8_t uartOut;
    uint8_t tmpI;
//
    __xdata uint8_t *flashBuf;
    uint16_t flashCnt;
    union FlashAddr_t FlashAddr;

    union wrk_t wrk;
    __xdata  struct FlashProg_t *FlashMemFunc;
};
struct CDC_LINE_CODING_t
{
    uint32_t dwDTERate;       /// Data terminal rate {57600,0,0,8,2,}
    uint8_t  bStopBits;       /// Number of stop bits
    uint8_t  bParityType;     /// Parity bit type
    uint8_t  bDataBits;       /// Number of data bits
    uint8_t  bState;
};
struct usbSetupPacket_t
{
    uint8_t   bmRequestType;
    uint8_t   bRequest;
    uint16_t  wValue;
    uint16_t  wIndex;
    uint16_t  wLength;
};
//
//__data __at (0x08) volatile uint8_t reserved[0x23-0x08];
__data __at (0x23) struct  OsParam_t  OsParam; //
//__data __at (0x23+9) uint16_t OldAddr;
// Common Flags
#define SpI (1<<2)  // uart SPI
#define CdC (1<<4)  // CDC mode
#define PrG (1<<5)  // Flash Programming
#define FlT (1<<7)  // Ticks Timer
// JTAG Flags
#define TmS (1<<0)  // SBW TMS state
#define TdI (1<<1)  // SBW TDI bit
#define TdO (1<<2)  // SBW TDO bit
#define TcK (1<<6)  // SBW TCLK state
// SWD Flags
#define B32 (1<<4)  // SWD 32 BIT mode
#define IgN (1<<5)  // SWD ignore ACK
//
__xdata uint8_t  uartBuf[256]; // __at 0xF000
__xdata struct cc_dma_channel dmaCfg; // __at 0xF100
__xdata struct CDC_LINE_CODING_t CDC_LINE_CODING; // __at 0xF108
__xdata struct usbSetupPacket_t usbSetupPacket; // __at 0xF110
__xdata uint8_t store[8]; // __at 0xF118
//
#define xAdr    OsParam.FlashAddr.A32
#define xBuf    OsParam.flashBuf
#define xCnt    OsParam.flashCnt
//
void PORT1_isr (void) __interrupt (P1INT_VECTOR);
//
void SetAlarm(uint8_t Q)
{
    OsParam.Alarm=Q;
}
void delay (uint8_t ticks)
{
    uint8_t j;
    j=WORTIME0;
    while(ticks--)
    {
        while(j==WORTIME0);
        j=WORTIME0;
    }
}
void PORT1_isr (void) __interrupt (P1INT_VECTOR)
{
//    __asm
//        ljmp #USER_CODE_BASE + #(3+8*P1INT_VECTOR)
//    __endasm;
    P1IFG=0;
}
#define DelayAlarm(T)   {SetAlarm(T); while(OsParam.Alarm){};}
//#pragma codeseg userfunc
uint8_t UserInit(void);
uint8_t UserUnprotect(void);
uint8_t UserRead(void);
uint8_t UserErase(void);
uint8_t UserWrite(void);
uint8_t UserVerify(void);
uint8_t UserProtect(void);
uint8_t UserExit(void);
//
uint8_t UserInit(void)
{
    RTOSfunction.osPrintf("UserInit\n");
    return 0;
}
uint8_t UserUnprotect(void)
{
    return 0;
}
uint8_t UserRead(void)
{
    RTOSfunction.osPrintf("UserRead\n");
    xCnt=0;
    return 0;
}
uint8_t UserErase(void)
{
//        OsParam.FlashAddr.A32=RTOSfunction.osGetDigit();
    RTOSfunction.osPrintf("UserErase at 0x%08lX\n",RTOSfunction.osGetDigit());
    return 0;
}
uint8_t UserWrite(void)
{
    RTOSfunction.osPrintf("UserWrite\n");
    return 0;
}
uint8_t UserVerify(void)
{
    return 0;
}
uint8_t UserProtect(void)
{
    return 0;
}
uint8_t UserExit(void)
{
    RTOSfunction.osPrintf("UserExit\n");
    return 0;
}
//
uint8_t __code UserDesc[]="UserExample TEST";
__code struct FlashProg_t  UserE=\
{
    '0',0xFF,UserDesc,UserInit,UserUnprotect,UserRead,UserErase,UserWrite,UserVerify,UserProtect,UserExit
};
//struct FlashProg_t* __code  __at (FUNCROM + 0*3) UserProg[1] =  {&UserE};
#endif // USER_H_INCLUDED
