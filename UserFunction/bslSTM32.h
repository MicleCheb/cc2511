#ifndef BSLSTM32_H_INCLUDED
#define BSLSTM32_H_INCLUDED

#define BOOT0           DC           // // RTS - TEST - TCK
#define BOOT1           DD           // // DTR - RESET
#define BSLRST          DBG_RESET     // // DTR - RESET
#define BSLRST_ON       DBG_RESET=0     // // DTR - RESET
#define BSLRST_OFF      DBG_RESET=1     // // DTR - RESET
#define BOOT1_DIR_OFF   {P1DIR&=~DD_BIT;DBG_DD_DIR=INPUT;}
#define BOOT1_DIR_ON    {DBG_DD_DIR=OUTPUT;P1DIR|=DD_BIT;}
#define BOOT0_DIR_OFF   {P1DIR&=~DC_BIT;DBG_DC_DIR=INPUT;}
#define BOOT0_DIR_ON    {DBG_DC_DIR=OUTPUT;P1DIR|=DC_BIT;}

#define BSL_TX_SIZE 256
// Gets the version and the allowed commands supported by the current version of the bootloader
#define GET         0x00
// Gets the bootloader version and the Read Protection status of the Flash memory
#define GET_VERSION 0x01
// Gets the chip ID
#define GET_ID      0x02
// Reads up to 256 bytes of memory starting from an address specified by the application
#define ReadMemory  0x11
// Jumps to user application code located in the internal Flash memory or in SRAM
#define Go          0x21
// Writes up to 256 bytes to the RAM or Flash memory starting from an address specified by the application
#define WriteMemory 0x31
// Erases from one to all the Flash memory pages
#define Erase       0x43
// Erases from one to all the Flash memory pages using
// two byte addressing mode (available only for v3.0 usart bootloader versions and above).
// Erase (x043) and Extended Erase (0x44) are exclusive. A device may support either the Erase command
// or the Extended Erase command but not both.
#define ExtendedErase 0x44
// Enables the write protection for some sectors
#define WriteProtect 0x63
//  Disables the write protection for all Flash memory sectors
#define WriteUnprotect 0x73
//  Enables the read protection
#define ReadoutProtect 0x82
//  Disables the read protection
#define ReadoutUnprotect 0x92

#define STM32_START  0x7F
#define STM32_ACK    0x79
#define STM32_NACK   0x1F
#define NO_BSL       0xFF
#define OK_BSL       0x00

#define stmCnt      OsParam.wrk.d8[0]
#define stmCRC      OsParam.wrk.d8[1]
#define XAdr        OsParam.FlashAddr.A32
#define xBuf        OsParam.flashBuf
#define xCnt        OsParam.flashCnt



uint8_t bslSTM32Init(void);
uint8_t bslSTM32PageErase(void);
uint8_t bslSTM32WriteFlash(void);
uint8_t bslSTM32ReadMemory(void);
uint8_t bslSTM32Exit(void);

uint8_t bslSTM32Cmd(uint8_t bslCommand); //,__xdata uint8_t *buf,uint32_t Addr, uint16_t length

//
void bslSTM32_SEND(uint8_t Q)
{
    U0CSR &= ~UxCSR_TX_BYTE;
    U0DBUF = Q;
    stmCRC ^= Q;
    while(!(U0CSR & UxCSR_TX_BYTE));
}
//
uint8_t bslSTM32_READ(uint8_t *ch)
{
    SetAlarm(2);
    while(OsParam.Alarm!=0)
    {
        if(OsParam.uartIn != OsParam.uartOut)
        {
            *ch=uartBuf[OsParam.uartOut++];
//            stmCRC += *ch;
            return OK_BSL;
        }
    }
    return NO_BSL;
}
//
uint8_t bslSTM32SendAdr(void)
{
    uint8_t rsp;
    stmCRC=0x00;
    bslSTM32_SEND(XAdr>>24);
    bslSTM32_SEND(XAdr>>16);
    bslSTM32_SEND(XAdr>>8);
    bslSTM32_SEND(XAdr>>0);
    bslSTM32_SEND(stmCRC);
    if(bslSTM32_READ(&rsp)) return NO_BSL;
    else return rsp;
}
//
uint8_t bslSTM32Cmd(uint8_t bslCommand)
{
    uint8_t rsp;
    stmCRC=0xFF;
    bslSTM32_SEND(bslCommand);
    bslSTM32_SEND(stmCRC);
    if(bslSTM32_READ(&rsp))
    {
//        RTOSfunction.osPrintf("GET %02X\n",rsp);
        return NO_BSL;
    }
    if(rsp != STM32_ACK)
    {
//        RTOSfunction.osPrintf("GET %02X\n",rsp);
        return rsp;
    }
    switch(bslCommand)
    {
    case GET:
    case GET_VERSION:
    case GET_ID:
        if(bslSTM32_READ(&rsp))
        {
//            RTOSfunction.osPrintf("CNT %02X\n",rsp);
            return NO_BSL;
        }
        *xBuf++=rsp; // number of bytes-1
        xCnt=rsp+1;
        while(xCnt--)
        {
            if(bslSTM32_READ(&rsp)) return NO_BSL;
            *xBuf++=rsp;
        }
        if(bslSTM32_READ(&rsp) || (rsp != STM32_ACK))         if(bslSTM32_READ(&rsp))
            {
//                RTOSfunction.osPrintf("CMD %02X\n",rsp);
                return NO_BSL;
            }
        break;
    case Go:
        rsp=bslSTM32SendAdr();
        if(rsp==NO_BSL) return NO_BSL;
        if (rsp != STM32_ACK)
        {
//            RTOSfunction.osPrintf("ADR %02X\n",rsp);
            return rsp;
        }
        break;
    case Erase:
        if(xCnt==255)
        {
            stmCRC=0xFF;
            bslSTM32_SEND(0xFF);
        }
        else
        {
            stmCRC=0xFF;
            bslSTM32_SEND(1);
            bslSTM32_SEND(xCnt);
        }
        bslSTM32_SEND(stmCRC);
        if(bslSTM32_READ(&rsp)) return NO_BSL;
        return rsp;
        break;
    case ReadMemory:
        rsp=bslSTM32SendAdr();
        if(rsp==NO_BSL) return NO_BSL;
        if (rsp != STM32_ACK)
        {
//            RTOSfunction.osPrintf("ADR %02X\n",rsp);
            return rsp;
        }
        stmCRC=0xFF;
        bslSTM32_SEND(xCnt-1);
        bslSTM32_SEND(stmCRC);
        if(bslSTM32_READ(&rsp)) return NO_BSL;
        if (rsp != STM32_ACK)
        {
//            RTOSfunction.osPrintf("ADR %02X\n",rsp);
            return rsp;
        }
        while(xCnt--)
        {
            if(bslSTM32_READ(&rsp)) return NO_BSL;
            *xBuf++=rsp;
        }
        break;
    default:
        return NO_BSL;
    }
    return OK_BSL;
}
//
uint8_t bslSTM32Init(void)
{
    uint8_t i,rsp;
    BOOT0=1;
    BOOT1=0;
    BOOT0_DIR_ON;
    BOOT1_DIR_ON;
    delay(10);
    BSLRST_OFF;
//
    SPI_DISABLE;
    for(i=0; i<8; i++) store[i] = CDC_LINE_CODING.b8[i];
    CDC_LINE_CODING.dwDTERate=115200;
    CDC_LINE_CODING.bDataBits=8;
    CDC_LINE_CODING.bParityType=CDC_PARITY_TYPE_EVEN;
    CDC_LINE_CODING.bStopBits=CDC_CHAR_FORMAT_1_STOP_BIT;
    RTOSfunction.osUartSet();
    UART_ENABLE;
//
    OsParam.uartIn=OsParam.uartOut=0;
    UTX0IF = 0;
    URX0IF = 0;
    URX0IE=1; // USART0 RX interrupt enable
    BSLRST_ON;
//        delay(1);
    bslSTM32_SEND(0);
    for(i=200; i; i--)
    {
        bslSTM32_SEND(STM32_START);
        delay(4);
        if(OsParam.uartIn != OsParam.uartOut)
        {
            rsp=uartBuf[OsParam.uartOut++];
            if(rsp==STM32_ACK) break;
        }
        if(i==1)
        {
            RTOSfunction.osPrintf("ErrBSL...\n");
            return 1+bslSTM32Exit();
        }
    }
    OsParam.uartIn=OsParam.uartOut=0;
//   BSL should now be running!
    RTOSfunction.osPrintf("STM32 BSL ");
    xBuf=(__xdata uint8_t *)XRDATA;
//    xCnt=0;
//    xAdr=0;
    if(bslSTM32Cmd(GET)) return 3+bslSTM32Exit();
    xBuf=(__xdata uint8_t *)XRDATA;
    i=xBuf[0];
    RTOSfunction.osPrintf("%u.%u\n",xBuf[1]>>4,xBuf[1]&0x0F);
    xBuf++;
    xBuf++;
    RTOSfunction.osPrintf("CMD ");
    while(i--)     RTOSfunction.osPrintf("0x%02X ",*xBuf++);
    RTOSfunction.osPrintf("\n");
    return 0;
}
//
uint8_t bslSTM32PageErase(void)
{
    uint8_t rsp;
    RTOSfunction.osPrintf("Page ? (0-254), (255-ALL)\n");
    xCnt=RTOSfunction.osGetDigit();
    rsp=bslSTM32Cmd(Erase);
    if(rsp==NO_BSL)
    {
        return -1;
    }
    else if (rsp==STM32_NACK)
    {
        RTOSfunction.osPrintf("ERROR\n");
//        return 0;
    }
    else
    {
        RTOSfunction.osPrintf("OK\n");
//        return 0;
    }
    return 0;
}
uint8_t bslSTM32WriteFlash(void)
{
//    uint8_t stCnt;
//    uint16_t stAdr;
//    stCnt=xCnt;
//    stAdr=xAdr;
//    xAdr=0x0220;
//    xCnt=0;
////    RTOSfunction.osPrintf("PC 0x0C22\n");
//    if(bsl430F149Cmd(LOAD_PC)) return 4;
//    xCnt=stCnt;
//    xAdr=stAdr;
//    return bsl430F149Cmd( RX_DATA_BLOCK );
    return 0;
}
//
uint8_t bslSTM32ReadMemory(void)
{
    uint8_t rsp;
    xBuf=(__xdata uint8_t *)XRDATA;
    xCnt = 256;
    rsp=bslSTM32Cmd(ReadMemory);
    if(rsp==NO_BSL)
    {
        xCnt = 0;
        return -1;
    }
    else if (rsp==STM32_NACK)
    {
        xCnt = 0;
        return 0;
    }
    else
    {
        xCnt = 256;
        return 0;
    }
}
//
uint8_t bslSTM32Exit(void)
{
    uint8_t i;
    URX0IE=0; // USART0 RX interrupt enable
    UART_DISABLE;
    for(i=0; i<8; i++) CDC_LINE_CODING.b8[i] = store[i];
//C:\Prj\UsbFlash\UsbBsl.c
    BSLRST_OFF;   // DTR - RESET
    BOOT0=0;
    BOOT1=0;
    BOOT0_DIR_OFF;
    BOOT1_DIR_OFF;
    delay(32);
    BSLRST_ON;   // DTR - RESET
    delay(32);
    return 0;
}
uint8_t __code bslSTM32_Desk[] = "STM32 BSL";
__code __at (FUNCROMTEST + 4*sizeof(struct FlashProg_t)) struct FlashProg_t bslSTM32=\
{
    4,'B',bslSTM32_Desk,\
    bslSTM32Init,bslSTM32PageErase,bslSTM32WriteFlash,bslSTM32ReadMemory,bslSTM32Exit
};

#endif // BSLSTM32_H_INCLUDED
