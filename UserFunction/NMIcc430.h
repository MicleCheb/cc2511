#ifndef NMICC430_INCLUDED
#define NMICC430_INCLUDED
#define bsl430FLASHPAGE_SIZE 512

#define BSL_HDR	            0x80
#define NMI_BSL_ACK	        0x00

#define NMI_RX_DATA_BLOCK   0x10
#define NMI_RX_PASSWORD     0x11
#define NMI_ERASE_SEGMENT   0x12
#define UNLOCK_LOCK_INFO    0x13
#define ERASE_BLOCK         0x14
#define NMI_MASS_ERASE      0x15
#define CRC_CHECK           0x16
#define NMI_LOAD_PC         0x17
#define NMI_TX_DATA_BLOCK   0x18
#define TX_NMI_BSL_VERSION  0x19
#define TX_BUFFER_SIZE      0x1A
#define NMI_RX_DATA_BLOCK_FAST  0x1B

#define  DATA_BLOCK         0x3A
#define  NMI_BSL_VERSION    0x3A
#define  CRC_VALUE          0x3A
#define  BUFFER_SIZE        0x3A
#define  MESSAGE            0x3B

#define  NMI_CMD_BAUD       0x52
#define  BAUD_9600          0x02
#define  BAUD_19200         0x03
#define  BAUD_38400         0x04
#define  BAUD_57600         0x05
#define  BAUD_115200        0x06

#define BSL_TX_SIZE 256

//#define BSLRTS         DC      // // RTS - TEST - TCK
//#define BSLDTR         DD     // // DTR - RESET
//#define DTR_DIR_OFF   {P1DIR&=~DD_BIT;DBG_DD_DIR=INPUT;}
//#define DTR_DIR_ON    {DBG_DD_DIR=OUTPUT;P1DIR|=DD_BIT;}
//#define RTS_DIR_OFF   {P1DIR&=~DC_BIT;DBG_DC_DIR=INPUT;}
//#define RTS_DIR_ON    {DBG_DC_DIR=OUTPUT;P1DIR|=DC_BIT;}

#define BSLTST         DC           // // RTS - TEST - TCK
#define BSLTCK         DD           // // DTR - RESET
#define BSLRST         DBG_RESET     // // DTR - RESET
#define BSLRST_ON      DBG_RESET=0     // // DTR - RESET
#define BSLRST_OFF     DBG_RESET=1     // // DTR - RESET
#define TCK_DIR_OFF   {P1DIR&=~DD_BIT;DBG_DD_DIR=INPUT;}
#define TCK_DIR_ON    {DBG_DD_DIR=OUTPUT;P1DIR|=DD_BIT;}
#define TST_DIR_OFF   {P1DIR&=~DC_BIT;DBG_DC_DIR=INPUT;}
#define TST_DIR_ON    {DBG_DC_DIR=OUTPUT;P1DIR|=DC_BIT;}




#define NMI_bslCnt  OsParam.wrk.d16[0]
#define bslCRC      OsParam.wrk.d16[1]
#define XAdr        OsParam.FlashAddr.A32
//#define xBuf        OsParam.flashBuf
//#define xCnt        OsParam.flashCnt

uint8_t bsl430Init(void);
uint8_t bsl430Unprotect(void);
uint8_t bsl430Read(void);
uint8_t bsl430Erase(void);
uint8_t bsl430Write(void);
uint8_t bsl430Verify(void);
uint8_t bsl430Protect(void);
uint8_t bsl430Exit(void);
//
void crc_ccitt(uint8_t value); //,uint16_t crcInit
uint8_t bsl430Cmd(uint8_t bslCommand); //,__xdata uint8_t *buf,uint32_t Addr, uint16_t length

void crc_ccitt(uint8_t value)    //__xdata uint8_t *buf, uint16_t length
{
#define Poly_CCITT 0x1021
#define crcInit bslCRC
    uint8_t  i;
//    uint16_t crc=0xFFFF; ,uint16_t crcInit
//    while(length--)
//    {
    bslCRC ^= value << 8;
    for (i = 0; i < 8; i++)
    {
        bslCRC = (bslCRC & 0x8000)?(bslCRC<<1)^Poly_CCITT:(bslCRC<<1);
    };
//    }
//    return crcInit;
}
//
void SEND_BSL(uint8_t Q)
{
    U0CSR &= ~UxCSR_TX_BYTE;
    U0DBUF = Q;
    while(!(U0CSR & UxCSR_TX_BYTE));
}
void  SEND_CORE(uint8_t Q)
{
    U0CSR &= ~UxCSR_TX_BYTE;
    U0DBUF = Q;
    crc_ccitt(Q);
    while(!(U0CSR & UxCSR_TX_BYTE));
    --xCnt;
}
//
uint8_t READ_BSL(uint8_t *ch)
{
    SetAlarm(2);
    while(1)
    {
        if(OsParam.uartIn != OsParam.uartOut)
        {
            *ch=uartBuf[OsParam.uartOut++];
            return 0;
        }
        else if (OsParam.Alarm==0) return 0xFF;
    }
}
//
uint8_t bsl430Cmd(uint8_t bslCommand)
{
    uint8_t rsp;
    bslCRC=0xFFFF;
// Send data to BSL
    SEND_BSL(BSL_HDR);
    SEND_BSL(xCnt & 0xff);
    SEND_BSL((xCnt >> 8) & 0xff);
// CMD
    SEND_CORE(bslCommand);
// ADDRESS
    switch(bslCommand)
    {
    case NMI_RX_DATA_BLOCK:
    case NMI_RX_DATA_BLOCK_FAST:
    case NMI_ERASE_SEGMENT:
    case CRC_CHECK:
    case NMI_LOAD_PC:
    case NMI_TX_DATA_BLOCK:
        SEND_CORE(XAdr&0x000000FF);
        SEND_CORE((XAdr>>8)&0x000000FF);
        SEND_CORE((XAdr>>16)&0x000000FF);
        break;
//
    case NMI_RX_PASSWORD:
    case UNLOCK_LOCK_INFO:
//    case BSL_NOTUSED:
    case NMI_MASS_ERASE:
    case TX_NMI_BSL_VERSION:
    case TX_BUFFER_SIZE:
    case NMI_CMD_BAUD:
        break;
    default :
        return -1;
        break;
    };
// DATA
    switch(bslCommand)
    {
    case NMI_RX_DATA_BLOCK:
    case NMI_RX_DATA_BLOCK_FAST:
    case NMI_RX_PASSWORD:
    case CRC_CHECK:
    case NMI_TX_DATA_BLOCK:
        while(xCnt)
        {
            SEND_CORE(*xBuf++);
        }
        break;
    default :
        break;
    }
// CRC
    SEND_BSL(bslCRC & 0xff);
    SEND_BSL((bslCRC >> 8) & 0xff);
//
    if(READ_BSL(&rsp) || (rsp !=NMI_BSL_ACK)) return -1;
// Receive data from BSL
    if(bslCommand==NMI_CMD_BAUD) return 0;
//      HEADER
    if(READ_BSL(&rsp) || (rsp!=BSL_HDR)) return -1;
//      Length
    if(READ_BSL(&rsp)) return -1;
    xCnt=rsp;
    if(READ_BSL(&rsp)) return -1;
    xCnt |= rsp<<8;
//
    bslCRC=0xFFFF;
    xBuf = (__xdata uint8_t *)XRDATA;
    if(READ_BSL(&rsp)) return -1;
    crc_ccitt(rsp);
    --xCnt;
    switch(rsp)
    {
    case DATA_BLOCK:
//    case NMI_BSL_VERSION:
//    case CRC_VALUE:
//    case BUFFER_SIZE:
        while(xCnt--)
        {
            if(READ_BSL(&rsp)) return -1;
            crc_ccitt(rsp);
            *xBuf++=rsp;
        }
        break;
    case MESSAGE:
        if(READ_BSL(&rsp)) return -1;
        crc_ccitt(rsp);
        *xBuf++=rsp;
        break;
    default:
        return -1;
        break;
    }
// Check CRC
    if(READ_BSL(&rsp) || (rsp!= (bslCRC&0x00FF)))  return -1;
    if(READ_BSL(&rsp) || (rsp!= (bslCRC>>8)))  return -1;
    return 0;
}
//
uint8_t bsl430Init(void)
{
    uint8_t i;
    __xdata uint8_t *b8;
    b8 = (__xdata uint8_t *)&CDC_LINE_CODING;
//
//    BSLDTR=0;   // DTR - RESET
//    DTR_DIR_ON;
    BSLRST_OFF;
    BSLTST=0;   // RTS - TEST - TCK
    TST_DIR_ON;
    SetAlarm(64);
    RTOSfunction.osPrintf("Plug Cable && Pwr...\n");
    while(OsParam.Alarm!=0) {}
    delay(40);
//
    SPI_DISABLE;
    for(i=0; i<8; i++) store[i] = b8[i];
    CDC_LINE_CODING.dwDTERate=9600;
    CDC_LINE_CODING.bDataBits=8;
    CDC_LINE_CODING.bParityType=CDC_PARITY_TYPE_EVEN;
    CDC_LINE_CODING.bStopBits=CDC_CHAR_FORMAT_1_STOP_BIT;
    RTOSfunction.osUartSet();
    UART_ENABLE;
//
    OsParam.uartIn=OsParam.uartOut=0;
    UTX0IF = 0;
    URX0IF = 0;
    URX0IE=1; // USART0 RX interrupt enable
//    for(i=10; i; i--)
    {
//        bslStart();
//        BSLDTR=0;   // DTR - RESET
//        BSLTST=0;   // RTS - TEST - TCK
//        delay(40);
        BSLTST=1;
        delay(40);
        BSLTST=0;
        delay(40);

        BSLTST=1;
        delay(40);
        BSLRST_ON;
        delay(40);
        BSLTST=0;
        delay(40);
        SetAlarm(2);
        while(OsParam.Alarm);
        OsParam.uartIn=OsParam.uartOut=0;
    }
//   BSL should now be running!
    if(READ_BSL(&i)==0)
    {
        RTOSfunction.osPrintf("ErrBSL...\n");
        return 1;
    }
    else RTOSfunction.osPrintf("Erase...\n");
//  ERASE
    xCnt=1;
    if(bsl430Cmd( NMI_MASS_ERASE )) return 1;
// RX PASSWORD
    xBuf=(__xdata uint8_t *)XRDATA;
    for(i=32; i; i--) *xBuf++=0xFF;
//
    xBuf=(__xdata uint8_t *)XRDATA;
    xCnt=32+1;
    if(bsl430Cmd(NMI_RX_PASSWORD)) return 2;
//  UNLOCK_LOCK_INFO A
    xCnt=1;
    if(bsl430Cmd( UNLOCK_LOCK_INFO )) return 1;
// BSL VERSION
    xCnt=1;
    xBuf=(__xdata uint8_t *)XRDATA;
    if(bsl430Cmd(TX_NMI_BSL_VERSION)) return 3;
//
    RTOSfunction.osPrintf("BSL : "); // BSL : 00.05.04.52.
    xBuf=(__xdata uint8_t *)XRDATA;
    for(i=4; i; i--) RTOSfunction.osPrintf("%02X.",*xBuf++);
    RTOSfunction.osPrintf("\n");
//    if( bsl430Cmd( NMI_TX_DATA_BLOCK,RamBuf,0x1A04,2)==0xFF) return -1;
    XAdr=0x001A00;
    xBuf=(__xdata uint8_t *)XRDATA;
    xCnt = 8;
    *xBuf++=xCnt&0x00FF;
    *xBuf=xCnt>>8;
    xBuf=(__xdata uint8_t *)XRDATA;
    xCnt = 1+3+2;
    if(bsl430Cmd( NMI_TX_DATA_BLOCK )) return 1;
    xBuf=(__xdata uint8_t *)(XRDATA+4);
    RTOSfunction.osPrintf("CHIP ID : "); // CHIP ID : 51351212
    for(i=4; i; i--) RTOSfunction.osPrintf("%02X",*xBuf++);
    RTOSfunction.osPrintf("\n");
    return 0;
}
uint8_t bsl430Unprotect(void)
{
    return 0;
}
//
uint8_t bsl430Read(void)
{
//   uint8_t i;
//    XAdr <<= 8;
    xBuf=(__xdata uint8_t *)XRDATA;
    xCnt = BSL_TX_SIZE;
    *xBuf++=xCnt&0x00FF;
    *xBuf=xCnt>>8;
    xBuf=(__xdata uint8_t *)XRDATA;
    xCnt = 1+3+2;
    if(bsl430Cmd( NMI_TX_DATA_BLOCK )) return 1;
    xCnt=BSL_TX_SIZE;
    return 0;
}
//
uint8_t bsl430Erase(void)
{
//    uint8_t i;
    XAdr=RTOSfunction.osGetDigit();
    RTOSfunction.osPrintf("0x%08lX ",XAdr);
    xCnt=1+3;
    if(bsl430Cmd( NMI_ERASE_SEGMENT )) return 1; //
    else return 0;
}
uint8_t bsl430Write(void)
{
//    uint8_t i;
    xCnt += 1+3;
    if(bsl430Cmd( NMI_RX_DATA_BLOCK )) return 1;
    return 0;
}
//
uint8_t bsl430Verify(void)
{
    return 0;
}
uint8_t bsl430Protect(void)
{
    return 0;
}
//
uint8_t bsl430Exit(void)
{
    uint8_t i;
        __xdata uint8_t *b8;
    b8 = (__xdata uint8_t *)&CDC_LINE_CODING;
#define PMMCTL0 0x0120
#define PMMCTL0_UNLOCK 0xA5
#define PMMSWPOR ((1<<3)+(1<<2))
//    XAdr=PMMCTL0+1;
//    xBuf=(__xdata uint8_t *)XRDATA;
//    xCnt = 1+3+1;
//    *xBuf=PMMCTL0_UNLOCK;
//    bsl430Cmd( NMI_RX_DATA_BLOCK );
////   if(bsl430Cmd( NMI_RX_DATA_BLOCK )) return 1;
//    XAdr=PMMCTL0+0;
//    xBuf=(__xdata uint8_t *)XRDATA;
//    xCnt = 1+3+1;
//    *xBuf=PMMSWPOR;
//    bsl430Cmd( NMI_RX_DATA_BLOCK );
//   if(bsl430Cmd( NMI_RX_DATA_BLOCK )) return 1;
#define WDTCTL 0x015C
    XAdr=WDTCTL;
    xBuf=(__xdata uint8_t *)XRDATA;
    xCnt = 1+3+1;
    *xBuf=0;
    bsl430Cmd( NMI_RX_DATA_BLOCK );
//
    TST_DIR_OFF;
    URX0IE=0; // USART0 RX interrupt enable
    UART_DISABLE;
    for(i=0; i<8; i++) b8[i] = store[i];
    return 0;
}
uint8_t __code bsl430Desk[]="NMI_MSP430F5xxx BSL";
__code struct FlashProg_t BSL430=\
{ '2',0xFF,bsl430Desk,bsl430Init,bsl430Init,bsl430Read,bsl430Erase,bsl430Write,bsl430Verify,bsl430Protect,bsl430Exit
};
//struct FlashProg_t* __code  __at (FUNCROM + 2*3) bsl430Prog[1] =  {&BSL430};

#endif // NMICC430_INCLUDED
