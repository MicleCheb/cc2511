#ifndef AVRISP_H_INCLUDED
#define AVRISP_H_INCLUDED

#define ispDO           P1_1
#define ispDI           P1_7
#define ispRST          P0_1
#define ispCLK          P0_3

#define ispRSTon        P0_1=1
#define ispRSToff       P0_1=0
#define ispCLKoff       SPI_DISABLE
#define ispCLKon        {P0_3 = 0; P0DIR |= (1<<3); SPI_ENABLE;}
#define ispDOon         {P1_1 = 0; P1DIR |= (1<<1); DC_OUTPUT;}
#define ispDOoff        DC_INPUT
#define ispDIon         {P1DIR &= ~(1<<7);  DD_INPUT;}
#define ispDIoff        DD_INPUT



uint8_t ispSendByte(uint8_t rwByte);
uint8_t AvrBusy(void);
uint8_t AvrMemRead(uint32_t address);
uint8_t AvrMemWrite(uint32_t address,uint8_t data);
uint8_t AvrInit(void);
uint8_t AvrUnprotect(void);
uint8_t AvrRead(void);
uint8_t AvrErase(void);
uint8_t AvrWrite(void);
uint8_t AvrVerify(void);
uint8_t AvrProtect(void);
uint8_t AvrExit(void);
//
uint8_t ispSendByte(uint8_t rwByte)
{
    uint8_t i,j;
    for(i=8; i; i--)
    {
        if(rwByte & 0x80) ispDO = 1;
        else ispDO = 0;
        rwByte <<= 1;
        if(ispDI) rwByte |= 1;
        ispCLK = 1;
        for(j=12; j; j--);
        ispCLK = 0;
        for(j=12; j; j--);
    }
    return rwByte;
}
//
uint8_t AvrBusy(void)
{
// Poll RDY/~BSY 1111 0000 0000 0000 xxxx xxxx xxxx xxxo
    uint8_t ispByte;
    ispSendByte(0xF0);
    ispSendByte(0x00);
    ispSendByte(0x00);
    ispByte = ispSendByte(0x00);
// If �1�, a programming operation is still busy.
    return ispByte & 1;
}
//

//
uint8_t AvrMemWrite(uint32_t address,uint8_t data)
{
// Load program memory page 0100 H000 000x xxxx xxbb bbbb iiii iiii
    ispSendByte(0x40 | ((address & 1) << 3));
    ispSendByte(address >> 9);
    ispSendByte(address >> 1);
    ispSendByte(data);
    return 0;
}
//
uint8_t AvrInit(void)
{
// Programming enable 1010 1100 0101 0011 xxxx xxxx xxxx xxxx
    uint8_t retries,ispByte,j;
// set pins
    UART_DISABLE;
    ispCLKon;
    ispDIon;
    ispDOon;
// reset device
    RTOSfunction.osPrintf("AVR Reset...");
    DelayAlarm(1);
    for (retries = 4; retries > 0; retries--)
    {
        ispRSTon;
        /* positive reset pulse > 2 SCK (target) */
        for(j=24; j; j--);
        ispRSToff; /* RST high */
        for(j=24; j; j--);
        ispRSTon; /* RST low */
// wait minimum 20ms
        DelayAlarm(2);
//
        j = ispSendByte(0xAC);
        RTOSfunction.osPrintf(" %02X",j);
        j = ispSendByte(0x53);
        RTOSfunction.osPrintf(" %02X",j);
        ispByte = ispSendByte(0x00);
        RTOSfunction.osPrintf(" %02X",ispByte);
        j = ispSendByte(0x00);
        RTOSfunction.osPrintf(" %02X",j);
        if (ispByte == 0x53)
        {
// we are in sync
// Read signature byte 0011 0000 000x xxxx xxxx xxbb oooo oooo
//ATmega88 Signature Bytes
//1. 0x000: 0x1E (indicates manufactured by Atmel).
//2. 0x001: 0x93 (indicates 8KB flash memory).
//3. 0x002: 0x0A (indicates ATmega88 device when 0x001 is 0x93).
            ispSendByte(0x30);
            ispSendByte(0x00);
            ispSendByte(0x00);
            ispByte = ispSendByte(0x00);
            RTOSfunction.osPrintf("Signature : 0x%02X",ispByte);
            ispSendByte(0x30);
            ispSendByte(0x00);
            ispSendByte(0x01);
            ispByte = ispSendByte(0x00);
            RTOSfunction.osPrintf("%02X",ispByte);
            ispSendByte(0x30);
            ispSendByte(0x00);
            ispSendByte(0x02);
            ispByte = ispSendByte(0x00);
            RTOSfunction.osPrintf("%02X\n",ispByte);
// Read calibration byte 0011 1000 000x xxxx 0000 0000 oooo oooo
            ispSendByte(0x38);
            ispSendByte(0x00);
            ispSendByte(0x00);
            ispByte = ispSendByte(0x00);
            RTOSfunction.osPrintf("Calibration : 0x%02X\n",ispByte);
// Read lock byte
            ispSendByte(0x58);
            ispSendByte(0x00);
            ispSendByte(0x00);
            ispByte = ispSendByte(0x00);
            RTOSfunction.osPrintf("Lock : 0x%02X\n",ispByte);
//Read Fuse Low bits
            ispSendByte(0x50);
            ispSendByte(0x00);
            ispSendByte(0x00);
            ispByte = ispSendByte(0x00);
            RTOSfunction.osPrintf("FuseL : 0x%02X\n",ispByte);
// Read Fuse High bits
            ispSendByte(0x58);
            ispSendByte(0x08);
            ispSendByte(0x00);
            ispByte = ispSendByte(0x00);
            RTOSfunction.osPrintf("FuseH : 0x%02X\n",ispByte);
//Read Extended Fuse Bits
            ispSendByte(0x50);
            ispSendByte(0x08);
            ispSendByte(0x00);
            ispByte = ispSendByte(0x00);
            RTOSfunction.osPrintf("FuseE : 0x%02X\n",ispByte);
            return 0;
        }
    };
    RTOSfunction.osPrintf(" No Chip\n");
    return 1;
}
uint8_t AvrUnprotect(void)
{

//    AvrErase();
    return 0;
}
//
uint8_t AvrRead(void)
{
   uint8_t  x,j;
#define AVRBUFSIZE 32
    while(xAdr < 0x8000)
    {
        x  = 0x20;
        x += (xAdr>>8);
        x += (xAdr & 0x00FF);
        x += 0;
        RTOSfunction.osPrintf(":20%04X00",(uint16_t)xAdr);
        for(xCnt=0; xCnt < AVRBUFSIZE; xCnt++)
        {
// Read program memory 0010 H000 000a aaaa bbbb bbbb oooo oooo
            ispSendByte(0x20 | ((xAdr & 1) << 3));
            ispSendByte(xAdr >> 9);
            ispSendByte(xAdr >> 1);
            j = ispSendByte(0);
            RTOSfunction.osPrintf("%02X",j);
            x += j;
            ++xAdr;
        }
        x ^= 0xFF;
        ++x;
        RTOSfunction.osPrintf("%02X\n",x);
    }
    RTOSfunction.osPrintf(":00000001FF\n");
    xAdr -= AVRBUFSIZE;
    return 0;
}
uint8_t AvrErase(void)
{
// Chip erase 1010 1100 100x xxxx xxxx xxxx xxxx xxxx
    ispSendByte(0xAC);
    ispSendByte(0x80);
    ispSendByte(0x00);
    ispSendByte(0x00);
    DelayAlarm(2);
    return 0;
}
uint8_t AvrWrite(void)
{
#define AVR88PAGESIZE 64    // 32 words
    uint8_t doflash = 0;
// Write program memory page 0100 1100 000a aaaa bbxx xxxx xxxx xxxx
    while(xCnt)
    {
        AvrMemWrite(xAdr,*xBuf);
        doflash = 1;
        if(((xAdr % AVR88PAGESIZE) == 0) || (xCnt == 1))
        {
            ispSendByte(0x4C);
            ispSendByte(xAdr>>9);
            ispSendByte(xAdr>>1);
            ispSendByte(0x00);
            delay(200); // 5msec
            doflash = 0;
        }
        ++xAdr;
        ++xBuf;
        --xCnt;
    }
    return 0;
}
uint8_t AvrVerify(void)
{
    return 0;
}
uint8_t AvrProtect(void)
{
// Write fuse bits 1010 1100 1010 0000 xxxx xxxx iiii iiii
// Write fuse high bits 1010 1100 1010 1000 xxxx xxxx iiii iiii
// Write extended fuse bits 1010 1100 1010 0100 xxxx xxxx xxxx xxii
// Read fuse bits 0101 0000 0000 0000 xxxx xxxx oooo oooo
// Read fuse high bits 0101 1000 0000 1000 xxxx xxxx oooo oooo
// Read extended fuse bits 0101 0000 0000 1000 xxxx xxxx oooo oooo
// Read lock bits 0101 1000 0000 0000 xxxx xxxx xxoo oooo
// Write lock bits 1010 1100 111x xxxx xxxx xxxx 11ii iiii

// Extended Fuse Byte 0
// Fuse High Byte BODLEVEL0
#define FUSEHIGHBYTE (1<<0)
    ispSendByte(0xAC);
    ispSendByte(0xA8);
    ispSendByte(0);
    ispSendByte(FUSEHIGHBYTE^0xFF);
    delay(200); // 5msec
// Fuse Low Byte CKDIV8 SUT0 CKSEL3  CKSEL2  CKSEL1
#define FUSELOWBYTE ((1<<7)|(1<<4)|(1<<3)|(1<<2)|(1<<0))
    ispSendByte(0xAC);
    ispSendByte(0xA0);
    ispSendByte(0);
    ispSendByte(FUSELOWBYTE^0xFF);
    delay(200); // 5msec
// Lock Bit Byte  BL11 BL01  L2 L1
#define LOCKBITBYTE ((1<<4)|(1<<2)|(1<<1)|(1<<0))
    ispSendByte(0xAC);
    ispSendByte(0xE0);
    ispSendByte(0);
    ispSendByte(LOCKBITBYTE^0xFF);
    delay(200); // 5msec
    return 0;
}
uint8_t AvrExit(void)
{
// set all ISP pins inputs
    ispCLKoff;
    ispDIoff;
    ispDOoff;
    ispRSToff;
    DelayAlarm(1);
    return 0;
}
//
uint8_t ispReadEEPROM(uint16_t address)
{
// Read EEPROM memory 1010 0000 000x xxaa bbbb bbbb oooo oooo
    ispSendByte(0xA0);
    ispSendByte(address >> 8);
    ispSendByte(address);
    return ispSendByte(0);
}

uint8_t ispWriteEEPROM(uint16_t address, uint8_t data)
{
// Write EEPROM memory 1100 0000 000x xxaa bbbb bbbb iiii iiii
    ispSendByte(0xC0);
    ispSendByte(address >> 8);
    ispSendByte(address);
    ispSendByte(data);
    DelayAlarm(2); // wait 9,6 ms
    return 0;
}
uint8_t __code AvrDesc[]="ATMEGA88";
__code struct FlashProg_t  AvrE=\
{
    'M',0xFF,AvrDesc,AvrInit,AvrUnprotect,AvrRead,AvrErase,AvrWrite,AvrVerify,AvrProtect,AvrExit
};

#endif // AVRISP_H_INCLUDED
