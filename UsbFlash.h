#ifndef USBFLASH_H_INCLUDED
#define USBFLASH_H_INCLUDED
#include <stdio.h>
#include <stdint.h>
#include "cc1111.h"
//-Wl -buserfunc=0x3000
//
void FlasherHelp(void)
{
    printf(" h - Help\n");
    printf(" u - UnProtect\n");
    printf(" r - Read\n");
    printf(" e - Erase\n");
    printf(" w - Write\n");
    printf(" v - Verify\n");
    printf(" p - Protect\n");
    printf(" q - Quit\n");
}
//
uint8_t Flasher(void) __reentrant
{
    if((OsParam.FlashMemFunc)->flInit()) return (0x70+(OsParam.FlashMemFunc)->flExit());
    FlasherHelp();
    while(1)
    {
        switch(getchar())
        {
        case 'e':
            printf("Erase...\n");
            if((OsParam.FlashMemFunc)->flErase()) return (0x30+(OsParam.FlashMemFunc)->flExit());
            printf("OK\n");
            break;
        case 'w':
            printf("Prog...\n");
            OsParam.FlashAddr.A32=0;
            do
            {
                OsParam.flashBuf=(__xdata uint8_t *)XRDATA;
                if(IntelHex())
                {
                    printf("HEX ERROR\n");
                    return (0x40+(OsParam.FlashMemFunc)->flExit());
                }
                else
                {
                    OsParam.flashBuf=(__xdata uint8_t *)XRDATA;
                    OsParam.flashCnt =OsParam.flashBuf[0];
                    switch(OsParam.flashBuf[3])
                    {
                    case recEOF:
                        printf("OK\nVerify Flash\n");
                        goto FlashOK;
                        break;
                    case recData:
                        OsParam.FlashAddr.A8[0]=OsParam.flashBuf[2];
                        OsParam.FlashAddr.A8[1]=OsParam.flashBuf[1];
                        OsParam.flashBuf=(__xdata uint8_t *)XRDATA+4;
                        if((OsParam.FlashMemFunc)->flWrite()) return (0x40+(OsParam.FlashMemFunc)->flExit());
                        break;
                    case   recSegment:
//#define recSegment  2   //:020000021200EA
//                flashAddr.A8[0]=flashBuf[5];
//                flashAddr.A8[1]=flashBuf[4];
//                flashAddr.A8[2]=0;//flashBuf[2];
//                flashAddr.A8[3]=0;//flashBuf[1];
//                flashAddr.A32<<=4;
                        break;
                    case recStartSeg:
// #define recStartSeg 0x03 // :040000030000C00039 Start Segment Address Record.
//                        flashAddr.A8[0]=flashBuf[7];
//                        flashAddr.A8[1]=flashBuf[6];
//                        flashAddr.A8[2]=flashBuf[5];
//                        flashAddr.A8[3]=flashBuf[4];
                        break;
                    case recLinear:
//#define recLinear   4   //:02000004FFFFFC
                        OsParam.FlashAddr.A8[0]=0;//flashBuf[2];
                        OsParam.FlashAddr.A8[1]=0;//flashBuf[1];
                        OsParam.FlashAddr.A8[2]=OsParam.flashBuf[5];
                        OsParam.FlashAddr.A8[3]=OsParam.flashBuf[4];
                        break;
                    case recMDKARM:
//#define recMDKARM   5   //:04000005000000CD2A
//                flashAddr.A8[0]=flashBuf[7];
//                flashAddr.A8[1]=flashBuf[6];
//                flashAddr.A8[2]=flashBuf[5];
//                flashAddr.A8[3]=flashBuf[4];
                        break;
                    default:
                        printf("UNKNOWN %02X\n",OsParam.flashBuf[3]);
                        return (0x40+(OsParam.FlashMemFunc)->flExit());
                        break;
                    }
                };
            }
            while(1);
            break;
        case 'r':
            printf("Read Adr...\n");
            OsParam.FlashAddr.A32=getDigit();
            printf("0x%08lX\n",OsParam.FlashAddr.A32);
            OsParam.flashBuf=(__xdata uint8_t *)XRDATA;
// read max 1024 bytes (flashCnt)
            if((OsParam.FlashMemFunc)->flRead()) return (0x20+(OsParam.FlashMemFunc)->flExit());
            OsParam.flashBuf=(__xdata uint8_t *)XRDATA;
            printf("@%08lX\n",OsParam.FlashAddr.A32);
            while(OsParam.flashCnt--)
            {
                printf("%02X",*OsParam.flashBuf++);
                if(OsParam.flashCnt%32 == 0)  printf("\n");
            }
            printf("\n\r");
            printf("OK\n");
            break;
        case 'v':
            if((OsParam.FlashMemFunc)->flVerify()) return (0x50+(OsParam.FlashMemFunc)->flExit());
            break;
        case 'p':
            if((OsParam.FlashMemFunc)->flProtect()) return (0x60+(OsParam.FlashMemFunc)->flExit());
            break;
        case 'u':
            if((OsParam.FlashMemFunc)->flUnProtect()) return (0x10+(OsParam.FlashMemFunc)->flExit());
            break;
        case 'q':
            return (0x00+(OsParam.FlashMemFunc)->flExit());
            break;
        case 'h':
FlashOK:
            FlasherHelp();
            break;
        default:
            break;
        }
    };
//    return 0;
}
//
void printFlashHelp(void)
{
    uint8_t i;
    __code struct FlashProg_t* Fun;
    union
    {
        uint8_t *a8;
        uint16_t *a16;
        uint16_t d16;
    } aFun;
    printf("\n\r");
    printf("  H - HELP\n");
    aFun.d16 = FUNCROM;
    for(i=0; i < MAX_USER_PROGRAM; i++)
    {
//        printf("Check adr %04X %04X\n",aFun.d16,*aFun.a16);
        Fun = (__code struct FlashProg_t*)(*aFun.a16);
        if(*aFun.a8 != 0xFF)
        {
            printf("%c - %s\n",Fun->ind,Fun->descriptor);
        };
        aFun.a8 += 3;
    }
    printf("  Q - QUIT\n");
}
//
void LedsWait(void)
{
    OsParam.GreenLedMask=MaskOff;
    OsParam.RedLedMask=MaskNorma;
}
//
uint8_t UsbFlashProg(void)
{
    uint8_t Tind,i;
    union
    {
        uint8_t *a8;
        uint16_t *a16;
        uint16_t d16;
    } aFun;
    printFlashHelp();
    while(1)
    {
        switch(Tind=getchar())
        {
        case 'Q':
            return 0;
            break;
        case 'H':
            printFlashHelp();
            break;
        default:
            aFun.d16 = FUNCROM;
            for(i=0; i < MAX_USER_PROGRAM; i++)
            {
                OsParam.FlashMemFunc = (__code struct FlashProg_t *)(*aFun.a16);
                if(Tind == (OsParam.FlashMemFunc)->ind)
                {
                    return Flasher();
                }
                else aFun.a8 += 3;
            }
            break;
        }
    }
}
//
#endif // USBFLASH_H_INCLUDED
